export enum Currency {
  LYD = 'LYD',

  USD = 'USD',

  EUR = 'EUR',

  TND = 'TND',
}
