import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IPaymentReceipt, PaymentReceipt } from '../payment-receipt.model';
import { PaymentReceiptService } from '../service/payment-receipt.service';
import { IInvestor } from 'app/entities/investor/investor.model';
import { InvestorService } from 'app/entities/investor/service/investor.service';

@Component({
  selector: 'jhi-payment-receipt-update',
  templateUrl: './payment-receipt-update.component.html',
})
export class PaymentReceiptUpdateComponent implements OnInit {
  isSaving = false;

  investorsSharedCollection: IInvestor[] = [];

  editForm = this.fb.group({
    id: [],
    amount: [],
    details: [],
    receiptDate: [],
    investor: [],
  });

  constructor(
    protected paymentReceiptService: PaymentReceiptService,
    protected investorService: InvestorService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ paymentReceipt }) => {
      this.updateForm(paymentReceipt);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const paymentReceipt = this.createFromForm();
    if (paymentReceipt.id !== undefined) {
      this.subscribeToSaveResponse(this.paymentReceiptService.update(paymentReceipt));
    } else {
      this.subscribeToSaveResponse(this.paymentReceiptService.create(paymentReceipt));
    }
  }

  trackInvestorById(index: number, item: IInvestor): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPaymentReceipt>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(paymentReceipt: IPaymentReceipt): void {
    this.editForm.patchValue({
      id: paymentReceipt.id,
      amount: paymentReceipt.amount,
      details: paymentReceipt.details,
      receiptDate: paymentReceipt.receiptDate,
      investor: paymentReceipt.investor,
    });

    this.investorsSharedCollection = this.investorService.addInvestorToCollectionIfMissing(
      this.investorsSharedCollection,
      paymentReceipt.investor
    );
  }

  protected loadRelationshipsOptions(): void {
    this.investorService
      .query()
      .pipe(map((res: HttpResponse<IInvestor[]>) => res.body ?? []))
      .pipe(
        map((investors: IInvestor[]) =>
          this.investorService.addInvestorToCollectionIfMissing(investors, this.editForm.get('investor')!.value)
        )
      )
      .subscribe((investors: IInvestor[]) => (this.investorsSharedCollection = investors));
  }

  protected createFromForm(): IPaymentReceipt {
    return {
      ...new PaymentReceipt(),
      id: this.editForm.get(['id'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      details: this.editForm.get(['details'])!.value,
      receiptDate: this.editForm.get(['receiptDate'])!.value,
      investor: this.editForm.get(['investor'])!.value,
    };
  }
}
