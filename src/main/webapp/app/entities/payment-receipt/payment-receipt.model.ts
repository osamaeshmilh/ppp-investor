import * as dayjs from 'dayjs';
import { IInvestor } from 'app/entities/investor/investor.model';

export interface IPaymentReceipt {
  id?: number;
  amount?: number | null;
  details?: string | null;
  receiptDate?: dayjs.Dayjs | null;
  investor?: IInvestor | null;
}

export class PaymentReceipt implements IPaymentReceipt {
  constructor(
    public id?: number,
    public amount?: number | null,
    public details?: string | null,
    public receiptDate?: dayjs.Dayjs | null,
    public investor?: IInvestor | null
  ) {}
}

export function getPaymentReceiptIdentifier(paymentReceipt: IPaymentReceipt): number | undefined {
  return paymentReceipt.id;
}
