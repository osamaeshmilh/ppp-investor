import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PaymentReceiptComponent } from './list/payment-receipt.component';
import { PaymentReceiptDetailComponent } from './detail/payment-receipt-detail.component';
import { PaymentReceiptUpdateComponent } from './update/payment-receipt-update.component';
import { PaymentReceiptDeleteDialogComponent } from './delete/payment-receipt-delete-dialog.component';
import { PaymentReceiptRoutingModule } from './route/payment-receipt-routing.module';
import { NgxPrintModule } from 'ngx-print';

@NgModule({
  imports: [SharedModule, PaymentReceiptRoutingModule, NgxPrintModule],
  declarations: [
    PaymentReceiptComponent,
    PaymentReceiptDetailComponent,
    PaymentReceiptUpdateComponent,
    PaymentReceiptDeleteDialogComponent,
  ],
  entryComponents: [PaymentReceiptDeleteDialogComponent],
})
export class PaymentReceiptModule {}
