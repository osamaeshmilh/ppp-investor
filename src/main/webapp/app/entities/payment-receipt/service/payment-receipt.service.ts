import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPaymentReceipt, getPaymentReceiptIdentifier } from '../payment-receipt.model';

export type EntityResponseType = HttpResponse<IPaymentReceipt>;
export type EntityArrayResponseType = HttpResponse<IPaymentReceipt[]>;

@Injectable({ providedIn: 'root' })
export class PaymentReceiptService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/payment-receipts');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(paymentReceipt: IPaymentReceipt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(paymentReceipt);
    return this.http
      .post<IPaymentReceipt>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(paymentReceipt: IPaymentReceipt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(paymentReceipt);
    return this.http
      .put<IPaymentReceipt>(`${this.resourceUrl}/${getPaymentReceiptIdentifier(paymentReceipt) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(paymentReceipt: IPaymentReceipt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(paymentReceipt);
    return this.http
      .patch<IPaymentReceipt>(`${this.resourceUrl}/${getPaymentReceiptIdentifier(paymentReceipt) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPaymentReceipt>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPaymentReceipt[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPaymentReceiptToCollectionIfMissing(
    paymentReceiptCollection: IPaymentReceipt[],
    ...paymentReceiptsToCheck: (IPaymentReceipt | null | undefined)[]
  ): IPaymentReceipt[] {
    const paymentReceipts: IPaymentReceipt[] = paymentReceiptsToCheck.filter(isPresent);
    if (paymentReceipts.length > 0) {
      const paymentReceiptCollectionIdentifiers = paymentReceiptCollection.map(
        paymentReceiptItem => getPaymentReceiptIdentifier(paymentReceiptItem)!
      );
      const paymentReceiptsToAdd = paymentReceipts.filter(paymentReceiptItem => {
        const paymentReceiptIdentifier = getPaymentReceiptIdentifier(paymentReceiptItem);
        if (paymentReceiptIdentifier == null || paymentReceiptCollectionIdentifiers.includes(paymentReceiptIdentifier)) {
          return false;
        }
        paymentReceiptCollectionIdentifiers.push(paymentReceiptIdentifier);
        return true;
      });
      return [...paymentReceiptsToAdd, ...paymentReceiptCollection];
    }
    return paymentReceiptCollection;
  }

  protected convertDateFromClient(paymentReceipt: IPaymentReceipt): IPaymentReceipt {
    return Object.assign({}, paymentReceipt, {
      receiptDate: paymentReceipt.receiptDate?.isValid() ? paymentReceipt.receiptDate.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.receiptDate = res.body.receiptDate ? dayjs(res.body.receiptDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((paymentReceipt: IPaymentReceipt) => {
        paymentReceipt.receiptDate = paymentReceipt.receiptDate ? dayjs(paymentReceipt.receiptDate) : undefined;
      });
    }
    return res;
  }
}
