import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPaymentReceipt } from '../payment-receipt.model';

@Component({
  selector: 'jhi-payment-receipt-detail',
  templateUrl: './payment-receipt-detail.component.html',
})
export class PaymentReceiptDetailComponent implements OnInit {
  paymentReceipt: IPaymentReceipt | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ paymentReceipt }) => {
      this.paymentReceipt = paymentReceipt;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
