import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PaymentReceiptComponent } from '../list/payment-receipt.component';
import { PaymentReceiptDetailComponent } from '../detail/payment-receipt-detail.component';
import { PaymentReceiptUpdateComponent } from '../update/payment-receipt-update.component';
import { PaymentReceiptRoutingResolveService } from './payment-receipt-routing-resolve.service';

const paymentReceiptRoute: Routes = [
  {
    path: '',
    component: PaymentReceiptComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PaymentReceiptDetailComponent,
    resolve: {
      paymentReceipt: PaymentReceiptRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PaymentReceiptUpdateComponent,
    resolve: {
      paymentReceipt: PaymentReceiptRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PaymentReceiptUpdateComponent,
    resolve: {
      paymentReceipt: PaymentReceiptRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(paymentReceiptRoute)],
  exports: [RouterModule],
})
export class PaymentReceiptRoutingModule {}
