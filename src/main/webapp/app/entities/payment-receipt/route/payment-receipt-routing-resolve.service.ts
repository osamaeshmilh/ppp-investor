import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPaymentReceipt, PaymentReceipt } from '../payment-receipt.model';
import { PaymentReceiptService } from '../service/payment-receipt.service';

@Injectable({ providedIn: 'root' })
export class PaymentReceiptRoutingResolveService implements Resolve<IPaymentReceipt> {
  constructor(protected service: PaymentReceiptService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPaymentReceipt> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((paymentReceipt: HttpResponse<PaymentReceipt>) => {
          if (paymentReceipt.body) {
            return of(paymentReceipt.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PaymentReceipt());
  }
}
