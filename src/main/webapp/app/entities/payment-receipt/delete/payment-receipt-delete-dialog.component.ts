import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPaymentReceipt } from '../payment-receipt.model';
import { PaymentReceiptService } from '../service/payment-receipt.service';

@Component({
  templateUrl: './payment-receipt-delete-dialog.component.html',
})
export class PaymentReceiptDeleteDialogComponent {
  paymentReceipt?: IPaymentReceipt;

  constructor(protected paymentReceiptService: PaymentReceiptService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.paymentReceiptService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
