export interface ICity {
  id?: number;
  name?: string | null;
  nameAr?: string | null;
  nameEn?: string | null;
  lat?: number | null;
  lng?: number | null;
}

export class City implements ICity {
  constructor(
    public id?: number,
    public name?: string | null,
    public nameAr?: string | null,
    public nameEn?: string | null,
    public lat?: number | null,
    public lng?: number | null
  ) {}
}

export function getCityIdentifier(city: ICity): number | undefined {
  return city.id;
}
