import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProject } from '../project.model';
import { HttpResponse } from '@angular/common/http';
import { IMilestone, Milestone } from '../../milestone/milestone.model';
import { MilestoneService } from '../../milestone/service/milestone.service';
import { IAttachment } from '../../attachment/attachment.model';
import { AttachmentService } from '../../attachment/service/attachment.service';

@Component({
  selector: 'jhi-project-detail',
  templateUrl: './project-detail.component.html',
})
export class ProjectDetailComponent implements OnInit {
  project: IProject | null = null;
  isLoading = false;
  milestones?: IMilestone[];
  markers: any;
  attachments?: IAttachment[];

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected attachmentService: AttachmentService,
    protected milestoneService: MilestoneService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ project }) => {
      this.project = project;
      this.markers = [
        {
          lat: this.project?.lat,
          lng: this.project?.lng,
          draggable: false,
        },
      ];

      this.milestoneService
        .query({
          'projectId.equals': this.project?.id,
        })
        .subscribe((res: HttpResponse<IMilestone[]>) => {
          this.isLoading = false;
          this.milestones = res.body ?? [];
        });

      this.attachmentService
        .query({
          'projectId.equals': this.project?.id,
        })
        .subscribe((res: HttpResponse<IAttachment[]>) => {
          this.isLoading = false;
          this.attachments = res.body ?? [];
        });
    });
  }

  openFile(fileUrl: any): void {
    window.open('/api/public/file/download/' + String(fileUrl), '_blank');
  }

  previousState(): void {
    window.history.back();
  }
}
