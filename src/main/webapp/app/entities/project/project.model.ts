import { IBusinessField } from 'app/entities/business-field/business-field.model';
import { ICity } from 'app/entities/city/city.model';
import { ICountry } from 'app/entities/country/country.model';
import { IInvestor } from 'app/entities/investor/investor.model';
import { IOrginization } from 'app/entities/orginization/orginization.model';
import { Currency } from 'app/entities/enumerations/currency.model';
import { ProjectStatus } from 'app/entities/enumerations/project-status.model';

export interface IProject {
  id?: number;
  projectNo?: string | null;
  name?: string | null;
  nameEn?: string | null;
  details?: string | null;
  detailsEn?: string | null;
  address?: string | null;
  estimatedCost?: number | null;
  estimatedCostCurrency?: Currency | null;
  projectStatus?: ProjectStatus | null;
  lat?: number | null;
  lng?: number | null;
  businessField?: IBusinessField | null;
  city?: ICity | null;
  country?: ICountry | null;
  investor?: IInvestor | null;
  orginization?: IOrginization | null;
}

export class Project implements IProject {
  constructor(
    public id?: number,
    public projectNo?: string | null,
    public name?: string | null,
    public nameEn?: string | null,
    public details?: string | null,
    public detailsEn?: string | null,
    public address?: string | null,
    public estimatedCost?: number | null,
    public estimatedCostCurrency?: Currency | null,
    public projectStatus?: ProjectStatus | null,
    public lat?: number | null,
    public lng?: number | null,
    public businessField?: IBusinessField | null,
    public city?: ICity | null,
    public country?: ICountry | null,
    public investor?: IInvestor | null,
    public orginization?: IOrginization | null
  ) {}
}

export function getProjectIdentifier(project: IProject): number | undefined {
  return project.id;
}
