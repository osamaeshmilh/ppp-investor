import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IProject, Project } from '../project.model';
import { ProjectService } from '../service/project.service';
import { IBusinessField } from 'app/entities/business-field/business-field.model';
import { BusinessFieldService } from 'app/entities/business-field/service/business-field.service';
import { ICity } from 'app/entities/city/city.model';
import { CityService } from 'app/entities/city/service/city.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { IInvestor } from 'app/entities/investor/investor.model';
import { InvestorService } from 'app/entities/investor/service/investor.service';
import { IOrginization } from 'app/entities/orginization/orginization.model';
import { OrginizationService } from 'app/entities/orginization/service/orginization.service';
import { Currency } from 'app/entities/enumerations/currency.model';
import { ProjectStatus } from 'app/entities/enumerations/project-status.model';
import { IMilestone, Milestone } from '../../milestone/milestone.model';
import { MilestoneService } from '../../milestone/service/milestone.service';
import swal from 'sweetalert2';

@Component({
  selector: 'jhi-project-update',
  templateUrl: './project-update.component.html',
})
export class ProjectUpdateComponent implements OnInit {
  isSaving = false;
  currencyValues = Object.keys(Currency);
  projectStatusValues = Object.keys(ProjectStatus);

  businessFieldsSharedCollection: IBusinessField[] = [];
  citiesSharedCollection: ICity[] = [];
  countriesSharedCollection: ICountry[] = [];
  investorsSharedCollection: IInvestor[] = [];
  orginizationsSharedCollection: IOrginization[] = [];

  milestones?: IMilestone[];
  isLoading = false;
  project: any;
  milestone: IMilestone = new Milestone();

  markers = [
    {
      lat: 32.87849,
      lng: 13.187364,
      draggable: true,
    },
  ];

  editForm = this.fb.group({
    id: [],
    projectNo: [],
    name: [],
    nameEn: [],
    details: [],
    detailsEn: [],
    address: [],
    estimatedCost: [],
    estimatedCostCurrency: [],
    projectStatus: [],
    lat: [],
    lng: [],
    businessField: [],
    city: [],
    country: [],
    investor: [],
    orginization: [],
  });

  constructor(
    protected projectService: ProjectService,
    protected businessFieldService: BusinessFieldService,
    protected cityService: CityService,
    protected countryService: CountryService,
    protected investorService: InvestorService,
    protected orginizationService: OrginizationService,
    protected activatedRoute: ActivatedRoute,
    protected milestoneService: MilestoneService,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ project }) => {
      this.updateForm(project);
      this.project = project;

      this.markers[0].lat = project.lat;
      this.markers[0].lng = project.lng;

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const project = this.createFromForm();
    if (project.id !== undefined) {
      this.subscribeToSaveResponse(this.projectService.update(project));
    } else {
      this.subscribeToSaveResponse(this.projectService.create(project));
    }
  }

  trackBusinessFieldById(index: number, item: IBusinessField): number {
    return item.id!;
  }

  trackCityById(index: number, item: ICity): number {
    return item.id!;
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackInvestorById(index: number, item: IInvestor): number {
    return item.id!;
  }

  trackOrginizationById(index: number, item: IOrginization): number {
    return item.id!;
  }

  deleteMilestone(id: any): void {
    swal
      .fire({
        icon: 'info',
        title: 'تنبيه !',
        text: 'هل تريد تأكيد حذف العنصر ؟',
        confirmButtonText: 'نعم',
        showCancelButton: true,
        cancelButtonText: 'لا',
      })
      .then(finish => {
        if (finish.value) {
          this.milestoneService.delete(id).subscribe(
            deleted => {
              if (deleted.ok) {
                this.milestoneService
                  .query({
                    'projectId.equals': this.project?.id,
                  })
                  .subscribe((res: HttpResponse<IMilestone[]>) => (this.milestones = res.body ?? []));
                swal.fire({
                  icon: 'success',
                  title: 'تم الحدف !',
                  text: 'تم الحذف بنجاح !',
                  confirmButtonText: 'موافق',
                });
              }
            },
            (err: HttpErrorResponse) => {
              swal
                .fire({
                  icon: 'error',
                  title: 'خطأ !',
                  text: err.error.title,
                  confirmButtonText: 'حسنا',
                })
                .then(() => {
                  this.isSaving = false;
                });
            }
          );
        }
      });
  }

  saveMilestone(): void {
    this.isSaving = true;
    this.milestone.project = this.project;
    this.milestoneService.create(this.milestone).subscribe(
      () => {
        this.milestoneService
          .query({
            'projectId.equals': this.project.id,
          })
          .subscribe((res: HttpResponse<IMilestone[]>) => (this.milestones = res.body ?? []));
        this.milestone = new Milestone();
        swal.fire({
          icon: 'success',
          title: 'تم حفظ البيانات !',
          text: 'تمت عملية الاضافة بنجاح !',
          confirmButtonText: 'موافق',
        });
        this.isSaving = false;
      },
      (err: HttpErrorResponse) => {
        swal
          .fire({
            icon: 'error',
            title: 'خطأ !',
            text: err.error.title,
            confirmButtonText: 'حسنا',
          })
          .then(() => {
            this.isSaving = false;
          });
      }
    );
  }

  mapClicked($event: any): void {
    // eslint-disable-next-line
    this.markers[0].lat = $event.coords.lat;
    this.markers[0].lng = $event.coords.lng;
  }

  markerDragEnd(m: any, $event: any): void {
    this.markers[0].lat = $event.coords.lat;
    this.markers[0].lng = $event.coords.lng;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProject>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      res => this.onSaveSuccess(res),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(res: any): void {
    this.project = res.body;
    swal.fire({
      icon: 'success',
      title: 'تم حفظ البيانات !',
      text: ` تمت عملية الحفظ بنجاح `,
      confirmButtonText: 'موافق',
    });
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(project: IProject): void {
    this.editForm.patchValue({
      id: project.id,
      projectNo: project.projectNo,
      name: project.name,
      nameEn: project.nameEn,
      details: project.details,
      detailsEn: project.detailsEn,
      address: project.address,
      estimatedCost: project.estimatedCost,
      estimatedCostCurrency: project.estimatedCostCurrency,
      projectStatus: project.projectStatus,
      lat: project.lat,
      lng: project.lng,
      businessField: project.businessField,
      city: project.city,
      country: project.country,
      investor: project.investor,
      orginization: project.orginization,
    });

    this.businessFieldsSharedCollection = this.businessFieldService.addBusinessFieldToCollectionIfMissing(
      this.businessFieldsSharedCollection,
      project.businessField
    );
    this.citiesSharedCollection = this.cityService.addCityToCollectionIfMissing(this.citiesSharedCollection, project.city);
    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(this.countriesSharedCollection, project.country);
    this.investorsSharedCollection = this.investorService.addInvestorToCollectionIfMissing(
      this.investorsSharedCollection,
      project.investor
    );
    this.orginizationsSharedCollection = this.orginizationService.addOrginizationToCollectionIfMissing(
      this.orginizationsSharedCollection,
      project.orginization
    );
  }

  protected loadRelationshipsOptions(): void {
    this.businessFieldService
      .query()
      .pipe(map((res: HttpResponse<IBusinessField[]>) => res.body ?? []))
      .pipe(
        map((businessFields: IBusinessField[]) =>
          this.businessFieldService.addBusinessFieldToCollectionIfMissing(businessFields, this.editForm.get('businessField')!.value)
        )
      )
      .subscribe((businessFields: IBusinessField[]) => (this.businessFieldsSharedCollection = businessFields));

    this.cityService
      .query()
      .pipe(map((res: HttpResponse<ICity[]>) => res.body ?? []))
      .pipe(map((cities: ICity[]) => this.cityService.addCityToCollectionIfMissing(cities, this.editForm.get('city')!.value)))
      .subscribe((cities: ICity[]) => (this.citiesSharedCollection = cities));

    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.investorService
      .query()
      .pipe(map((res: HttpResponse<IInvestor[]>) => res.body ?? []))
      .pipe(
        map((investors: IInvestor[]) =>
          this.investorService.addInvestorToCollectionIfMissing(investors, this.editForm.get('investor')!.value)
        )
      )
      .subscribe((investors: IInvestor[]) => (this.investorsSharedCollection = investors));

    this.orginizationService
      .query()
      .pipe(map((res: HttpResponse<IOrginization[]>) => res.body ?? []))
      .pipe(
        map((orginizations: IOrginization[]) =>
          this.orginizationService.addOrginizationToCollectionIfMissing(orginizations, this.editForm.get('orginization')!.value)
        )
      )
      .subscribe((orginizations: IOrginization[]) => (this.orginizationsSharedCollection = orginizations));

    this.milestoneService
      .query({
        'projectId.equals': this.project?.id,
      })
      .subscribe((res: HttpResponse<IMilestone[]>) => {
        this.isLoading = false;
        this.milestones = res.body ?? [];
      });
  }

  protected createFromForm(): IProject {
    return {
      ...new Project(),
      id: this.editForm.get(['id'])!.value,
      projectNo: this.editForm.get(['projectNo'])!.value,
      name: this.editForm.get(['name'])!.value,
      nameEn: this.editForm.get(['nameEn'])!.value,
      details: this.editForm.get(['details'])!.value,
      detailsEn: this.editForm.get(['detailsEn'])!.value,
      address: this.editForm.get(['address'])!.value,
      estimatedCost: this.editForm.get(['estimatedCost'])!.value,
      estimatedCostCurrency: this.editForm.get(['estimatedCostCurrency'])!.value,
      projectStatus: this.editForm.get(['projectStatus'])!.value,
      lat: this.markers[0].lat,
      lng: this.markers[0].lng,
      businessField: this.editForm.get(['businessField'])!.value,
      city: this.editForm.get(['city'])!.value,
      country: this.editForm.get(['country'])!.value,
      investor: this.editForm.get(['investor'])!.value,
      orginization: this.editForm.get(['orginization'])!.value,
    };
  }
}
