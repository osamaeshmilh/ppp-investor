import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ProjectComponent } from './list/project.component';
import { ProjectDetailComponent } from './detail/project-detail.component';
import { ProjectUpdateComponent } from './update/project-update.component';
import { ProjectDeleteDialogComponent } from './delete/project-delete-dialog.component';
import { ProjectRoutingModule } from './route/project-routing.module';
import { AgmCoreModule } from '@agm/core';
import { NgxPrintModule } from 'ngx-print';

@NgModule({
  imports: [
    SharedModule,
    ProjectRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDKNsqVQn-1t08BQRIKvIvumVLYnjEQ7J0',
    }),
    NgxPrintModule,
  ],
  declarations: [ProjectComponent, ProjectDetailComponent, ProjectUpdateComponent, ProjectDeleteDialogComponent],
  entryComponents: [ProjectDeleteDialogComponent],
})
export class ProjectModule {}
