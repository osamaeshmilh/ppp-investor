import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
      },
      {
        path: 'investor',
        data: { pageTitle: 'pppInvestorApp.investor.home.title' },
        loadChildren: () => import('./investor/investor.module').then(m => m.InvestorModule),
      },
      {
        path: 'project',
        data: { pageTitle: 'pppInvestorApp.project.home.title' },
        loadChildren: () => import('./project/project.module').then(m => m.ProjectModule),
      },
      {
        path: 'city',
        data: { pageTitle: 'pppInvestorApp.city.home.title' },
        loadChildren: () => import('./city/city.module').then(m => m.CityModule),
      },
      {
        path: 'country',
        data: { pageTitle: 'pppInvestorApp.country.home.title' },
        loadChildren: () => import('./country/country.module').then(m => m.CountryModule),
      },
      {
        path: 'attachment',
        data: { pageTitle: 'pppInvestorApp.attachment.home.title' },
        loadChildren: () => import('./attachment/attachment.module').then(m => m.AttachmentModule),
      },
      {
        path: 'business-field',
        data: { pageTitle: 'pppInvestorApp.businessField.home.title' },
        loadChildren: () => import('./business-field/business-field.module').then(m => m.BusinessFieldModule),
      },
      {
        path: 'orginization',
        data: { pageTitle: 'pppInvestorApp.orginization.home.title' },
        loadChildren: () => import('./orginization/orginization.module').then(m => m.OrginizationModule),
      },
      {
        path: 'bank',
        data: { pageTitle: 'pppInvestorApp.bank.home.title' },
        loadChildren: () => import('./bank/bank.module').then(m => m.BankModule),
      },
      {
        path: 'bank-account',
        data: { pageTitle: 'pppInvestorApp.bankAccount.home.title' },
        loadChildren: () => import('./bank-account/bank-account.module').then(m => m.BankAccountModule),
      },
      {
        path: 'milestone',
        data: { pageTitle: 'pppInvestorApp.milestone.home.title' },
        loadChildren: () => import('./milestone/milestone.module').then(m => m.MilestoneModule),
      },
      {
        path: 'payment-receipt',
        data: { pageTitle: 'pppInvestorApp.paymentReceipt.home.title' },
        loadChildren: () => import('./payment-receipt/payment-receipt.module').then(m => m.PaymentReceiptModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
