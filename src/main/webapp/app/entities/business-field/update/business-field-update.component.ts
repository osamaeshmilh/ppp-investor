import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IBusinessField, BusinessField } from '../business-field.model';
import { BusinessFieldService } from '../service/business-field.service';

@Component({
  selector: 'jhi-business-field-update',
  templateUrl: './business-field-update.component.html',
})
export class BusinessFieldUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    businessFieldNo: [],
    name: [],
    nameAr: [],
    nameEn: [],
  });

  constructor(protected businessFieldService: BusinessFieldService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ businessField }) => {
      this.updateForm(businessField);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const businessField = this.createFromForm();
    if (businessField.id !== undefined) {
      this.subscribeToSaveResponse(this.businessFieldService.update(businessField));
    } else {
      this.subscribeToSaveResponse(this.businessFieldService.create(businessField));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBusinessField>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(businessField: IBusinessField): void {
    this.editForm.patchValue({
      id: businessField.id,
      businessFieldNo: businessField.businessFieldNo,
      name: businessField.name,
      nameAr: businessField.nameAr,
      nameEn: businessField.nameEn,
    });
  }

  protected createFromForm(): IBusinessField {
    return {
      ...new BusinessField(),
      id: this.editForm.get(['id'])!.value,
      businessFieldNo: this.editForm.get(['businessFieldNo'])!.value,
      name: this.editForm.get(['name'])!.value,
      nameAr: this.editForm.get(['nameAr'])!.value,
      nameEn: this.editForm.get(['nameEn'])!.value,
    };
  }
}
