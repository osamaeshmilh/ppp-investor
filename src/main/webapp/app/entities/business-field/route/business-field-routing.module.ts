import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { BusinessFieldComponent } from '../list/business-field.component';
import { BusinessFieldDetailComponent } from '../detail/business-field-detail.component';
import { BusinessFieldUpdateComponent } from '../update/business-field-update.component';
import { BusinessFieldRoutingResolveService } from './business-field-routing-resolve.service';

const businessFieldRoute: Routes = [
  {
    path: '',
    component: BusinessFieldComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BusinessFieldDetailComponent,
    resolve: {
      businessField: BusinessFieldRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BusinessFieldUpdateComponent,
    resolve: {
      businessField: BusinessFieldRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BusinessFieldUpdateComponent,
    resolve: {
      businessField: BusinessFieldRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(businessFieldRoute)],
  exports: [RouterModule],
})
export class BusinessFieldRoutingModule {}
