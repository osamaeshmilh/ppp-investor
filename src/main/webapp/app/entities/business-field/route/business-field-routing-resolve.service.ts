import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBusinessField, BusinessField } from '../business-field.model';
import { BusinessFieldService } from '../service/business-field.service';

@Injectable({ providedIn: 'root' })
export class BusinessFieldRoutingResolveService implements Resolve<IBusinessField> {
  constructor(protected service: BusinessFieldService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBusinessField> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((businessField: HttpResponse<BusinessField>) => {
          if (businessField.body) {
            return of(businessField.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BusinessField());
  }
}
