export interface IBusinessField {
  id?: number;
  businessFieldNo?: string | null;
  name?: string | null;
  nameAr?: string | null;
  nameEn?: string | null;
}

export class BusinessField implements IBusinessField {
  constructor(
    public id?: number,
    public businessFieldNo?: string | null,
    public name?: string | null,
    public nameAr?: string | null,
    public nameEn?: string | null
  ) {}
}

export function getBusinessFieldIdentifier(businessField: IBusinessField): number | undefined {
  return businessField.id;
}
