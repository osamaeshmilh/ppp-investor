import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBusinessField } from '../business-field.model';

@Component({
  selector: 'jhi-business-field-detail',
  templateUrl: './business-field-detail.component.html',
})
export class BusinessFieldDetailComponent implements OnInit {
  businessField: IBusinessField | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ businessField }) => {
      this.businessField = businessField;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
