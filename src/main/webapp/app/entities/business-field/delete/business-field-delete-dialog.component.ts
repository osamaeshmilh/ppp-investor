import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IBusinessField } from '../business-field.model';
import { BusinessFieldService } from '../service/business-field.service';

@Component({
  templateUrl: './business-field-delete-dialog.component.html',
})
export class BusinessFieldDeleteDialogComponent {
  businessField?: IBusinessField;

  constructor(protected businessFieldService: BusinessFieldService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.businessFieldService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
