import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { BusinessFieldComponent } from './list/business-field.component';
import { BusinessFieldDetailComponent } from './detail/business-field-detail.component';
import { BusinessFieldUpdateComponent } from './update/business-field-update.component';
import { BusinessFieldDeleteDialogComponent } from './delete/business-field-delete-dialog.component';
import { BusinessFieldRoutingModule } from './route/business-field-routing.module';

@NgModule({
  imports: [SharedModule, BusinessFieldRoutingModule],
  declarations: [BusinessFieldComponent, BusinessFieldDetailComponent, BusinessFieldUpdateComponent, BusinessFieldDeleteDialogComponent],
  entryComponents: [BusinessFieldDeleteDialogComponent],
})
export class BusinessFieldModule {}
