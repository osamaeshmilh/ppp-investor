import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBusinessField, getBusinessFieldIdentifier } from '../business-field.model';

export type EntityResponseType = HttpResponse<IBusinessField>;
export type EntityArrayResponseType = HttpResponse<IBusinessField[]>;

@Injectable({ providedIn: 'root' })
export class BusinessFieldService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/business-fields');
  protected resourcePublicUrl = this.applicationConfigService.getEndpointFor('api/public/business-fields');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(businessField: IBusinessField): Observable<EntityResponseType> {
    return this.http.post<IBusinessField>(this.resourceUrl, businessField, { observe: 'response' });
  }

  update(businessField: IBusinessField): Observable<EntityResponseType> {
    return this.http.put<IBusinessField>(`${this.resourceUrl}/${getBusinessFieldIdentifier(businessField) as number}`, businessField, {
      observe: 'response',
    });
  }

  partialUpdate(businessField: IBusinessField): Observable<EntityResponseType> {
    return this.http.patch<IBusinessField>(`${this.resourceUrl}/${getBusinessFieldIdentifier(businessField) as number}`, businessField, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBusinessField>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBusinessField[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  queryPublic(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBusinessField[]>(this.resourcePublicUrl, { params: options, observe: 'response' });
  }

  addBusinessFieldToCollectionIfMissing(
    businessFieldCollection: IBusinessField[],
    ...businessFieldsToCheck: (IBusinessField | null | undefined)[]
  ): IBusinessField[] {
    const businessFields: IBusinessField[] = businessFieldsToCheck.filter(isPresent);
    if (businessFields.length > 0) {
      const businessFieldCollectionIdentifiers = businessFieldCollection.map(
        businessFieldItem => getBusinessFieldIdentifier(businessFieldItem)!
      );
      const businessFieldsToAdd = businessFields.filter(businessFieldItem => {
        const businessFieldIdentifier = getBusinessFieldIdentifier(businessFieldItem);
        if (businessFieldIdentifier == null || businessFieldCollectionIdentifiers.includes(businessFieldIdentifier)) {
          return false;
        }
        businessFieldCollectionIdentifiers.push(businessFieldIdentifier);
        return true;
      });
      return [...businessFieldsToAdd, ...businessFieldCollection];
    }
    return businessFieldCollection;
  }
}
