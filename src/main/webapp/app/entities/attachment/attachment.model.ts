import { IInvestor } from 'app/entities/investor/investor.model';
import { IProject } from 'app/entities/project/project.model';
import { AttachmentType } from 'app/entities/enumerations/attachment-type.model';

export interface IAttachment {
  id?: number;
  name?: string | null;
  attachmentType?: AttachmentType | null;
  fileContentType?: string | null;
  file?: string | null;
  fileUrl?: string | null;
  notes?: string | null;
  investor?: IInvestor | null;
  project?: IProject | null;
}

export class Attachment implements IAttachment {
  constructor(
    public id?: number,
    public name?: string | null,
    public attachmentType?: AttachmentType | null,
    public fileContentType?: string | null,
    public file?: string | null,
    public fileUrl?: string | null,
    public notes?: string | null,
    public investor?: IInvestor | null,
    public project?: IProject | null
  ) {}
}

export function getAttachmentIdentifier(attachment: IAttachment): number | undefined {
  return attachment.id;
}
