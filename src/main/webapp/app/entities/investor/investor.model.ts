import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { ICity } from 'app/entities/city/city.model';
import { ICountry } from 'app/entities/country/country.model';
import { IBusinessField } from 'app/entities/business-field/business-field.model';
import { CompanyPurpose } from 'app/entities/enumerations/company-purpose.model';
import { Currency } from 'app/entities/enumerations/currency.model';
import { InvestorStatus } from 'app/entities/enumerations/investor-status.model';
import { IAttachment } from '../attachment/attachment.model';

export interface IInvestor {
  id?: number;
  investorNo?: string | null;
  companyName?: string;
  companyPurpose?: CompanyPurpose | null;
  companyRegisterNo?: string | null;
  companyForeignEmployeeNo?: number | null;
  companyLocalEmployeeNo?: number | null;
  companyTradeChamberNo?: string | null;
  companyEstablishmentDate?: dayjs.Dayjs | null;
  companyMobileNo?: string | null;
  companyEmail?: string | null;
  companyWebsite?: string | null;
  companyAddress?: string | null;
  companyAddressCity?: string | null;
  companyCapital?: string | null;
  companyCapitalText?: string | null;
  companyCapitalCurrency?: Currency | null;
  representativeFullName?: string | null;
  representativeMobileNo?: string | null;
  representativeEmail?: string | null;
  representativeNationalNo?: string | null;
  representativeRegisterNo?: string | null;
  representativePassportNo?: string | null;
  isRepresentativeForeign?: boolean | null;
  notes?: string | null;
  investorStatus?: InvestorStatus | null;
  user?: IUser | null;
  companyCity?: ICity | null;
  companyCountry?: ICountry | null;
  companyAddressCountry?: ICountry | null;
  representativeCity?: ICity | null;
  representativeCountry?: ICountry | null;
  businessField?: IBusinessField | null;
  uploadedAttachments?: IAttachment[] | null;
}

export class Investor implements IInvestor {
  constructor(
    public id?: number,
    public investorNo?: string | null,
    public companyName?: string,
    public companyPurpose?: CompanyPurpose | null,
    public companyRegisterNo?: string | null,
    public companyForeignEmployeeNo?: number | null,
    public companyLocalEmployeeNo?: number | null,
    public companyTradeChamberNo?: string | null,
    public companyEstablishmentDate?: dayjs.Dayjs | null,
    public companyMobileNo?: string | null,
    public companyEmail?: string | null,
    public companyWebsite?: string | null,
    public companyAddress?: string | null,
    public companyAddressCity?: string | null,
    public companyCapital?: string | null,
    public companyCapitalText?: string | null,
    public companyCapitalCurrency?: Currency | null,
    public representativeFullName?: string | null,
    public representativeMobileNo?: string | null,
    public representativeEmail?: string | null,
    public representativeNationalNo?: string | null,
    public representativeRegisterNo?: string | null,
    public representativePassportNo?: string | null,
    public isRepresentativeForeign?: boolean | null,
    public notes?: string | null,
    public investorStatus?: InvestorStatus | null,
    public user?: IUser | null,
    public companyCity?: ICity | null,
    public companyCountry?: ICountry | null,
    public companyAddressCountry?: ICountry | null,
    public representativeCity?: ICity | null,
    public representativeCountry?: ICountry | null,
    public businessField?: IBusinessField | null,
    public uploadedAttachments?: IAttachment[] | null
  ) {
    this.isRepresentativeForeign = this.isRepresentativeForeign ?? false;
  }
}

export function getInvestorIdentifier(investor: IInvestor): number | undefined {
  return investor.id;
}
