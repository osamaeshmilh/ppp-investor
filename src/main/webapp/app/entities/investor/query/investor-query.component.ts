import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HttpResponse } from '@angular/common/http';
import swal from 'sweetalert2';
import { InvestorService } from '../service/investor.service';
import { IInvestor } from '../investor.model';

@Component({
  selector: 'jhi-investor-query',
  templateUrl: './investor-query.component.html',
})
export class InvestorQueryComponent {
  investor: any;
  isLoading: any;
  currentSearch: any;

  constructor(protected activatedRoute: ActivatedRoute, private investorService: InvestorService) {}

  previousState(): void {
    window.history.back();
  }

  clear(): void {
    this.currentSearch = '';
    this.investor = null;
  }

  search(): void {
    this.investor = null;
    this.isLoading = true;
    this.investorService.findPublic(this.currentSearch).subscribe(
      (response: HttpResponse<IInvestor>) => {
        this.investor = response.body;
        this.currentSearch = '';
        this.isLoading = false;
      },
      () => {
        swal
          .fire({
            icon: 'error',
            title: 'خطأ !',
            text: 'رقم تسجيل غير صحيح!',
            confirmButtonText: 'حسنا',
          })
          .then(() => {
            this.currentSearch = '';
            this.isLoading = false;
          });
      }
    );
  }
}
