import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IInvestor, Investor } from '../investor.model';
import { InvestorService } from '../service/investor.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { ICity } from 'app/entities/city/city.model';
import { CityService } from 'app/entities/city/service/city.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { IBusinessField } from 'app/entities/business-field/business-field.model';
import { BusinessFieldService } from 'app/entities/business-field/service/business-field.service';
import { CompanyPurpose } from 'app/entities/enumerations/company-purpose.model';
import { Currency } from 'app/entities/enumerations/currency.model';
import { InvestorStatus } from 'app/entities/enumerations/investor-status.model';

@Component({
  selector: 'jhi-investor-update',
  templateUrl: './investor-update.component.html',
})
export class InvestorUpdateComponent implements OnInit {
  isSaving = false;
  companyPurposeValues = Object.keys(CompanyPurpose);
  currencyValues = Object.keys(Currency);
  investorStatusValues = Object.keys(InvestorStatus);

  usersSharedCollection: IUser[] = [];
  citiesSharedCollection: ICity[] = [];
  countriesSharedCollection: ICountry[] = [];
  businessFieldsSharedCollection: IBusinessField[] = [];

  editForm = this.fb.group({
    id: [],
    investorNo: [],
    companyName: [null, [Validators.required]],
    companyPurpose: [],
    companyRegisterNo: [],
    companyForeignEmployeeNo: [],
    companyLocalEmployeeNo: [],
    companyTradeChamberNo: [],
    companyEstablishmentDate: [],
    companyMobileNo: [],
    companyEmail: [],
    companyWebsite: [],
    companyAddress: [],
    companyCapital: [],
    companyCapitalText: [],
    companyCapitalCurrency: [],
    representativeFullName: [],
    representativeMobileNo: [],
    representativeNationalNo: [],
    representativeRegisterNo: [],
    representativePassportNo: [],
    isRepresentativeForeign: [],
    investorStatus: [],
    user: [],
    companyCity: [],
    companyCountry: [],
    representativeCity: [],
    representativeCountry: [],
    businessField: [],
  });

  constructor(
    protected investorService: InvestorService,
    protected userService: UserService,
    protected cityService: CityService,
    protected countryService: CountryService,
    protected businessFieldService: BusinessFieldService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ investor }) => {
      this.updateForm(investor);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const investor = this.createFromForm();
    if (investor.id !== undefined) {
      this.subscribeToSaveResponse(this.investorService.update(investor));
    } else {
      this.subscribeToSaveResponse(this.investorService.create(investor));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  trackCityById(index: number, item: ICity): number {
    return item.id!;
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackBusinessFieldById(index: number, item: IBusinessField): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInvestor>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(investor: IInvestor): void {
    this.editForm.patchValue({
      id: investor.id,
      investorNo: investor.investorNo,
      companyName: investor.companyName,
      companyPurpose: investor.companyPurpose,
      companyRegisterNo: investor.companyRegisterNo,
      companyForeignEmployeeNo: investor.companyForeignEmployeeNo,
      companyLocalEmployeeNo: investor.companyLocalEmployeeNo,
      companyTradeChamberNo: investor.companyTradeChamberNo,
      companyEstablishmentDate: investor.companyEstablishmentDate,
      companyMobileNo: investor.companyMobileNo,
      companyEmail: investor.companyEmail,
      companyWebsite: investor.companyWebsite,
      companyAddress: investor.companyAddress,
      companyCapital: investor.companyCapital,
      companyCapitalText: investor.companyCapitalText,
      companyCapitalCurrency: investor.companyCapitalCurrency,
      representativeFullName: investor.representativeFullName,
      representativeMobileNo: investor.representativeMobileNo,
      representativeNationalNo: investor.representativeNationalNo,
      representativeRegisterNo: investor.representativeRegisterNo,
      representativePassportNo: investor.representativePassportNo,
      isRepresentativeForeign: investor.isRepresentativeForeign,
      investorStatus: investor.investorStatus,
      user: investor.user,
      companyCity: investor.companyCity,
      companyCountry: investor.companyCountry,
      representativeCity: investor.representativeCity,
      representativeCountry: investor.representativeCountry,
      businessField: investor.businessField,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, investor.user);
    this.citiesSharedCollection = this.cityService.addCityToCollectionIfMissing(
      this.citiesSharedCollection,
      investor.companyCity,
      investor.representativeCity
    );
    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(
      this.countriesSharedCollection,
      investor.companyCountry,
      investor.representativeCountry
    );
    this.businessFieldsSharedCollection = this.businessFieldService.addBusinessFieldToCollectionIfMissing(
      this.businessFieldsSharedCollection,
      investor.businessField
    );
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));

    this.cityService
      .query()
      .pipe(map((res: HttpResponse<ICity[]>) => res.body ?? []))
      .pipe(
        map((cities: ICity[]) =>
          this.cityService.addCityToCollectionIfMissing(
            cities,
            this.editForm.get('companyCity')!.value,
            this.editForm.get('representativeCity')!.value
          )
        )
      )
      .subscribe((cities: ICity[]) => (this.citiesSharedCollection = cities));

    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) =>
          this.countryService.addCountryToCollectionIfMissing(
            countries,
            this.editForm.get('companyCountry')!.value,
            this.editForm.get('representativeCountry')!.value
          )
        )
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.businessFieldService
      .query()
      .pipe(map((res: HttpResponse<IBusinessField[]>) => res.body ?? []))
      .pipe(
        map((businessFields: IBusinessField[]) =>
          this.businessFieldService.addBusinessFieldToCollectionIfMissing(businessFields, this.editForm.get('businessField')!.value)
        )
      )
      .subscribe((businessFields: IBusinessField[]) => (this.businessFieldsSharedCollection = businessFields));
  }

  protected createFromForm(): IInvestor {
    return {
      ...new Investor(),
      id: this.editForm.get(['id'])!.value,
      investorNo: this.editForm.get(['investorNo'])!.value,
      companyName: this.editForm.get(['companyName'])!.value,
      companyPurpose: this.editForm.get(['companyPurpose'])!.value,
      companyRegisterNo: this.editForm.get(['companyRegisterNo'])!.value,
      companyForeignEmployeeNo: this.editForm.get(['companyForeignEmployeeNo'])!.value,
      companyLocalEmployeeNo: this.editForm.get(['companyLocalEmployeeNo'])!.value,
      companyTradeChamberNo: this.editForm.get(['companyTradeChamberNo'])!.value,
      companyEstablishmentDate: this.editForm.get(['companyEstablishmentDate'])!.value,
      companyMobileNo: this.editForm.get(['companyMobileNo'])!.value,
      companyEmail: this.editForm.get(['companyEmail'])!.value,
      companyWebsite: this.editForm.get(['companyWebsite'])!.value,
      companyAddress: this.editForm.get(['companyAddress'])!.value,
      companyCapital: this.editForm.get(['companyCapital'])!.value,
      companyCapitalText: this.editForm.get(['companyCapitalText'])!.value,
      companyCapitalCurrency: this.editForm.get(['companyCapitalCurrency'])!.value,
      representativeFullName: this.editForm.get(['representativeFullName'])!.value,
      representativeMobileNo: this.editForm.get(['representativeMobileNo'])!.value,
      representativeNationalNo: this.editForm.get(['representativeNationalNo'])!.value,
      representativeRegisterNo: this.editForm.get(['representativeRegisterNo'])!.value,
      representativePassportNo: this.editForm.get(['representativePassportNo'])!.value,
      isRepresentativeForeign: this.editForm.get(['isRepresentativeForeign'])!.value,
      investorStatus: this.editForm.get(['investorStatus'])!.value,
      user: this.editForm.get(['user'])!.value,
      companyCity: this.editForm.get(['companyCity'])!.value,
      companyCountry: this.editForm.get(['companyCountry'])!.value,
      representativeCity: this.editForm.get(['representativeCity'])!.value,
      representativeCountry: this.editForm.get(['representativeCountry'])!.value,
      businessField: this.editForm.get(['businessField'])!.value,
    };
  }
}
