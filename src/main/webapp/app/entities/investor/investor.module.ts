import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { InvestorComponent } from './list/investor.component';
import { InvestorDetailComponent } from './detail/investor-detail.component';
import { InvestorUpdateComponent } from './update/investor-update.component';
import { InvestorDeleteDialogComponent } from './delete/investor-delete-dialog.component';
import { InvestorRoutingModule } from './route/investor-routing.module';
import { InvestorRegisterComponent } from './register/investor-register.component';
import { InvestorQueryComponent } from './query/investor-query.component';

@NgModule({
  imports: [SharedModule, InvestorRoutingModule],
  declarations: [
    InvestorComponent,
    InvestorDetailComponent,
    InvestorUpdateComponent,
    InvestorRegisterComponent,
    InvestorDeleteDialogComponent,
    InvestorQueryComponent,
  ],
  entryComponents: [InvestorDeleteDialogComponent],
})
export class InvestorModule {}
