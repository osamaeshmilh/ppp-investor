import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInvestor } from '../investor.model';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IMilestone } from '../../milestone/milestone.model';
import { AttachmentService } from '../../attachment/service/attachment.service';
import { ProjectService } from '../../project/service/project.service';
import { IProject } from '../../project/project.model';
import { IAttachment } from '../../attachment/attachment.model';
import { IBankAccount } from '../../bank-account/bank-account.model';
import { BankAccountService } from '../../bank-account/service/bank-account.service';
import swal from 'sweetalert2';
import { InvestorService } from '../service/investor.service';

@Component({
  selector: 'jhi-investor-detail',
  templateUrl: './investor-detail.component.html',
})
export class InvestorDetailComponent implements OnInit {
  investor: IInvestor | null = null;
  isLoading: any;
  projects?: IProject[];
  attachments?: IAttachment[];
  bankAccounts?: IBankAccount[];
  isSaving: any;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected attachmentService: AttachmentService,
    protected bankAccountService: BankAccountService,
    protected investorService: InvestorService,
    protected projectService: ProjectService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ investor }) => {
      this.investor = investor;

      this.projectService
        .query({
          'investorId.equals': this.investor?.id,
        })
        .subscribe((res: HttpResponse<IProject[]>) => {
          this.isLoading = false;
          this.projects = res.body ?? [];
        });

      this.bankAccountService
        .query({
          'investorId.equals': this.investor?.id,
        })
        .subscribe((res: HttpResponse<IBankAccount[]>) => {
          this.isLoading = false;
          this.bankAccounts = res.body ?? [];
        });

      this.attachmentService
        .query({
          'investorId.equals': this.investor?.id,
        })
        .subscribe((res: HttpResponse<IAttachment[]>) => {
          this.isLoading = false;
          this.attachments = res.body ?? [];
        });
    });
  }

  previousState(): void {
    window.history.back();
  }

  openFile(fileUrl: any): void {
    window.open('/api/public/file/download/' + String(fileUrl), '_blank');
  }

  approveInvestor(id: any): void {
    swal
      .fire({
        icon: 'info',
        title: 'تنبيه هام !',
        text: 'سيتم تأكيد البيانات واصدار رقم مستثمر , هل تريد الاستمرار ؟',
        confirmButtonText: 'نعم',
        showCancelButton: true,
        cancelButtonText: 'لا',
      })
      .then(finish => {
        if (finish.value) {
          this.isSaving = true;
          swal.fire({
            title: 'جاري الارسال !',
            text: 'الرجاء الانتظار ...',
            showConfirmButton: false,
          });
          this.investorService.approve(id).subscribe(
            (res: HttpResponse<IInvestor>) => {
              this.isLoading = false;
              this.investor = res.body ?? null;
              swal
                .fire({
                  icon: 'success',
                  title: 'تمت الموافقة !',
                  text: 'تمت الموافقة واصدار الرقم بنجاح !',
                  confirmButtonText: 'موافق',
                })
                .then(() => {
                  this.isLoading = false;
                });
            },
            (err: HttpErrorResponse) => {
              swal
                .fire({
                  icon: 'error',
                  title: 'خطأ !',
                  text: err.error.title,
                  confirmButtonText: 'حسنا',
                })
                .then(() => {
                  this.isSaving = false;
                });
            }
          );
        }
      });
  }
}
