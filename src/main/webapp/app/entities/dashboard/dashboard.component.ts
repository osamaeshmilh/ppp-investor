import { Component } from '@angular/core';
import { InvestorService } from '../investor/service/investor.service';
import { ProjectService } from '../project/service/project.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'jhi-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {
  public companyPurposeDoughnutChartLabels = ['استيراد', 'تصدير', 'خدمات', 'صناعة', 'تأمين', 'مصرف', 'غير محدد'];
  public companyPurposeDoughnutChartData: any;
  public companyPurposeData: Array<any> = [];

  public projectStatusPieChartLabels = ['انتظار المراجعة', 'تم الموافقة', 'مشروع قائم', 'تم تجهيز المشروع', 'تحت التطوير'];
  public projectStatusPieChartData: any;
  public projectStatusData: Array<any> = [];

  pendingInvestorCount: any;
  approvedInvestorCount: any;
  investorCount: any;
  projectsCount: any;

  constructor(private investorService: InvestorService, private projectService: ProjectService) {}

  ngOnInit(): void {
    this.investorService
      .count({
        'investorStatus.equals': 'APPROVED',
      })
      .subscribe((res: any) => {
        this.approvedInvestorCount = res.body;
      });
    this.investorService
      .count({
        'investorStatus.equals': 'PENDING',
      })
      .subscribe((res: any) => {
        this.pendingInvestorCount = res.body;
      });
    this.investorService.count().subscribe((res: any) => {
      this.investorCount = res.body;
    });
    this.projectService.count().subscribe((res: any) => {
      this.projectsCount = res.body;
    });

    forkJoin([
      this.projectService.count({ 'projectStatus.equals': 'PENDING' }),
      this.projectService.count({ 'projectStatus.equals': 'APPROVED' }),
      this.projectService.count({ 'projectStatus.equals': 'ACTIVE' }),
      this.projectService.count({ 'projectStatus.equals': 'READY_NOT_ACTIVE' }),
      this.projectService.count({ 'projectStatus.equals': 'UNDER_DEVELOPMENT' }),
    ]).subscribe(data => {
      this.projectStatusData.push(data[0].body);
      this.projectStatusData.push(data[1].body);
      this.projectStatusData.push(data[2].body);
      this.projectStatusData.push(data[3].body);
      this.projectStatusData.push(data[4].body);
      this.projectStatusPieChartData = this.projectStatusData;
    });

    forkJoin([
      this.investorService.count({ 'companyPurpose.equals': 'IMPORT' }),
      this.investorService.count({ 'companyPurpose.equals': 'EXPORT' }),
      this.investorService.count({ 'companyPurpose.equals': 'SERVICE' }),
      this.investorService.count({ 'companyPurpose.equals': 'MANUFACTURE' }),
      this.investorService.count({ 'companyPurpose.equals': 'INSURANCE' }),
      this.investorService.count({ 'companyPurpose.equals': 'BANKING' }),
      this.investorService.count({ 'companyPurpose.equals': 'NONE' }),
    ]).subscribe(data => {
      this.companyPurposeData.push(data[0].body);
      this.companyPurposeData.push(data[1].body);
      this.companyPurposeData.push(data[2].body);
      this.companyPurposeData.push(data[3].body);
      this.companyPurposeData.push(data[4].body);
      this.companyPurposeData.push(data[5].body);
      this.companyPurposeData.push(data[6].body);
      this.companyPurposeDoughnutChartData = this.companyPurposeData;
    });
  }
}
