export interface ICountry {
  id?: number;
  name?: string | null;
  nameAr?: string | null;
  nameEn?: string | null;
  code?: string | null;
}

export class Country implements ICountry {
  constructor(
    public id?: number,
    public name?: string | null,
    public nameAr?: string | null,
    public nameEn?: string | null,
    public code?: string | null
  ) {}
}

export function getCountryIdentifier(country: ICountry): number | undefined {
  return country.id;
}
