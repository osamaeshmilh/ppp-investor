import { IUser } from 'app/entities/user/user.model';

export interface IBank {
  id?: number;
  name?: string | null;
  user?: IUser | null;
}

export class Bank implements IBank {
  constructor(public id?: number, public name?: string | null, public user?: IUser | null) {}
}

export function getBankIdentifier(bank: IBank): number | undefined {
  return bank.id;
}
