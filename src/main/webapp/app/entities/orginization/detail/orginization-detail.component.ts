import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrginization } from '../orginization.model';

@Component({
  selector: 'jhi-orginization-detail',
  templateUrl: './orginization-detail.component.html',
})
export class OrginizationDetailComponent implements OnInit {
  orginization: IOrginization | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orginization }) => {
      this.orginization = orginization;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
