import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { OrginizationComponent } from '../list/orginization.component';
import { OrginizationDetailComponent } from '../detail/orginization-detail.component';
import { OrginizationUpdateComponent } from '../update/orginization-update.component';
import { OrginizationRoutingResolveService } from './orginization-routing-resolve.service';

const orginizationRoute: Routes = [
  {
    path: '',
    component: OrginizationComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OrginizationDetailComponent,
    resolve: {
      orginization: OrginizationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OrginizationUpdateComponent,
    resolve: {
      orginization: OrginizationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OrginizationUpdateComponent,
    resolve: {
      orginization: OrginizationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(orginizationRoute)],
  exports: [RouterModule],
})
export class OrginizationRoutingModule {}
