import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOrginization, Orginization } from '../orginization.model';
import { OrginizationService } from '../service/orginization.service';

@Injectable({ providedIn: 'root' })
export class OrginizationRoutingResolveService implements Resolve<IOrginization> {
  constructor(protected service: OrginizationService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrginization> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((orginization: HttpResponse<Orginization>) => {
          if (orginization.body) {
            return of(orginization.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Orginization());
  }
}
