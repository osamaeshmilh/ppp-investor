import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IOrginization, Orginization } from '../orginization.model';
import { OrginizationService } from '../service/orginization.service';

@Component({
  selector: 'jhi-orginization-update',
  templateUrl: './orginization-update.component.html',
})
export class OrginizationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    description: [],
    address: [],
  });

  constructor(protected orginizationService: OrginizationService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ orginization }) => {
      this.updateForm(orginization);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const orginization = this.createFromForm();
    if (orginization.id !== undefined) {
      this.subscribeToSaveResponse(this.orginizationService.update(orginization));
    } else {
      this.subscribeToSaveResponse(this.orginizationService.create(orginization));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrginization>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(orginization: IOrginization): void {
    this.editForm.patchValue({
      id: orginization.id,
      name: orginization.name,
      description: orginization.description,
      address: orginization.address,
    });
  }

  protected createFromForm(): IOrginization {
    return {
      ...new Orginization(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      address: this.editForm.get(['address'])!.value,
    };
  }
}
