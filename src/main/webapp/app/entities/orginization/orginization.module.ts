import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { OrginizationComponent } from './list/orginization.component';
import { OrginizationDetailComponent } from './detail/orginization-detail.component';
import { OrginizationUpdateComponent } from './update/orginization-update.component';
import { OrginizationDeleteDialogComponent } from './delete/orginization-delete-dialog.component';
import { OrginizationRoutingModule } from './route/orginization-routing.module';

@NgModule({
  imports: [SharedModule, OrginizationRoutingModule],
  declarations: [OrginizationComponent, OrginizationDetailComponent, OrginizationUpdateComponent, OrginizationDeleteDialogComponent],
  entryComponents: [OrginizationDeleteDialogComponent],
})
export class OrginizationModule {}
