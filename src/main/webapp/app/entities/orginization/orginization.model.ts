export interface IOrginization {
  id?: number;
  name?: string | null;
  description?: string | null;
  address?: string | null;
}

export class Orginization implements IOrginization {
  constructor(public id?: number, public name?: string | null, public description?: string | null, public address?: string | null) {}
}

export function getOrginizationIdentifier(orginization: IOrginization): number | undefined {
  return orginization.id;
}
