import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOrginization, getOrginizationIdentifier } from '../orginization.model';

export type EntityResponseType = HttpResponse<IOrginization>;
export type EntityArrayResponseType = HttpResponse<IOrginization[]>;

@Injectable({ providedIn: 'root' })
export class OrginizationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/orginizations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(orginization: IOrginization): Observable<EntityResponseType> {
    return this.http.post<IOrginization>(this.resourceUrl, orginization, { observe: 'response' });
  }

  update(orginization: IOrginization): Observable<EntityResponseType> {
    return this.http.put<IOrginization>(`${this.resourceUrl}/${getOrginizationIdentifier(orginization) as number}`, orginization, {
      observe: 'response',
    });
  }

  partialUpdate(orginization: IOrginization): Observable<EntityResponseType> {
    return this.http.patch<IOrginization>(`${this.resourceUrl}/${getOrginizationIdentifier(orginization) as number}`, orginization, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOrginization>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOrginization[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addOrginizationToCollectionIfMissing(
    orginizationCollection: IOrginization[],
    ...orginizationsToCheck: (IOrginization | null | undefined)[]
  ): IOrginization[] {
    const orginizations: IOrginization[] = orginizationsToCheck.filter(isPresent);
    if (orginizations.length > 0) {
      const orginizationCollectionIdentifiers = orginizationCollection.map(
        orginizationItem => getOrginizationIdentifier(orginizationItem)!
      );
      const orginizationsToAdd = orginizations.filter(orginizationItem => {
        const orginizationIdentifier = getOrginizationIdentifier(orginizationItem);
        if (orginizationIdentifier == null || orginizationCollectionIdentifiers.includes(orginizationIdentifier)) {
          return false;
        }
        orginizationCollectionIdentifiers.push(orginizationIdentifier);
        return true;
      });
      return [...orginizationsToAdd, ...orginizationCollection];
    }
    return orginizationCollection;
  }
}
