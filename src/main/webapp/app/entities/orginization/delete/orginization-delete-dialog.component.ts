import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IOrginization } from '../orginization.model';
import { OrginizationService } from '../service/orginization.service';

@Component({
  templateUrl: './orginization-delete-dialog.component.html',
})
export class OrginizationDeleteDialogComponent {
  orginization?: IOrginization;

  constructor(protected orginizationService: OrginizationService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.orginizationService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
