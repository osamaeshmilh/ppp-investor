import { IBank } from 'app/entities/bank/bank.model';
import { IInvestor } from 'app/entities/investor/investor.model';

export interface IBankAccount {
  id?: number;
  accountNo?: string | null;
  bankBranchName?: string | null;
  notes?: string | null;
  bank?: IBank | null;
  investor?: IInvestor | null;
}

export class BankAccount implements IBankAccount {
  constructor(
    public id?: number,
    public accountNo?: string | null,
    public bankBranchName?: string | null,
    public notes?: string | null,
    public bank?: IBank | null,
    public investor?: IInvestor | null
  ) {}
}

export function getBankAccountIdentifier(bankAccount: IBankAccount): number | undefined {
  return bankAccount.id;
}
