import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IBankAccount, BankAccount } from '../bank-account.model';
import { BankAccountService } from '../service/bank-account.service';
import { IBank } from 'app/entities/bank/bank.model';
import { BankService } from 'app/entities/bank/service/bank.service';
import { IInvestor } from 'app/entities/investor/investor.model';
import { InvestorService } from 'app/entities/investor/service/investor.service';

@Component({
  selector: 'jhi-bank-account-update',
  templateUrl: './bank-account-update.component.html',
})
export class BankAccountUpdateComponent implements OnInit {
  isSaving = false;

  banksSharedCollection: IBank[] = [];
  investorsSharedCollection: IInvestor[] = [];

  editForm = this.fb.group({
    id: [],
    accountNo: [],
    bankBranchName: [],
    notes: [],
    bank: [],
    investor: [],
  });

  constructor(
    protected bankAccountService: BankAccountService,
    protected bankService: BankService,
    protected investorService: InvestorService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bankAccount }) => {
      this.updateForm(bankAccount);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bankAccount = this.createFromForm();
    if (bankAccount.id !== undefined) {
      this.subscribeToSaveResponse(this.bankAccountService.update(bankAccount));
    } else {
      this.subscribeToSaveResponse(this.bankAccountService.create(bankAccount));
    }
  }

  trackBankById(index: number, item: IBank): number {
    return item.id!;
  }

  trackInvestorById(index: number, item: IInvestor): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBankAccount>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(bankAccount: IBankAccount): void {
    this.editForm.patchValue({
      id: bankAccount.id,
      accountNo: bankAccount.accountNo,
      bankBranchName: bankAccount.bankBranchName,
      notes: bankAccount.notes,
      bank: bankAccount.bank,
      investor: bankAccount.investor,
    });

    this.banksSharedCollection = this.bankService.addBankToCollectionIfMissing(this.banksSharedCollection, bankAccount.bank);
    this.investorsSharedCollection = this.investorService.addInvestorToCollectionIfMissing(
      this.investorsSharedCollection,
      bankAccount.investor
    );
  }

  protected loadRelationshipsOptions(): void {
    this.bankService
      .query()
      .pipe(map((res: HttpResponse<IBank[]>) => res.body ?? []))
      .pipe(map((banks: IBank[]) => this.bankService.addBankToCollectionIfMissing(banks, this.editForm.get('bank')!.value)))
      .subscribe((banks: IBank[]) => (this.banksSharedCollection = banks));

    this.investorService
      .query()
      .pipe(map((res: HttpResponse<IInvestor[]>) => res.body ?? []))
      .pipe(
        map((investors: IInvestor[]) =>
          this.investorService.addInvestorToCollectionIfMissing(investors, this.editForm.get('investor')!.value)
        )
      )
      .subscribe((investors: IInvestor[]) => (this.investorsSharedCollection = investors));
  }

  protected createFromForm(): IBankAccount {
    return {
      ...new BankAccount(),
      id: this.editForm.get(['id'])!.value,
      accountNo: this.editForm.get(['accountNo'])!.value,
      bankBranchName: this.editForm.get(['bankBranchName'])!.value,
      notes: this.editForm.get(['notes'])!.value,
      bank: this.editForm.get(['bank'])!.value,
      investor: this.editForm.get(['investor'])!.value,
    };
  }
}
