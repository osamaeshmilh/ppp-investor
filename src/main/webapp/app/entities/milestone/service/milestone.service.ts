import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMilestone, getMilestoneIdentifier } from '../milestone.model';

export type EntityResponseType = HttpResponse<IMilestone>;
export type EntityArrayResponseType = HttpResponse<IMilestone[]>;

@Injectable({ providedIn: 'root' })
export class MilestoneService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/milestones');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(milestone: IMilestone): Observable<EntityResponseType> {
    return this.http.post<IMilestone>(this.resourceUrl, milestone, { observe: 'response' });
  }

  update(milestone: IMilestone): Observable<EntityResponseType> {
    return this.http.put<IMilestone>(`${this.resourceUrl}/${getMilestoneIdentifier(milestone) as number}`, milestone, {
      observe: 'response',
    });
  }

  partialUpdate(milestone: IMilestone): Observable<EntityResponseType> {
    return this.http.patch<IMilestone>(`${this.resourceUrl}/${getMilestoneIdentifier(milestone) as number}`, milestone, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMilestone>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMilestone[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMilestoneToCollectionIfMissing(
    milestoneCollection: IMilestone[],
    ...milestonesToCheck: (IMilestone | null | undefined)[]
  ): IMilestone[] {
    const milestones: IMilestone[] = milestonesToCheck.filter(isPresent);
    if (milestones.length > 0) {
      const milestoneCollectionIdentifiers = milestoneCollection.map(milestoneItem => getMilestoneIdentifier(milestoneItem)!);
      const milestonesToAdd = milestones.filter(milestoneItem => {
        const milestoneIdentifier = getMilestoneIdentifier(milestoneItem);
        if (milestoneIdentifier == null || milestoneCollectionIdentifiers.includes(milestoneIdentifier)) {
          return false;
        }
        milestoneCollectionIdentifiers.push(milestoneIdentifier);
        return true;
      });
      return [...milestonesToAdd, ...milestoneCollection];
    }
    return milestoneCollection;
  }
}
