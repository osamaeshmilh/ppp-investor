import { IProject } from 'app/entities/project/project.model';

export interface IMilestone {
  id?: number;
  name?: string | null;
  details?: string | null;
  order?: number | null;
  isFinished?: boolean | null;
  notes?: string | null;
  project?: IProject | null;
}

export class Milestone implements IMilestone {
  constructor(
    public id?: number,
    public name?: string | null,
    public details?: string | null,
    public order?: number | null,
    public isFinished?: boolean | null,
    public notes?: string | null,
    public project?: IProject | null
  ) {
    this.isFinished = this.isFinished ?? false;
  }
}

export function getMilestoneIdentifier(milestone: IMilestone): number | undefined {
  return milestone.id;
}
