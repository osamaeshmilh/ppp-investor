package ly.ppp.investor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import ly.ppp.investor.repository.PaymentReceiptRepository;
import ly.ppp.investor.service.PaymentReceiptQueryService;
import ly.ppp.investor.service.PaymentReceiptService;
import ly.ppp.investor.service.criteria.PaymentReceiptCriteria;
import ly.ppp.investor.service.dto.PaymentReceiptDTO;
import ly.ppp.investor.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link ly.ppp.investor.domain.PaymentReceipt}.
 */
@RestController
@RequestMapping("/api")
public class PaymentReceiptResource {

    private final Logger log = LoggerFactory.getLogger(PaymentReceiptResource.class);

    private static final String ENTITY_NAME = "paymentReceipt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentReceiptService paymentReceiptService;

    private final PaymentReceiptRepository paymentReceiptRepository;

    private final PaymentReceiptQueryService paymentReceiptQueryService;

    public PaymentReceiptResource(
        PaymentReceiptService paymentReceiptService,
        PaymentReceiptRepository paymentReceiptRepository,
        PaymentReceiptQueryService paymentReceiptQueryService
    ) {
        this.paymentReceiptService = paymentReceiptService;
        this.paymentReceiptRepository = paymentReceiptRepository;
        this.paymentReceiptQueryService = paymentReceiptQueryService;
    }

    /**
     * {@code POST  /payment-receipts} : Create a new paymentReceipt.
     *
     * @param paymentReceiptDTO the paymentReceiptDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paymentReceiptDTO, or with status {@code 400 (Bad Request)} if the paymentReceipt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/payment-receipts")
    public ResponseEntity<PaymentReceiptDTO> createPaymentReceipt(@RequestBody PaymentReceiptDTO paymentReceiptDTO)
        throws URISyntaxException {
        log.debug("REST request to save PaymentReceipt : {}", paymentReceiptDTO);
        if (paymentReceiptDTO.getId() != null) {
            throw new BadRequestAlertException("A new paymentReceipt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentReceiptDTO result = paymentReceiptService.save(paymentReceiptDTO);
        return ResponseEntity
            .created(new URI("/api/payment-receipts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /payment-receipts/:id} : Updates an existing paymentReceipt.
     *
     * @param id                the id of the paymentReceiptDTO to save.
     * @param paymentReceiptDTO the paymentReceiptDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentReceiptDTO,
     * or with status {@code 400 (Bad Request)} if the paymentReceiptDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paymentReceiptDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/payment-receipts/{id}")
    public ResponseEntity<PaymentReceiptDTO> updatePaymentReceipt(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PaymentReceiptDTO paymentReceiptDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PaymentReceipt : {}, {}", id, paymentReceiptDTO);
        if (paymentReceiptDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, paymentReceiptDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!paymentReceiptRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PaymentReceiptDTO result = paymentReceiptService.save(paymentReceiptDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, paymentReceiptDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /payment-receipts/:id} : Partial updates given fields of an existing paymentReceipt, field will ignore if it is null
     *
     * @param id                the id of the paymentReceiptDTO to save.
     * @param paymentReceiptDTO the paymentReceiptDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentReceiptDTO,
     * or with status {@code 400 (Bad Request)} if the paymentReceiptDTO is not valid,
     * or with status {@code 404 (Not Found)} if the paymentReceiptDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the paymentReceiptDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/payment-receipts/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PaymentReceiptDTO> partialUpdatePaymentReceipt(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PaymentReceiptDTO paymentReceiptDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PaymentReceipt partially : {}, {}", id, paymentReceiptDTO);
        if (paymentReceiptDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, paymentReceiptDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!paymentReceiptRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PaymentReceiptDTO> result = paymentReceiptService.partialUpdate(paymentReceiptDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, paymentReceiptDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /payment-receipts} : get all the paymentReceipts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paymentReceipts in body.
     */
    @GetMapping("/payment-receipts")
    public ResponseEntity<List<PaymentReceiptDTO>> getAllPaymentReceipts(PaymentReceiptCriteria criteria, Pageable pageable) {
        log.debug("REST request to get PaymentReceipts by criteria: {}", criteria);
        Page<PaymentReceiptDTO> page = paymentReceiptQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /payment-receipts/count} : count all the paymentReceipts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/payment-receipts/count")
    public ResponseEntity<Long> countPaymentReceipts(PaymentReceiptCriteria criteria) {
        log.debug("REST request to count PaymentReceipts by criteria: {}", criteria);
        return ResponseEntity.ok().body(paymentReceiptQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /payment-receipts/:id} : get the "id" paymentReceipt.
     *
     * @param id the id of the paymentReceiptDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paymentReceiptDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/payment-receipts/{id}")
    public ResponseEntity<PaymentReceiptDTO> getPaymentReceipt(@PathVariable Long id) {
        log.debug("REST request to get PaymentReceipt : {}", id);
        Optional<PaymentReceiptDTO> paymentReceiptDTO = paymentReceiptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(paymentReceiptDTO);
    }

    /**
     * {@code DELETE  /payment-receipts/:id} : delete the "id" paymentReceipt.
     *
     * @param id the id of the paymentReceiptDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/payment-receipts/{id}")
    public ResponseEntity<Void> deletePaymentReceipt(@PathVariable Long id) {
        log.debug("REST request to delete PaymentReceipt : {}", id);
        paymentReceiptService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping(value = "/public/payment-receipts/xlsx", produces = "application/vnd.ms-excel")
    public ResponseEntity<byte[]> getInvestorsAsXSLX() {
        log.debug("REST request to get xslx");

        byte[] bytes = paymentReceiptService.getAllXslx();

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf("application/vnd.ms-excel"));
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + new Date() + ".xlsx");
        header.setContentLength(bytes.length);

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bytes), header);
    }
}
