/**
 * View Models used by Spring MVC REST controllers.
 */
package ly.ppp.investor.web.rest.vm;
