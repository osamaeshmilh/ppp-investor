package ly.ppp.investor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import ly.ppp.investor.repository.MilestoneRepository;
import ly.ppp.investor.service.MilestoneQueryService;
import ly.ppp.investor.service.MilestoneService;
import ly.ppp.investor.service.criteria.MilestoneCriteria;
import ly.ppp.investor.service.dto.MilestoneDTO;
import ly.ppp.investor.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link ly.ppp.investor.domain.Milestone}.
 */
@RestController
@RequestMapping("/api")
public class MilestoneResource {

    private final Logger log = LoggerFactory.getLogger(MilestoneResource.class);

    private static final String ENTITY_NAME = "milestone";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MilestoneService milestoneService;

    private final MilestoneRepository milestoneRepository;

    private final MilestoneQueryService milestoneQueryService;

    public MilestoneResource(
        MilestoneService milestoneService,
        MilestoneRepository milestoneRepository,
        MilestoneQueryService milestoneQueryService
    ) {
        this.milestoneService = milestoneService;
        this.milestoneRepository = milestoneRepository;
        this.milestoneQueryService = milestoneQueryService;
    }

    /**
     * {@code POST  /milestones} : Create a new milestone.
     *
     * @param milestoneDTO the milestoneDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new milestoneDTO, or with status {@code 400 (Bad Request)} if the milestone has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/milestones")
    public ResponseEntity<MilestoneDTO> createMilestone(@RequestBody MilestoneDTO milestoneDTO) throws URISyntaxException {
        log.debug("REST request to save Milestone : {}", milestoneDTO);
        if (milestoneDTO.getId() != null) {
            throw new BadRequestAlertException("A new milestone cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MilestoneDTO result = milestoneService.save(milestoneDTO);
        return ResponseEntity
            .created(new URI("/api/milestones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /milestones/:id} : Updates an existing milestone.
     *
     * @param id           the id of the milestoneDTO to save.
     * @param milestoneDTO the milestoneDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated milestoneDTO,
     * or with status {@code 400 (Bad Request)} if the milestoneDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the milestoneDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/milestones/{id}")
    public ResponseEntity<MilestoneDTO> updateMilestone(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MilestoneDTO milestoneDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Milestone : {}, {}", id, milestoneDTO);
        if (milestoneDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, milestoneDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!milestoneRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MilestoneDTO result = milestoneService.save(milestoneDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, milestoneDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /milestones/:id} : Partial updates given fields of an existing milestone, field will ignore if it is null
     *
     * @param id the id of the milestoneDTO to save.
     * @param milestoneDTO the milestoneDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated milestoneDTO,
     * or with status {@code 400 (Bad Request)} if the milestoneDTO is not valid,
     * or with status {@code 404 (Not Found)} if the milestoneDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the milestoneDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/milestones/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MilestoneDTO> partialUpdateMilestone(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MilestoneDTO milestoneDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Milestone partially : {}, {}", id, milestoneDTO);
        if (milestoneDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, milestoneDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!milestoneRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MilestoneDTO> result = milestoneService.partialUpdate(milestoneDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, milestoneDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /milestones} : get all the milestones.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of milestones in body.
     */
    @GetMapping("/milestones")
    public ResponseEntity<List<MilestoneDTO>> getAllMilestones(MilestoneCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Milestones by criteria: {}", criteria);
        Page<MilestoneDTO> page = milestoneQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /milestones/count} : count all the milestones.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/milestones/count")
    public ResponseEntity<Long> countMilestones(MilestoneCriteria criteria) {
        log.debug("REST request to count Milestones by criteria: {}", criteria);
        return ResponseEntity.ok().body(milestoneQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /milestones/:id} : get the "id" milestone.
     *
     * @param id the id of the milestoneDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the milestoneDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/milestones/{id}")
    public ResponseEntity<MilestoneDTO> getMilestone(@PathVariable Long id) {
        log.debug("REST request to get Milestone : {}", id);
        Optional<MilestoneDTO> milestoneDTO = milestoneService.findOne(id);
        return ResponseUtil.wrapOrNotFound(milestoneDTO);
    }

    /**
     * {@code DELETE  /milestones/:id} : delete the "id" milestone.
     *
     * @param id the id of the milestoneDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/milestones/{id}")
    public ResponseEntity<Void> deleteMilestone(@PathVariable Long id) {
        log.debug("REST request to delete Milestone : {}", id);
        milestoneService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
