package ly.ppp.investor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import ly.ppp.investor.repository.BusinessFieldRepository;
import ly.ppp.investor.service.BusinessFieldQueryService;
import ly.ppp.investor.service.BusinessFieldService;
import ly.ppp.investor.service.criteria.BusinessFieldCriteria;
import ly.ppp.investor.service.dto.BusinessFieldDTO;
import ly.ppp.investor.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link ly.ppp.investor.domain.BusinessField}.
 */
@RestController
@RequestMapping("/api")
public class BusinessFieldResource {

    private final Logger log = LoggerFactory.getLogger(BusinessFieldResource.class);

    private static final String ENTITY_NAME = "businessField";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BusinessFieldService businessFieldService;

    private final BusinessFieldRepository businessFieldRepository;

    private final BusinessFieldQueryService businessFieldQueryService;

    public BusinessFieldResource(
        BusinessFieldService businessFieldService,
        BusinessFieldRepository businessFieldRepository,
        BusinessFieldQueryService businessFieldQueryService
    ) {
        this.businessFieldService = businessFieldService;
        this.businessFieldRepository = businessFieldRepository;
        this.businessFieldQueryService = businessFieldQueryService;
    }

    /**
     * {@code POST  /business-fields} : Create a new businessField.
     *
     * @param businessFieldDTO the businessFieldDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new businessFieldDTO, or with status {@code 400 (Bad Request)} if the businessField has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/business-fields")
    public ResponseEntity<BusinessFieldDTO> createBusinessField(@RequestBody BusinessFieldDTO businessFieldDTO) throws URISyntaxException {
        log.debug("REST request to save BusinessField : {}", businessFieldDTO);
        if (businessFieldDTO.getId() != null) {
            throw new BadRequestAlertException("A new businessField cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BusinessFieldDTO result = businessFieldService.save(businessFieldDTO);
        return ResponseEntity
            .created(new URI("/api/business-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /business-fields/:id} : Updates an existing businessField.
     *
     * @param id the id of the businessFieldDTO to save.
     * @param businessFieldDTO the businessFieldDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated businessFieldDTO,
     * or with status {@code 400 (Bad Request)} if the businessFieldDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the businessFieldDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/business-fields/{id}")
    public ResponseEntity<BusinessFieldDTO> updateBusinessField(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody BusinessFieldDTO businessFieldDTO
    ) throws URISyntaxException {
        log.debug("REST request to update BusinessField : {}, {}", id, businessFieldDTO);
        if (businessFieldDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, businessFieldDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!businessFieldRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        BusinessFieldDTO result = businessFieldService.save(businessFieldDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, businessFieldDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /business-fields/:id} : Partial updates given fields of an existing businessField, field will ignore if it is null
     *
     * @param id the id of the businessFieldDTO to save.
     * @param businessFieldDTO the businessFieldDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated businessFieldDTO,
     * or with status {@code 400 (Bad Request)} if the businessFieldDTO is not valid,
     * or with status {@code 404 (Not Found)} if the businessFieldDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the businessFieldDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/business-fields/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<BusinessFieldDTO> partialUpdateBusinessField(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody BusinessFieldDTO businessFieldDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update BusinessField partially : {}, {}", id, businessFieldDTO);
        if (businessFieldDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, businessFieldDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!businessFieldRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<BusinessFieldDTO> result = businessFieldService.partialUpdate(businessFieldDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, businessFieldDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /business-fields} : get all the businessFields.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of businessFields in body.
     */
    @GetMapping("/business-fields")
    public ResponseEntity<List<BusinessFieldDTO>> getAllBusinessFields(BusinessFieldCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BusinessFields by criteria: {}", criteria);
        Page<BusinessFieldDTO> page = businessFieldQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /business-fields/count} : count all the businessFields.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/business-fields/count")
    public ResponseEntity<Long> countBusinessFields(BusinessFieldCriteria criteria) {
        log.debug("REST request to count BusinessFields by criteria: {}", criteria);
        return ResponseEntity.ok().body(businessFieldQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /business-fields/:id} : get the "id" businessField.
     *
     * @param id the id of the businessFieldDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the businessFieldDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/business-fields/{id}")
    public ResponseEntity<BusinessFieldDTO> getBusinessField(@PathVariable Long id) {
        log.debug("REST request to get BusinessField : {}", id);
        Optional<BusinessFieldDTO> businessFieldDTO = businessFieldService.findOne(id);
        return ResponseUtil.wrapOrNotFound(businessFieldDTO);
    }

    /**
     * {@code DELETE  /business-fields/:id} : delete the "id" businessField.
     *
     * @param id the id of the businessFieldDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/business-fields/{id}")
    public ResponseEntity<Void> deleteBusinessField(@PathVariable Long id) {
        log.debug("REST request to delete BusinessField : {}", id);
        businessFieldService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/public/business-fields")
    public ResponseEntity<List<BusinessFieldDTO>> getAllBusinessFieldsPublic(BusinessFieldCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BusinessFields by criteria: {}", criteria);
        Page<BusinessFieldDTO> page = businessFieldQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
