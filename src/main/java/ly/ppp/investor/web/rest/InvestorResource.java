package ly.ppp.investor.web.rest;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import ly.ppp.investor.repository.InvestorRepository;
import ly.ppp.investor.security.AuthoritiesConstants;
import ly.ppp.investor.security.SecurityUtils;
import ly.ppp.investor.service.InvestorQueryService;
import ly.ppp.investor.service.InvestorService;
import ly.ppp.investor.service.criteria.InvestorCriteria;
import ly.ppp.investor.service.dto.InvestorDTO;
import ly.ppp.investor.web.rest.errors.BadRequestAlertException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link ly.ppp.investor.domain.Investor}.
 */
@RestController
@RequestMapping("/api")
public class InvestorResource {

    private final Logger log = LoggerFactory.getLogger(InvestorResource.class);

    private static final String ENTITY_NAME = "investor";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InvestorService investorService;

    private final InvestorRepository investorRepository;

    private final InvestorQueryService investorQueryService;

    public InvestorResource(
        InvestorService investorService,
        InvestorRepository investorRepository,
        InvestorQueryService investorQueryService
    ) {
        this.investorService = investorService;
        this.investorRepository = investorRepository;
        this.investorQueryService = investorQueryService;
    }

    /**
     * {@code POST  /investors} : Create a new investor.
     *
     * @param investorDTO the investorDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new investorDTO, or with status {@code 400 (Bad Request)} if the investor has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/investors")
    public ResponseEntity<InvestorDTO> createInvestor(@Valid @RequestBody InvestorDTO investorDTO) throws URISyntaxException {
        log.debug("REST request to save Investor : {}", investorDTO);
        if (investorDTO.getId() != null) {
            throw new BadRequestAlertException("A new investor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InvestorDTO result = investorService.save(investorDTO);
        return ResponseEntity
            .created(new URI("/api/investors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /investors/:id} : Updates an existing investor.
     *
     * @param id the id of the investorDTO to save.
     * @param investorDTO the investorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated investorDTO,
     * or with status {@code 400 (Bad Request)} if the investorDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the investorDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/investors/{id}")
    public ResponseEntity<InvestorDTO> updateInvestor(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody InvestorDTO investorDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Investor : {}, {}", id, investorDTO);
        if (investorDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, investorDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!investorRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        InvestorDTO result = investorService.save(investorDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, investorDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /investors/:id} : Partial updates given fields of an existing investor, field will ignore if it is null
     *
     * @param id the id of the investorDTO to save.
     * @param investorDTO the investorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated investorDTO,
     * or with status {@code 400 (Bad Request)} if the investorDTO is not valid,
     * or with status {@code 404 (Not Found)} if the investorDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the investorDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/investors/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<InvestorDTO> partialUpdateInvestor(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody InvestorDTO investorDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Investor partially : {}, {}", id, investorDTO);
        if (investorDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, investorDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!investorRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<InvestorDTO> result = investorService.partialUpdate(investorDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, investorDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /investors} : get all the investors.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of investors in body.
     */
    @GetMapping("/investors")
    public ResponseEntity<List<InvestorDTO>> getAllInvestors(InvestorCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Investors by criteria: {}", criteria);
        Page<InvestorDTO> page;
        if (SecurityUtils.hasCurrentUserThisAuthority(AuthoritiesConstants.INVESTOR)) {
            LongFilter longFilter = new LongFilter();
            longFilter.setEquals(investorService.findOneByUser().getId());
            criteria.setId(longFilter);
            page = investorQueryService.findByCriteria(criteria, pageable);
        } else if (SecurityUtils.hasCurrentUserThisAuthority(AuthoritiesConstants.ADMIN)) {
            page = investorQueryService.findByCriteria(criteria, pageable);
        } else {
            page = Page.empty();
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /investors/count} : count all the investors.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/investors/count")
    public ResponseEntity<Long> countInvestors(InvestorCriteria criteria) {
        log.debug("REST request to count Investors by criteria: {}", criteria);
        return ResponseEntity.ok().body(investorQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /investors/:id} : get the "id" investor.
     *
     * @param id the id of the investorDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the investorDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/investors/{id}")
    public ResponseEntity<InvestorDTO> getInvestor(@PathVariable Long id) {
        log.debug("REST request to get Investor : {}", id);
        Optional<InvestorDTO> investorDTO = investorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(investorDTO);
    }

    /**
     * {@code DELETE  /investors/:id} : delete the "id" investor.
     *
     * @param id the id of the investorDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/investors/{id}")
    public ResponseEntity<Void> deleteInvestor(@PathVariable Long id) {
        log.debug("REST request to delete Investor : {}", id);
        investorService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    @PostMapping("/public/investors/register")
    public ResponseEntity<InvestorDTO> registerInvestor(@RequestBody InvestorDTO investorDTO) throws URISyntaxException {
        log.debug("REST request to save Investor : {}", investorDTO);
        if (investorDTO.getId() != null) {
            throw new BadRequestAlertException("A new investor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InvestorDTO result = investorService.register(investorDTO);
        return ResponseEntity
            .created(new URI("/api/investors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/public/investors/{id}")
    public ResponseEntity<InvestorDTO> getInvestorPublic(@PathVariable Long id) {
        Optional<InvestorDTO> investorDTO = investorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(investorDTO);
    }

    @GetMapping("/investors/approve/{id}")
    public ResponseEntity<InvestorDTO> approveInvestor(@PathVariable Long id) throws URISyntaxException {
        InvestorDTO investorDTO = investorService.approveInvestor(id);
        return ResponseEntity
            .created(new URI("/api/investors/" + investorDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, investorDTO.getId().toString()))
            .body(investorDTO);
    }

    @GetMapping(value = "/public/investors/xlsx", produces = "application/vnd.ms-excel")
    public ResponseEntity<byte[]> getInvestorsAsXSLX() {
        log.debug("REST request to get xslx");

        byte[] bytes = investorService.getAllXslx();

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.valueOf("application/vnd.ms-excel"));
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + new Date() + ".xlsx");
        header.setContentLength(bytes.length);

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bytes), header);
    }
}
