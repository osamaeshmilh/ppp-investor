package ly.ppp.investor.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import ly.ppp.investor.domain.*; // for static metamodels
import ly.ppp.investor.domain.Orginization;
import ly.ppp.investor.repository.OrginizationRepository;
import ly.ppp.investor.service.criteria.OrginizationCriteria;
import ly.ppp.investor.service.dto.OrginizationDTO;
import ly.ppp.investor.service.mapper.OrginizationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Orginization} entities in the database.
 * The main input is a {@link OrginizationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrginizationDTO} or a {@link Page} of {@link OrginizationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrginizationQueryService extends QueryService<Orginization> {

    private final Logger log = LoggerFactory.getLogger(OrginizationQueryService.class);

    private final OrginizationRepository orginizationRepository;

    private final OrginizationMapper orginizationMapper;

    public OrginizationQueryService(OrginizationRepository orginizationRepository, OrginizationMapper orginizationMapper) {
        this.orginizationRepository = orginizationRepository;
        this.orginizationMapper = orginizationMapper;
    }

    /**
     * Return a {@link List} of {@link OrginizationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrginizationDTO> findByCriteria(OrginizationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Orginization> specification = createSpecification(criteria);
        return orginizationMapper.toDto(orginizationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OrginizationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrginizationDTO> findByCriteria(OrginizationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Orginization> specification = createSpecification(criteria);
        return orginizationRepository.findAll(specification, page).map(orginizationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrginizationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Orginization> specification = createSpecification(criteria);
        return orginizationRepository.count(specification);
    }

    /**
     * Function to convert {@link OrginizationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Orginization> createSpecification(OrginizationCriteria criteria) {
        Specification<Orginization> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Orginization_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Orginization_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Orginization_.description));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), Orginization_.address));
            }
        }
        return specification;
    }
}
