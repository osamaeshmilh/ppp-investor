package ly.ppp.investor.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import ly.ppp.investor.domain.*; // for static metamodels
import ly.ppp.investor.domain.BusinessField;
import ly.ppp.investor.repository.BusinessFieldRepository;
import ly.ppp.investor.service.criteria.BusinessFieldCriteria;
import ly.ppp.investor.service.dto.BusinessFieldDTO;
import ly.ppp.investor.service.mapper.BusinessFieldMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link BusinessField} entities in the database.
 * The main input is a {@link BusinessFieldCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BusinessFieldDTO} or a {@link Page} of {@link BusinessFieldDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BusinessFieldQueryService extends QueryService<BusinessField> {

    private final Logger log = LoggerFactory.getLogger(BusinessFieldQueryService.class);

    private final BusinessFieldRepository businessFieldRepository;

    private final BusinessFieldMapper businessFieldMapper;

    public BusinessFieldQueryService(BusinessFieldRepository businessFieldRepository, BusinessFieldMapper businessFieldMapper) {
        this.businessFieldRepository = businessFieldRepository;
        this.businessFieldMapper = businessFieldMapper;
    }

    /**
     * Return a {@link List} of {@link BusinessFieldDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BusinessFieldDTO> findByCriteria(BusinessFieldCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BusinessField> specification = createSpecification(criteria);
        return businessFieldMapper.toDto(businessFieldRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BusinessFieldDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BusinessFieldDTO> findByCriteria(BusinessFieldCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BusinessField> specification = createSpecification(criteria);
        return businessFieldRepository.findAll(specification, page).map(businessFieldMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BusinessFieldCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BusinessField> specification = createSpecification(criteria);
        return businessFieldRepository.count(specification);
    }

    /**
     * Function to convert {@link BusinessFieldCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BusinessField> createSpecification(BusinessFieldCriteria criteria) {
        Specification<BusinessField> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), BusinessField_.id));
            }
            if (criteria.getBusinessFieldNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBusinessFieldNo(), BusinessField_.businessFieldNo));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), BusinessField_.name));
            }
            if (criteria.getNameAr() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNameAr(), BusinessField_.nameAr));
            }
            if (criteria.getNameEn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNameEn(), BusinessField_.nameEn));
            }
        }
        return specification;
    }
}
