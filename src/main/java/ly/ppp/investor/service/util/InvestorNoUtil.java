package ly.ppp.investor.service.util;

import ly.ppp.investor.web.rest.errors.BadRequestAlertException;

public class InvestorNoUtil {

    public static final int numberLength = 9;
    public static final int numberLengthWithoutCheckDigits = 7;

    public static String CalculateCheckDigits(String number) {
        int T = 0, S = 0, c1, c2;
        StringBuilder resualt = new StringBuilder(number);
        if (!number.matches("[0-9]+") || number.length() != numberLengthWithoutCheckDigits) throw new BadRequestAlertException(
            "الرجاء استخدام ألأرقام فقط مع مراعة طول الرقم 7 خانات لا أكثر لاأقل",
            "",
            "moreThanSix"
        );
        resualt.append("**");
        for (int i = 0; i < number.length(); i++) {
            T += Character.getNumericValue(number.charAt(i));
            S += Character.getNumericValue(number.charAt(i)) * (i + 1);
        }

        c1 = (8 * T + S) % 17;
        c2 = (8 * T + 16 * S) % 17;

        switch (c1) {
            case 10:
                resualt.setCharAt(7, 'A');
                break;
            case 11:
                resualt.setCharAt(7, 'H');
                break;
            case 12:
                resualt.setCharAt(7, 'M');
                break;
            case 13:
                resualt.setCharAt(7, 'E');
                break;
            case 14:
                resualt.setCharAt(7, 'D');
                break;
            case 15:
                resualt.setCharAt(7, 'X');
                break;
            case 16:
                resualt.setCharAt(7, 'Y');
                break;
            default:
                resualt.setCharAt(7, (char) (c1 + '0'));
        }
        switch (c2) {
            case 10:
                resualt.setCharAt(8, 'A');
                break;
            case 11:
                resualt.setCharAt(8, 'H');
                break;
            case 12:
                resualt.setCharAt(8, 'M');
                break;
            case 13:
                resualt.setCharAt(8, 'E');
                break;
            case 14:
                resualt.setCharAt(8, 'D');
                break;
            case 15:
                resualt.setCharAt(8, 'X');
                break;
            case 16:
                resualt.setCharAt(8, 'Y');
                break;
            default:
                resualt.setCharAt(8, (char) (c2 + '0'));
        }
        return resualt.toString();
    }

    public static boolean checkNumberValidity(String number) {
        int T = 0, S = 0;

        if (number.length() != numberLength) return false;

        for (int i = 0; i < number.length(); i++) {
            int value;
            switch (number.charAt(i)) {
                case 'X':
                    value = 10;
                    break;
                case 'Y':
                    value = 11;
                    break;
                case 'Z':
                    value = 12;
                    break;
                default:
                    value = Character.getNumericValue(number.charAt(i));
            }
            T += value;
            S += value * (i + 1);
        }
        return T % 13 == 0 && S % 13 == 0;
    }
}
