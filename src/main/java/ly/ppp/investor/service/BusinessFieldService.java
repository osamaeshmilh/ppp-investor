package ly.ppp.investor.service;

import java.util.Optional;
import ly.ppp.investor.domain.BusinessField;
import ly.ppp.investor.repository.BusinessFieldRepository;
import ly.ppp.investor.service.dto.BusinessFieldDTO;
import ly.ppp.investor.service.mapper.BusinessFieldMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link BusinessField}.
 */
@Service
@Transactional
public class BusinessFieldService {

    private final Logger log = LoggerFactory.getLogger(BusinessFieldService.class);

    private final BusinessFieldRepository businessFieldRepository;

    private final BusinessFieldMapper businessFieldMapper;

    public BusinessFieldService(BusinessFieldRepository businessFieldRepository, BusinessFieldMapper businessFieldMapper) {
        this.businessFieldRepository = businessFieldRepository;
        this.businessFieldMapper = businessFieldMapper;
    }

    /**
     * Save a businessField.
     *
     * @param businessFieldDTO the entity to save.
     * @return the persisted entity.
     */
    public BusinessFieldDTO save(BusinessFieldDTO businessFieldDTO) {
        log.debug("Request to save BusinessField : {}", businessFieldDTO);
        BusinessField businessField = businessFieldMapper.toEntity(businessFieldDTO);
        businessField = businessFieldRepository.save(businessField);
        return businessFieldMapper.toDto(businessField);
    }

    /**
     * Partially update a businessField.
     *
     * @param businessFieldDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<BusinessFieldDTO> partialUpdate(BusinessFieldDTO businessFieldDTO) {
        log.debug("Request to partially update BusinessField : {}", businessFieldDTO);

        return businessFieldRepository
            .findById(businessFieldDTO.getId())
            .map(existingBusinessField -> {
                businessFieldMapper.partialUpdate(existingBusinessField, businessFieldDTO);

                return existingBusinessField;
            })
            .map(businessFieldRepository::save)
            .map(businessFieldMapper::toDto);
    }

    /**
     * Get all the businessFields.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BusinessFieldDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BusinessFields");
        return businessFieldRepository.findAll(pageable).map(businessFieldMapper::toDto);
    }

    /**
     * Get one businessField by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BusinessFieldDTO> findOne(Long id) {
        log.debug("Request to get BusinessField : {}", id);
        return businessFieldRepository.findById(id).map(businessFieldMapper::toDto);
    }

    /**
     * Delete the businessField by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BusinessField : {}", id);
        businessFieldRepository.deleteById(id);
    }
}
