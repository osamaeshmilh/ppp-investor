package ly.ppp.investor.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import ly.ppp.investor.domain.*; // for static metamodels
import ly.ppp.investor.domain.PaymentReceipt;
import ly.ppp.investor.repository.PaymentReceiptRepository;
import ly.ppp.investor.service.criteria.PaymentReceiptCriteria;
import ly.ppp.investor.service.dto.PaymentReceiptDTO;
import ly.ppp.investor.service.mapper.PaymentReceiptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PaymentReceipt} entities in the database.
 * The main input is a {@link PaymentReceiptCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PaymentReceiptDTO} or a {@link Page} of {@link PaymentReceiptDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PaymentReceiptQueryService extends QueryService<PaymentReceipt> {

    private final Logger log = LoggerFactory.getLogger(PaymentReceiptQueryService.class);

    private final PaymentReceiptRepository paymentReceiptRepository;

    private final PaymentReceiptMapper paymentReceiptMapper;

    public PaymentReceiptQueryService(PaymentReceiptRepository paymentReceiptRepository, PaymentReceiptMapper paymentReceiptMapper) {
        this.paymentReceiptRepository = paymentReceiptRepository;
        this.paymentReceiptMapper = paymentReceiptMapper;
    }

    /**
     * Return a {@link List} of {@link PaymentReceiptDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PaymentReceiptDTO> findByCriteria(PaymentReceiptCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PaymentReceipt> specification = createSpecification(criteria);
        return paymentReceiptMapper.toDto(paymentReceiptRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PaymentReceiptDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PaymentReceiptDTO> findByCriteria(PaymentReceiptCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PaymentReceipt> specification = createSpecification(criteria);
        return paymentReceiptRepository.findAll(specification, page).map(paymentReceiptMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PaymentReceiptCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PaymentReceipt> specification = createSpecification(criteria);
        return paymentReceiptRepository.count(specification);
    }

    /**
     * Function to convert {@link PaymentReceiptCriteria} to a {@link Specification}
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PaymentReceipt> createSpecification(PaymentReceiptCriteria criteria) {
        Specification<PaymentReceipt> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PaymentReceipt_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), PaymentReceipt_.amount));
            }
            if (criteria.getDetails() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDetails(), PaymentReceipt_.details));
            }
            if (criteria.getReceiptDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReceiptDate(), PaymentReceipt_.receiptDate));
            }
            if (criteria.getInvestorId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getInvestorId(),
                            root -> root.join(PaymentReceipt_.investor, JoinType.LEFT).get(Investor_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
