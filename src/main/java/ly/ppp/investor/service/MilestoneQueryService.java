package ly.ppp.investor.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import ly.ppp.investor.domain.*; // for static metamodels
import ly.ppp.investor.domain.Milestone;
import ly.ppp.investor.repository.MilestoneRepository;
import ly.ppp.investor.service.criteria.MilestoneCriteria;
import ly.ppp.investor.service.dto.MilestoneDTO;
import ly.ppp.investor.service.mapper.MilestoneMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Milestone} entities in the database.
 * The main input is a {@link MilestoneCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MilestoneDTO} or a {@link Page} of {@link MilestoneDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MilestoneQueryService extends QueryService<Milestone> {

    private final Logger log = LoggerFactory.getLogger(MilestoneQueryService.class);

    private final MilestoneRepository milestoneRepository;

    private final MilestoneMapper milestoneMapper;

    public MilestoneQueryService(MilestoneRepository milestoneRepository, MilestoneMapper milestoneMapper) {
        this.milestoneRepository = milestoneRepository;
        this.milestoneMapper = milestoneMapper;
    }

    /**
     * Return a {@link List} of {@link MilestoneDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MilestoneDTO> findByCriteria(MilestoneCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Milestone> specification = createSpecification(criteria);
        return milestoneMapper.toDto(milestoneRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MilestoneDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MilestoneDTO> findByCriteria(MilestoneCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Milestone> specification = createSpecification(criteria);
        return milestoneRepository.findAll(specification, page).map(milestoneMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MilestoneCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Milestone> specification = createSpecification(criteria);
        return milestoneRepository.count(specification);
    }

    /**
     * Function to convert {@link MilestoneCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Milestone> createSpecification(MilestoneCriteria criteria) {
        Specification<Milestone> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Milestone_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Milestone_.name));
            }
            if (criteria.getDetails() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDetails(), Milestone_.details));
            }
            if (criteria.getOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrder(), Milestone_.order));
            }
            if (criteria.getIsFinished() != null) {
                specification = specification.and(buildSpecification(criteria.getIsFinished(), Milestone_.isFinished));
            }
            if (criteria.getNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotes(), Milestone_.notes));
            }
            if (criteria.getProjectId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getProjectId(), root -> root.join(Milestone_.project, JoinType.LEFT).get(Project_.id))
                    );
            }
        }
        return specification;
    }
}
