package ly.ppp.investor.service;

import java.util.Optional;
import ly.ppp.investor.domain.Orginization;
import ly.ppp.investor.repository.OrginizationRepository;
import ly.ppp.investor.service.dto.OrginizationDTO;
import ly.ppp.investor.service.mapper.OrginizationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Orginization}.
 */
@Service
@Transactional
public class OrginizationService {

    private final Logger log = LoggerFactory.getLogger(OrginizationService.class);

    private final OrginizationRepository orginizationRepository;

    private final OrginizationMapper orginizationMapper;

    public OrginizationService(OrginizationRepository orginizationRepository, OrginizationMapper orginizationMapper) {
        this.orginizationRepository = orginizationRepository;
        this.orginizationMapper = orginizationMapper;
    }

    /**
     * Save a orginization.
     *
     * @param orginizationDTO the entity to save.
     * @return the persisted entity.
     */
    public OrginizationDTO save(OrginizationDTO orginizationDTO) {
        log.debug("Request to save Orginization : {}", orginizationDTO);
        Orginization orginization = orginizationMapper.toEntity(orginizationDTO);
        orginization = orginizationRepository.save(orginization);
        return orginizationMapper.toDto(orginization);
    }

    /**
     * Partially update a orginization.
     *
     * @param orginizationDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<OrginizationDTO> partialUpdate(OrginizationDTO orginizationDTO) {
        log.debug("Request to partially update Orginization : {}", orginizationDTO);

        return orginizationRepository
            .findById(orginizationDTO.getId())
            .map(existingOrginization -> {
                orginizationMapper.partialUpdate(existingOrginization, orginizationDTO);

                return existingOrginization;
            })
            .map(orginizationRepository::save)
            .map(orginizationMapper::toDto);
    }

    /**
     * Get all the orginizations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OrginizationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Orginizations");
        return orginizationRepository.findAll(pageable).map(orginizationMapper::toDto);
    }

    /**
     * Get one orginization by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OrginizationDTO> findOne(Long id) {
        log.debug("Request to get Orginization : {}", id);
        return orginizationRepository.findById(id).map(orginizationMapper::toDto);
    }

    /**
     * Delete the orginization by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Orginization : {}", id);
        orginizationRepository.deleteById(id);
    }
}
