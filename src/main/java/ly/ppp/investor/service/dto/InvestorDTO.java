package ly.ppp.investor.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;
import ly.ppp.investor.domain.enumeration.CompanyPurpose;
import ly.ppp.investor.domain.enumeration.Currency;
import ly.ppp.investor.domain.enumeration.InvestorStatus;

/**
 * A DTO for the {@link ly.ppp.investor.domain.Investor} entity.
 */
public class InvestorDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String investorNo;

    @NotNull
    private String companyName;

    private CompanyPurpose companyPurpose;

    private String companyRegisterNo;

    private Integer companyForeignEmployeeNo;

    private Integer companyLocalEmployeeNo;

    private String companyTradeChamberNo;

    private LocalDate companyEstablishmentDate;

    private String companyMobileNo;

    private String companyEmail;

    private String companyWebsite;

    private String companyAddress;

    private String companyAddressCity;

    private String companyCapital;

    private String companyCapitalText;

    private Currency companyCapitalCurrency;

    private String representativeFullName;

    private String representativeMobileNo;

    private String representativeEmail;

    private String representativeNationalNo;

    private String representativeRegisterNo;

    private String representativePassportNo;

    private Boolean isRepresentativeForeign;

    private String notes;

    private InvestorStatus investorStatus;

    private UserDTO user;

    private CityDTO companyCity;

    private CountryDTO companyCountry;

    private CountryDTO companyAddressCountry;

    private CityDTO representativeCity;

    private CountryDTO representativeCountry;

    private BusinessFieldDTO businessField;

    private Set<AttachmentDTO> uploadedAttachments = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvestorNo() {
        return investorNo;
    }

    public void setInvestorNo(String investorNo) {
        this.investorNo = investorNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public CompanyPurpose getCompanyPurpose() {
        return companyPurpose;
    }

    public void setCompanyPurpose(CompanyPurpose companyPurpose) {
        this.companyPurpose = companyPurpose;
    }

    public String getCompanyRegisterNo() {
        return companyRegisterNo;
    }

    public void setCompanyRegisterNo(String companyRegisterNo) {
        this.companyRegisterNo = companyRegisterNo;
    }

    public Integer getCompanyForeignEmployeeNo() {
        return companyForeignEmployeeNo;
    }

    public void setCompanyForeignEmployeeNo(Integer companyForeignEmployeeNo) {
        this.companyForeignEmployeeNo = companyForeignEmployeeNo;
    }

    public Integer getCompanyLocalEmployeeNo() {
        return companyLocalEmployeeNo;
    }

    public void setCompanyLocalEmployeeNo(Integer companyLocalEmployeeNo) {
        this.companyLocalEmployeeNo = companyLocalEmployeeNo;
    }

    public String getCompanyTradeChamberNo() {
        return companyTradeChamberNo;
    }

    public void setCompanyTradeChamberNo(String companyTradeChamberNo) {
        this.companyTradeChamberNo = companyTradeChamberNo;
    }

    public LocalDate getCompanyEstablishmentDate() {
        return companyEstablishmentDate;
    }

    public void setCompanyEstablishmentDate(LocalDate companyEstablishmentDate) {
        this.companyEstablishmentDate = companyEstablishmentDate;
    }

    public String getCompanyMobileNo() {
        return companyMobileNo;
    }

    public void setCompanyMobileNo(String companyMobileNo) {
        this.companyMobileNo = companyMobileNo;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyAddressCity() {
        return companyAddressCity;
    }

    public void setCompanyAddressCity(String companyAddressCity) {
        this.companyAddressCity = companyAddressCity;
    }

    public String getCompanyCapital() {
        return companyCapital;
    }

    public void setCompanyCapital(String companyCapital) {
        this.companyCapital = companyCapital;
    }

    public String getCompanyCapitalText() {
        return companyCapitalText;
    }

    public void setCompanyCapitalText(String companyCapitalText) {
        this.companyCapitalText = companyCapitalText;
    }

    public Currency getCompanyCapitalCurrency() {
        return companyCapitalCurrency;
    }

    public void setCompanyCapitalCurrency(Currency companyCapitalCurrency) {
        this.companyCapitalCurrency = companyCapitalCurrency;
    }

    public String getRepresentativeFullName() {
        return representativeFullName;
    }

    public void setRepresentativeFullName(String representativeFullName) {
        this.representativeFullName = representativeFullName;
    }

    public String getRepresentativeMobileNo() {
        return representativeMobileNo;
    }

    public void setRepresentativeMobileNo(String representativeMobileNo) {
        this.representativeMobileNo = representativeMobileNo;
    }

    public String getRepresentativeEmail() {
        return representativeEmail;
    }

    public void setRepresentativeEmail(String representativeEmail) {
        this.representativeEmail = representativeEmail;
    }

    public String getRepresentativeNationalNo() {
        return representativeNationalNo;
    }

    public void setRepresentativeNationalNo(String representativeNationalNo) {
        this.representativeNationalNo = representativeNationalNo;
    }

    public String getRepresentativeRegisterNo() {
        return representativeRegisterNo;
    }

    public void setRepresentativeRegisterNo(String representativeRegisterNo) {
        this.representativeRegisterNo = representativeRegisterNo;
    }

    public String getRepresentativePassportNo() {
        return representativePassportNo;
    }

    public void setRepresentativePassportNo(String representativePassportNo) {
        this.representativePassportNo = representativePassportNo;
    }

    public Boolean getIsRepresentativeForeign() {
        return isRepresentativeForeign;
    }

    public void setIsRepresentativeForeign(Boolean isRepresentativeForeign) {
        this.isRepresentativeForeign = isRepresentativeForeign;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public InvestorStatus getInvestorStatus() {
        return investorStatus;
    }

    public void setInvestorStatus(InvestorStatus investorStatus) {
        this.investorStatus = investorStatus;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public CityDTO getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(CityDTO companyCity) {
        this.companyCity = companyCity;
    }

    public CountryDTO getCompanyCountry() {
        return companyCountry;
    }

    public void setCompanyCountry(CountryDTO companyCountry) {
        this.companyCountry = companyCountry;
    }

    public CountryDTO getCompanyAddressCountry() {
        return companyAddressCountry;
    }

    public void setCompanyAddressCountry(CountryDTO companyAddressCountry) {
        this.companyAddressCountry = companyAddressCountry;
    }

    public CityDTO getRepresentativeCity() {
        return representativeCity;
    }

    public void setRepresentativeCity(CityDTO representativeCity) {
        this.representativeCity = representativeCity;
    }

    public CountryDTO getRepresentativeCountry() {
        return representativeCountry;
    }

    public void setRepresentativeCountry(CountryDTO representativeCountry) {
        this.representativeCountry = representativeCountry;
    }

    public BusinessFieldDTO getBusinessField() {
        return businessField;
    }

    public void setBusinessField(BusinessFieldDTO businessField) {
        this.businessField = businessField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InvestorDTO)) {
            return false;
        }

        InvestorDTO investorDTO = (InvestorDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, investorDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InvestorDTO{" +
            "id=" + getId() +
            ", investorNo='" + getInvestorNo() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            ", companyPurpose='" + getCompanyPurpose() + "'" +
            ", companyRegisterNo='" + getCompanyRegisterNo() + "'" +
            ", companyForeignEmployeeNo=" + getCompanyForeignEmployeeNo() +
            ", companyLocalEmployeeNo=" + getCompanyLocalEmployeeNo() +
            ", companyTradeChamberNo='" + getCompanyTradeChamberNo() + "'" +
            ", companyEstablishmentDate='" + getCompanyEstablishmentDate() + "'" +
            ", companyMobileNo='" + getCompanyMobileNo() + "'" +
            ", companyEmail='" + getCompanyEmail() + "'" +
            ", companyWebsite='" + getCompanyWebsite() + "'" +
            ", companyAddress='" + getCompanyAddress() + "'" +
            ", companyAddressCity='" + getCompanyAddressCity() + "'" +
            ", companyCapital='" + getCompanyCapital() + "'" +
            ", companyCapitalText='" + getCompanyCapitalText() + "'" +
            ", companyCapitalCurrency='" + getCompanyCapitalCurrency() + "'" +
            ", representativeFullName='" + getRepresentativeFullName() + "'" +
            ", representativeMobileNo='" + getRepresentativeMobileNo() + "'" +
            ", representativeEmail='" + getRepresentativeEmail() + "'" +
            ", representativeNationalNo='" + getRepresentativeNationalNo() + "'" +
            ", representativeRegisterNo='" + getRepresentativeRegisterNo() + "'" +
            ", representativePassportNo='" + getRepresentativePassportNo() + "'" +
            ", isRepresentativeForeign='" + getIsRepresentativeForeign() + "'" +
            ", notes='" + getNotes() + "'" +
            ", investorStatus='" + getInvestorStatus() + "'" +
            ", user=" + getUser() +
            ", companyCity=" + getCompanyCity() +
            ", companyCountry=" + getCompanyCountry() +
            ", companyAddressCountry=" + getCompanyAddressCountry() +
            ", representativeCity=" + getRepresentativeCity() +
            ", representativeCountry=" + getRepresentativeCountry() +
            ", businessField=" + getBusinessField() +
            "}";
    }

    public Set<AttachmentDTO> getUploadedAttachments() {
        return uploadedAttachments;
    }

    public void setUploadedAttachments(Set<AttachmentDTO> uploadedAttachments) {
        this.uploadedAttachments = uploadedAttachments;
    }
}
