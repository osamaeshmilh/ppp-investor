package ly.ppp.investor.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link ly.ppp.investor.domain.BusinessField} entity.
 */
public class BusinessFieldDTO implements Serializable {

    private Long id;

    private String businessFieldNo;

    private String name;

    private String nameAr;

    private String nameEn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessFieldNo() {
        return businessFieldNo;
    }

    public void setBusinessFieldNo(String businessFieldNo) {
        this.businessFieldNo = businessFieldNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BusinessFieldDTO)) {
            return false;
        }

        BusinessFieldDTO businessFieldDTO = (BusinessFieldDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, businessFieldDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BusinessFieldDTO{" +
            "id=" + getId() +
            ", businessFieldNo='" + getBusinessFieldNo() + "'" +
            ", name='" + getName() + "'" +
            ", nameAr='" + getNameAr() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            "}";
    }
}
