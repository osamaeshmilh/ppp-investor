package ly.ppp.investor.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import ly.ppp.investor.domain.enumeration.Currency;
import ly.ppp.investor.domain.enumeration.ProjectStatus;

/**
 * A DTO for the {@link ly.ppp.investor.domain.Project} entity.
 */
public class ProjectPublicDTO implements Serializable {

    private Long id;

    private String projectNo;

    private String name;

    private String nameEn;

    private String details;

    private String detailsEn;

    private String address;

    private BusinessFieldDTO businessField;

    private CityDTO city;

    private CountryDTO country;

    private InvestorDTO investor;

    private OrginizationDTO orginization;

    private Set<AttachmentDTO> attachments = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectNo() {
        return projectNo;
    }

    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDetailsEn() {
        return detailsEn;
    }

    public void setDetailsEn(String detailsEn) {
        this.detailsEn = detailsEn;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BusinessFieldDTO getBusinessField() {
        return businessField;
    }

    public void setBusinessField(BusinessFieldDTO businessField) {
        this.businessField = businessField;
    }

    public CityDTO getCity() {
        return city;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }

    public CountryDTO getCountry() {
        return country;
    }

    public void setCountry(CountryDTO country) {
        this.country = country;
    }

    public InvestorDTO getInvestor() {
        return investor;
    }

    public void setInvestor(InvestorDTO investor) {
        this.investor = investor;
    }

    public OrginizationDTO getOrginization() {
        return orginization;
    }

    public void setOrginization(OrginizationDTO orginization) {
        this.orginization = orginization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProjectPublicDTO)) {
            return false;
        }

        ProjectPublicDTO projectDTO = (ProjectPublicDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, projectDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProjectDTO{" +
            "id=" + getId() +
            ", projectNo='" + getProjectNo() + "'" +
            ", name='" + getName() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", details='" + getDetails() + "'" +
            ", detailsEn='" + getDetailsEn() + "'" +
            ", address='" + getAddress() + "'" +
            ", businessField=" + getBusinessField() +
            ", city=" + getCity() +
            ", country=" + getCountry() +
            ", investor=" + getInvestor() +
            ", orginization=" + getOrginization() +
            "}";
    }

    public Set<AttachmentDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<AttachmentDTO> attachments) {
        this.attachments = attachments;
    }
}
