package ly.ppp.investor.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link ly.ppp.investor.domain.PaymentReceipt} entity.
 */
public class PaymentReceiptDTO implements Serializable {

    private Long id;

    private Float amount;

    private String details;

    private LocalDate receiptDate;

    private InvestorDTO investor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LocalDate getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(LocalDate receiptDate) {
        this.receiptDate = receiptDate;
    }

    public InvestorDTO getInvestor() {
        return investor;
    }

    public void setInvestor(InvestorDTO investor) {
        this.investor = investor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentReceiptDTO)) {
            return false;
        }

        PaymentReceiptDTO paymentReceiptDTO = (PaymentReceiptDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, paymentReceiptDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentReceiptDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", details='" + getDetails() + "'" +
            ", receiptDate='" + getReceiptDate() + "'" +
            ", investor=" + getInvestor() +
            "}";
    }
}
