package ly.ppp.investor.service.dto;

import java.io.Serializable;
import java.util.*;
import ly.ppp.investor.domain.enumeration.Currency;
import ly.ppp.investor.domain.enumeration.ProjectStatus;

/**
 * A DTO for the {@link ly.ppp.investor.domain.Project} entity.
 */
public class ProjectDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String projectNo;

    private String name;

    private String nameEn;

    private String details;

    private String detailsEn;

    private String address;

    private Float estimatedCost;

    private Currency estimatedCostCurrency;

    private ProjectStatus projectStatus;

    private Float lat;

    private Float lng;

    private BusinessFieldDTO businessField;

    private CityDTO city;

    private CountryDTO country;

    private InvestorDTO investor;

    private OrginizationDTO orginization;

    private List<AttachmentDTO> attachments = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectNo() {
        return projectNo;
    }

    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDetailsEn() {
        return detailsEn;
    }

    public void setDetailsEn(String detailsEn) {
        this.detailsEn = detailsEn;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(Float estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public Currency getEstimatedCostCurrency() {
        return estimatedCostCurrency;
    }

    public void setEstimatedCostCurrency(Currency estimatedCostCurrency) {
        this.estimatedCostCurrency = estimatedCostCurrency;
    }

    public ProjectStatus getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(ProjectStatus projectStatus) {
        this.projectStatus = projectStatus;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public BusinessFieldDTO getBusinessField() {
        return businessField;
    }

    public void setBusinessField(BusinessFieldDTO businessField) {
        this.businessField = businessField;
    }

    public CityDTO getCity() {
        return city;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }

    public CountryDTO getCountry() {
        return country;
    }

    public void setCountry(CountryDTO country) {
        this.country = country;
    }

    public InvestorDTO getInvestor() {
        return investor;
    }

    public void setInvestor(InvestorDTO investor) {
        this.investor = investor;
    }

    public OrginizationDTO getOrginization() {
        return orginization;
    }

    public void setOrginization(OrginizationDTO orginization) {
        this.orginization = orginization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProjectDTO)) {
            return false;
        }

        ProjectDTO projectDTO = (ProjectDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, projectDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProjectDTO{" +
            "id=" + getId() +
            ", projectNo='" + getProjectNo() + "'" +
            ", name='" + getName() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", details='" + getDetails() + "'" +
            ", detailsEn='" + getDetailsEn() + "'" +
            ", address='" + getAddress() + "'" +
            ", estimatedCost=" + getEstimatedCost() +
            ", estimatedCostCurrency='" + getEstimatedCostCurrency() + "'" +
            ", projectStatus='" + getProjectStatus() + "'" +
            ", lat=" + getLat() +
            ", lng=" + getLng() +
            ", businessField=" + getBusinessField() +
            ", city=" + getCity() +
            ", country=" + getCountry() +
            ", investor=" + getInvestor() +
            ", orginization=" + getOrginization() +
            "}";
    }

    public List<AttachmentDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDTO> attachments) {
        this.attachments = attachments;
    }
}
