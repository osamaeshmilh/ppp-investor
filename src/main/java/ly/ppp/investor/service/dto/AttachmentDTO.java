package ly.ppp.investor.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import ly.ppp.investor.domain.enumeration.AttachmentType;

/**
 * A DTO for the {@link ly.ppp.investor.domain.Attachment} entity.
 */
public class AttachmentDTO implements Serializable {

    private Long id;

    private String name;

    private AttachmentType attachmentType;

    @Lob
    private byte[] file;

    private String fileContentType;
    private String fileUrl;

    private String notes;

    private InvestorDTO investor;

    private ProjectDTO project;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AttachmentType getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(AttachmentType attachmentType) {
        this.attachmentType = attachmentType;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public InvestorDTO getInvestor() {
        return investor;
    }

    public void setInvestor(InvestorDTO investor) {
        this.investor = investor;
    }

    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AttachmentDTO)) {
            return false;
        }

        AttachmentDTO attachmentDTO = (AttachmentDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, attachmentDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AttachmentDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", attachmentType='" + getAttachmentType() + "'" +
            ", file='" + getFile() + "'" +
            ", fileUrl='" + getFileUrl() + "'" +
            ", notes='" + getNotes() + "'" +
            ", investor=" + getInvestor() +
            ", project=" + getProject() +
            "}";
    }
}
