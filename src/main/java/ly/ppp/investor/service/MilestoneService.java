package ly.ppp.investor.service;

import java.util.Optional;
import ly.ppp.investor.domain.Milestone;
import ly.ppp.investor.repository.MilestoneRepository;
import ly.ppp.investor.service.dto.MilestoneDTO;
import ly.ppp.investor.service.mapper.MilestoneMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Milestone}.
 */
@Service
@Transactional
public class MilestoneService {

    private final Logger log = LoggerFactory.getLogger(MilestoneService.class);

    private final MilestoneRepository milestoneRepository;

    private final MilestoneMapper milestoneMapper;

    public MilestoneService(MilestoneRepository milestoneRepository, MilestoneMapper milestoneMapper) {
        this.milestoneRepository = milestoneRepository;
        this.milestoneMapper = milestoneMapper;
    }

    /**
     * Save a milestone.
     *
     * @param milestoneDTO the entity to save.
     * @return the persisted entity.
     */
    public MilestoneDTO save(MilestoneDTO milestoneDTO) {
        log.debug("Request to save Milestone : {}", milestoneDTO);
        Milestone milestone = milestoneMapper.toEntity(milestoneDTO);
        milestone = milestoneRepository.save(milestone);
        return milestoneMapper.toDto(milestone);
    }

    /**
     * Partially update a milestone.
     *
     * @param milestoneDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MilestoneDTO> partialUpdate(MilestoneDTO milestoneDTO) {
        log.debug("Request to partially update Milestone : {}", milestoneDTO);

        return milestoneRepository
            .findById(milestoneDTO.getId())
            .map(existingMilestone -> {
                milestoneMapper.partialUpdate(existingMilestone, milestoneDTO);

                return existingMilestone;
            })
            .map(milestoneRepository::save)
            .map(milestoneMapper::toDto);
    }

    /**
     * Get all the milestones.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MilestoneDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Milestones");
        return milestoneRepository.findAll(pageable).map(milestoneMapper::toDto);
    }

    /**
     * Get one milestone by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MilestoneDTO> findOne(Long id) {
        log.debug("Request to get Milestone : {}", id);
        return milestoneRepository.findById(id).map(milestoneMapper::toDto);
    }

    /**
     * Delete the milestone by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Milestone : {}", id);
        milestoneRepository.deleteById(id);
    }
}
