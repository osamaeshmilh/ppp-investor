package ly.ppp.investor.service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Optional;
import ly.ppp.investor.domain.Project;
import ly.ppp.investor.repository.ProjectRepository;
import ly.ppp.investor.service.dto.PaymentReceiptDTO;
import ly.ppp.investor.service.dto.ProjectDTO;
import ly.ppp.investor.service.mapper.ProjectMapper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Project}.
 */
@Service
@Transactional
public class ProjectService {

    private final Logger log = LoggerFactory.getLogger(ProjectService.class);

    private final ProjectRepository projectRepository;

    private final ProjectMapper projectMapper;

    public ProjectService(ProjectRepository projectRepository, ProjectMapper projectMapper) {
        this.projectRepository = projectRepository;
        this.projectMapper = projectMapper;
    }

    /**
     * Save a project.
     *
     * @param projectDTO the entity to save.
     * @return the persisted entity.
     */
    public ProjectDTO save(ProjectDTO projectDTO) {
        log.debug("Request to save Project : {}", projectDTO);
        Project project = projectMapper.toEntity(projectDTO);
        project = projectRepository.save(project);
        return projectMapper.toDto(project);
    }

    /**
     * Partially update a project.
     *
     * @param projectDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ProjectDTO> partialUpdate(ProjectDTO projectDTO) {
        log.debug("Request to partially update Project : {}", projectDTO);

        return projectRepository
            .findById(projectDTO.getId())
            .map(existingProject -> {
                projectMapper.partialUpdate(existingProject, projectDTO);

                return existingProject;
            })
            .map(projectRepository::save)
            .map(projectMapper::toDto);
    }

    /**
     * Get all the projects.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ProjectDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Projects");
        return projectRepository.findAll(pageable).map(projectMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<ProjectDTO> findAll() {
        log.debug("Request to get all Projects");
        return projectMapper.toDto(projectRepository.findAll());
    }

    /**
     * Get one project by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProjectDTO> findOne(Long id) {
        log.debug("Request to get Project : {}", id);
        return projectRepository.findById(id).map(projectMapper::toDto);
    }

    /**
     * Delete the project by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Project : {}", id);
        projectRepository.deleteById(id);
    }

    public byte[] getAllXslx() {
        String[] columns = {
            "ر.ت",
            "رقم المشروع",
            "اسم المشروع",
            "التفاصيل",
            "العنوان",
            "التكلفة التقديرية",
            "العملة",
            "المستثمر",
            "مجال العمل",
            "الجهة المستفيدة",
            "المدينة",
            "حالة المشروع",
        };
        List<ProjectDTO> projectDTOS = findAll();

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Projects");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Other rows and cells with contacts data
        int rowNum = 1;

        for (ProjectDTO projectDTO : projectDTOS) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(projectDTO.getId());
            row.createCell(1).setCellValue(projectDTO.getProjectNo());
            row.createCell(2).setCellValue(projectDTO.getName());
            row.createCell(3).setCellValue(projectDTO.getDetails());
            row.createCell(4).setCellValue(projectDTO.getAddress());
            row.createCell(5).setCellValue(projectDTO.getEstimatedCost());
            row.createCell(6).setCellValue(projectDTO.getEstimatedCostCurrency().toString());
            row.createCell(7).setCellValue(projectDTO.getInvestor() != null ? projectDTO.getInvestor().getCompanyName() : "");
            row.createCell(8).setCellValue(projectDTO.getBusinessField() != null ? projectDTO.getBusinessField().getName() : "");
            row.createCell(9).setCellValue(projectDTO.getOrginization() != null ? projectDTO.getOrginization().getName() : "");
            row.createCell(10).setCellValue(projectDTO.getCity() != null ? projectDTO.getCity().getName() : "");
            row.createCell(11).setCellValue(projectDTO.getProjectStatus().toString());
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        byte[] bytes = new byte[0];

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            bos.close();
            bytes = bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bytes;
    }
}
