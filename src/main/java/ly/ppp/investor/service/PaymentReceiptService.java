package ly.ppp.investor.service;

import java.util.List;
import java.util.Optional;
import ly.ppp.investor.service.dto.PaymentReceiptDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Interface for managing {@link ly.ppp.investor.domain.PaymentReceipt}.
 */
public interface PaymentReceiptService {
    /**
     * Save a paymentReceipt.
     *
     * @param paymentReceiptDTO the entity to save.
     * @return the persisted entity.
     */
    PaymentReceiptDTO save(PaymentReceiptDTO paymentReceiptDTO);

    /**
     * Partially updates a paymentReceipt.
     *
     * @param paymentReceiptDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PaymentReceiptDTO> partialUpdate(PaymentReceiptDTO paymentReceiptDTO);

    /**
     * Get all the paymentReceipts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PaymentReceiptDTO> findAll(Pageable pageable);

    @Transactional(readOnly = true)
    List<PaymentReceiptDTO> findAll();

    /**
     * Get the "id" paymentReceipt.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PaymentReceiptDTO> findOne(Long id);

    /**
     * Delete the "id" paymentReceipt.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    byte[] getAllXslx();
}
