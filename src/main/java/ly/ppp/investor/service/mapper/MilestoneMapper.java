package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.Milestone;
import ly.ppp.investor.service.dto.MilestoneDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Milestone} and its DTO {@link MilestoneDTO}.
 */
@Mapper(componentModel = "spring", uses = { ProjectMapper.class })
public interface MilestoneMapper extends EntityMapper<MilestoneDTO, Milestone> {
    @Mapping(target = "project", source = "project", qualifiedByName = "name")
    MilestoneDTO toDto(Milestone s);
}
