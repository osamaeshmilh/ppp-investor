package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.Bank;
import ly.ppp.investor.service.dto.BankDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Bank} and its DTO {@link BankDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface BankMapper extends EntityMapper<BankDTO, Bank> {
    @Mapping(target = "user", source = "user", qualifiedByName = "id")
    BankDTO toDto(Bank s);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    BankDTO toDtoName(Bank bank);
}
