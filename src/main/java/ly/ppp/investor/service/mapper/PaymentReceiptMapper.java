package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.PaymentReceipt;
import ly.ppp.investor.service.dto.PaymentReceiptDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PaymentReceipt} and its DTO {@link PaymentReceiptDTO}.
 */
@Mapper(componentModel = "spring", uses = { InvestorMapper.class })
public interface PaymentReceiptMapper extends EntityMapper<PaymentReceiptDTO, PaymentReceipt> {
    @Mapping(target = "investor", source = "investor", qualifiedByName = "companyName")
    PaymentReceiptDTO toDto(PaymentReceipt s);
}
