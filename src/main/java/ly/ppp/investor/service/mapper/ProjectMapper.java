package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.Project;
import ly.ppp.investor.service.dto.ProjectDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Project} and its DTO {@link ProjectDTO}.
 */
@Mapper(
    componentModel = "spring",
    uses = { BusinessFieldMapper.class, CityMapper.class, CountryMapper.class, InvestorMapper.class, OrginizationMapper.class }
)
public interface ProjectMapper extends EntityMapper<ProjectDTO, Project> {
    @Mapping(target = "businessField", source = "businessField", qualifiedByName = "name")
    @Mapping(target = "city", source = "city", qualifiedByName = "name")
    @Mapping(target = "country", source = "country", qualifiedByName = "name")
    @Mapping(target = "investor", source = "investor", qualifiedByName = "companyName")
    @Mapping(target = "orginization", source = "orginization", qualifiedByName = "name")
    ProjectDTO toDto(Project s);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    ProjectDTO toDtoName(Project project);
}
