package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.BusinessField;
import ly.ppp.investor.service.dto.BusinessFieldDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link BusinessField} and its DTO {@link BusinessFieldDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BusinessFieldMapper extends EntityMapper<BusinessFieldDTO, BusinessField> {
    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    BusinessFieldDTO toDtoName(BusinessField businessField);
}
