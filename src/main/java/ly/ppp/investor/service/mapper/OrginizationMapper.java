package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.Orginization;
import ly.ppp.investor.service.dto.OrginizationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Orginization} and its DTO {@link OrginizationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrginizationMapper extends EntityMapper<OrginizationDTO, Orginization> {
    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    OrginizationDTO toDtoName(Orginization orginization);
}
