package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.Attachment;
import ly.ppp.investor.service.dto.AttachmentDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Attachment} and its DTO {@link AttachmentDTO}.
 */
@Mapper(componentModel = "spring", uses = { InvestorMapper.class, ProjectMapper.class })
public interface AttachmentMapper extends EntityMapper<AttachmentDTO, Attachment> {
    @Mapping(target = "investor", source = "investor", qualifiedByName = "companyName")
    @Mapping(target = "project", source = "project", qualifiedByName = "name")
    AttachmentDTO toDto(Attachment s);
}
