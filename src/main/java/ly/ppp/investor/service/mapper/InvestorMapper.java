package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.Investor;
import ly.ppp.investor.service.dto.InvestorDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Investor} and its DTO {@link InvestorDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class, CityMapper.class, CountryMapper.class, BusinessFieldMapper.class })
public interface InvestorMapper extends EntityMapper<InvestorDTO, Investor> {
    @Mapping(target = "user", source = "user", qualifiedByName = "id")
    @Mapping(target = "companyCity", source = "companyCity", qualifiedByName = "id")
    @Mapping(target = "companyCountry", source = "companyCountry", qualifiedByName = "id")
    @Mapping(target = "companyAddressCountry", source = "companyAddressCountry", qualifiedByName = "id")
    @Mapping(target = "representativeCity", source = "representativeCity", qualifiedByName = "id")
    @Mapping(target = "representativeCountry", source = "representativeCountry", qualifiedByName = "id")
    @Mapping(target = "businessField", source = "businessField", qualifiedByName = "name")
    InvestorDTO toDto(Investor s);

    @Named("companyName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "companyName", source = "companyName")
    InvestorDTO toDtoCompanyName(Investor investor);
}
