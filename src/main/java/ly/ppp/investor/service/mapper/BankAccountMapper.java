package ly.ppp.investor.service.mapper;

import ly.ppp.investor.domain.BankAccount;
import ly.ppp.investor.service.dto.BankAccountDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link BankAccount} and its DTO {@link BankAccountDTO}.
 */
@Mapper(componentModel = "spring", uses = { BankMapper.class, InvestorMapper.class })
public interface BankAccountMapper extends EntityMapper<BankAccountDTO, BankAccount> {
    @Mapping(target = "bank", source = "bank", qualifiedByName = "name")
    @Mapping(target = "investor", source = "investor", qualifiedByName = "companyName")
    BankAccountDTO toDto(BankAccount s);
}
