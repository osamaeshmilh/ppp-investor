package ly.ppp.investor.service;

import java.io.ByteArrayOutputStream;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import ly.ppp.investor.domain.Investor;
import ly.ppp.investor.domain.User;
import ly.ppp.investor.domain.enumeration.InvestorStatus;
import ly.ppp.investor.repository.InvestorRepository;
import ly.ppp.investor.security.AuthoritiesConstants;
import ly.ppp.investor.service.dto.InvestorDTO;
import ly.ppp.investor.service.mapper.InvestorMapper;
import ly.ppp.investor.service.mapper.UserMapper;
import ly.ppp.investor.service.util.InvestorNoUtil;
import ly.ppp.investor.web.rest.errors.BadRequestAlertException;
import ly.ppp.investor.web.rest.vm.ManagedUserVM;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Investor}.
 */
@Service
@Transactional
public class InvestorService {

    private final Logger log = LoggerFactory.getLogger(InvestorService.class);

    private final InvestorRepository investorRepository;

    private final InvestorMapper investorMapper;

    private final AttachmentService attachmentService;

    private final UserService userService;

    private final UserMapper userMapper;

    public InvestorService(
        InvestorRepository investorRepository,
        InvestorMapper investorMapper,
        AttachmentService attachmentService,
        UserService userService,
        UserMapper userMapper
    ) {
        this.investorRepository = investorRepository;
        this.investorMapper = investorMapper;
        this.attachmentService = attachmentService;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    /**
     * Save a investor.
     *
     * @param investorDTO the entity to save.
     * @return the persisted entity.
     */
    public InvestorDTO save(InvestorDTO investorDTO) {
        log.debug("Request to save Investor : {}", investorDTO);
        Investor investor = investorMapper.toEntity(investorDTO);
        investor = investorRepository.save(investor);
        return investorMapper.toDto(investor);
    }

    /**
     * Partially update a investor.
     *
     * @param investorDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<InvestorDTO> partialUpdate(InvestorDTO investorDTO) {
        log.debug("Request to partially update Investor : {}", investorDTO);

        return investorRepository
            .findById(investorDTO.getId())
            .map(existingInvestor -> {
                investorMapper.partialUpdate(existingInvestor, investorDTO);

                return existingInvestor;
            })
            .map(investorRepository::save)
            .map(investorMapper::toDto);
    }

    /**
     * Get all the investors.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<InvestorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Investors");
        return investorRepository.findAll(pageable).map(investorMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<InvestorDTO> findAll() {
        log.debug("Request to get all Investors");
        return investorMapper.toDto(investorRepository.findAll());
    }

    /**
     * Get one investor by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<InvestorDTO> findOne(Long id) {
        log.debug("Request to get Investor : {}", id);
        return investorRepository.findById(id).map(investorMapper::toDto);
    }

    /**
     * Delete the investor by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Investor : {}", id);
        investorRepository.deleteById(id);
    }

    public InvestorDTO register(InvestorDTO investorDTO) {
        investorDTO.setInvestorStatus(InvestorStatus.PENDING);
        Investor investor = investorMapper.toEntity(investorDTO);
        investor = investorRepository.save(investor);
        InvestorDTO result = investorMapper.toDto(investor);

        investorDTO
            .getUploadedAttachments()
            .forEach(attachmentDTO -> {
                attachmentDTO.setInvestor(result);
                attachmentService.save(attachmentDTO);
            });
        return investorMapper.toDto(investor);
    }

    public InvestorDTO approveInvestor(Long investorId) {
        Optional<InvestorDTO> investorDTOOptional = investorRepository.findById(investorId).map(investorMapper::toDto);
        if (investorDTOOptional.isPresent()) {
            InvestorDTO investorDTO = investorDTOOptional.get();
            if (investorDTO.getInvestorNo() != null) throw new BadRequestAlertException("المستثمر لديه رقم !", "", "haveNo");

            String year = Year.now().format(DateTimeFormatter.ofPattern("uu"));

            Long count = investorRepository.countByInvestorNoStartingWith(
                year + String.format("%02d", investorDTO.getBusinessField().getId())
            );
            count++;

            String newInvestorNo = String.format("%03d", count);

            String all = year + String.format("%02d", investorDTO.getBusinessField().getId()) + newInvestorNo;
            String resultNo = InvestorNoUtil.CalculateCheckDigits(all);
            investorDTO.setInvestorNo(resultNo);
            investorDTO.setInvestorStatus(InvestorStatus.APPROVED);

            ManagedUserVM managedUserVM = new ManagedUserVM();
            managedUserVM.setFirstName(investorDTO.getCompanyName());
            managedUserVM.setEmail(investorDTO.getCompanyEmail());
            managedUserVM.setLogin(investorDTO.getInvestorNo());
            User user = userService.createAndAssignUserWithPassword(
                managedUserVM,
                AuthoritiesConstants.INVESTOR,
                investorDTO.getInvestorNo()
            );
            investorDTO.setUser(userMapper.userToUserDTO(user));

            return save(investorDTO);
        } else {
            return null;
        }
    }

    public InvestorDTO findOneByUser() {
        return investorMapper.toDto(investorRepository.findByUser(userService.getUserWithAuthorities().get()));
    }

    public byte[] getAllXslx() {
        String[] columns = {
            "ر.ت",
            "رقم المستثمر",
            "اسم الشركة",
            "نوع الشركة",
            "رقم السجل التجاري",
            "عدد العمالة الوطنية",
            "عدد العمالة الاجنبية",
            "رقم الغرفة التجارية",
            "تاريخ التأسيس",
            "رقم هاتف الشركة",
            "البريد الالكتروني",
            "الموقع الالكتروني",
            "العنوان",
            "رأس مال الشركة",
            "رأس مال الشركة بالحروف",
            "عملة رأس مال",
            "اسم ممثل الشركة",
            "رقم هاتف ممثل الشركة",
            "الرقم الوطني لممثل الشركة",
            "بريد ممثل الشركة",
            "ملاحظات",
            "حالة المستثمر",
            "مجال العمل",
            "تاريخ التسجيل",
            "عدد المشاريع",
        };
        List<InvestorDTO> investorDTOS = findAll();

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Investors");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Other rows and cells with contacts data
        int rowNum = 1;

        for (InvestorDTO investorDTO : investorDTOS) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(investorDTO.getId());
            row.createCell(1).setCellValue(investorDTO.getInvestorNo());
            row.createCell(2).setCellValue(investorDTO.getCompanyName());
            row.createCell(3).setCellValue(investorDTO.getCompanyPurpose().toString());
            row.createCell(4).setCellValue(investorDTO.getCompanyRegisterNo());
            row.createCell(5).setCellValue(investorDTO.getCompanyLocalEmployeeNo());
            row.createCell(6).setCellValue(investorDTO.getCompanyForeignEmployeeNo());
            row.createCell(7).setCellValue(investorDTO.getCompanyTradeChamberNo());
            row.createCell(8).setCellValue(investorDTO.getCompanyEstablishmentDate().toString());
            row.createCell(9).setCellValue(investorDTO.getCompanyMobileNo());
            row.createCell(10).setCellValue(investorDTO.getCompanyEmail());
            row.createCell(11).setCellValue(investorDTO.getCompanyWebsite());
            row.createCell(12).setCellValue(investorDTO.getCompanyAddress());
            row.createCell(13).setCellValue(investorDTO.getCompanyCapital());
            row.createCell(14).setCellValue(investorDTO.getCompanyCapitalText());
            row
                .createCell(15)
                .setCellValue(investorDTO.getCompanyCapitalCurrency() != null ? investorDTO.getCompanyCapitalCurrency().toString() : "");
            row.createCell(16).setCellValue(investorDTO.getRepresentativeFullName());
            row.createCell(17).setCellValue(investorDTO.getRepresentativeMobileNo());
            row.createCell(18).setCellValue(investorDTO.getRepresentativeNationalNo());
            row.createCell(19).setCellValue(investorDTO.getRepresentativeEmail());
            row.createCell(20).setCellValue(investorDTO.getNotes());
            row.createCell(21).setCellValue(investorDTO.getInvestorStatus().toString());
            row.createCell(22).setCellValue(investorDTO.getBusinessField() != null ? investorDTO.getBusinessField().getName() : "");
            row.createCell(23).setCellValue(investorDTO.getCreatedDate().toString());
            row.createCell(24).setCellValue(0);
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        byte[] bytes = new byte[0];

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            bos.close();
            bytes = bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bytes;
    }
}
