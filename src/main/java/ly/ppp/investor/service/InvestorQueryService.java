package ly.ppp.investor.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import ly.ppp.investor.domain.*; // for static metamodels
import ly.ppp.investor.domain.Investor;
import ly.ppp.investor.repository.InvestorRepository;
import ly.ppp.investor.service.criteria.InvestorCriteria;
import ly.ppp.investor.service.dto.InvestorDTO;
import ly.ppp.investor.service.mapper.InvestorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Investor} entities in the database.
 * The main input is a {@link InvestorCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link InvestorDTO} or a {@link Page} of {@link InvestorDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class InvestorQueryService extends QueryService<Investor> {

    private final Logger log = LoggerFactory.getLogger(InvestorQueryService.class);

    private final InvestorRepository investorRepository;

    private final InvestorMapper investorMapper;

    public InvestorQueryService(InvestorRepository investorRepository, InvestorMapper investorMapper) {
        this.investorRepository = investorRepository;
        this.investorMapper = investorMapper;
    }

    /**
     * Return a {@link List} of {@link InvestorDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<InvestorDTO> findByCriteria(InvestorCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Investor> specification = createSpecification(criteria);
        return investorMapper.toDto(investorRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link InvestorDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<InvestorDTO> findByCriteria(InvestorCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Investor> specification = createSpecification(criteria);
        return investorRepository.findAll(specification, page).map(investorMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(InvestorCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Investor> specification = createSpecification(criteria);
        return investorRepository.count(specification);
    }

    /**
     * Function to convert {@link InvestorCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Investor> createSpecification(InvestorCriteria criteria) {
        Specification<Investor> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Investor_.id));
            }
            if (criteria.getInvestorNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInvestorNo(), Investor_.investorNo));
            }
            if (criteria.getCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyName(), Investor_.companyName));
            }
            if (criteria.getCompanyPurpose() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyPurpose(), Investor_.companyPurpose));
            }
            if (criteria.getCompanyRegisterNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyRegisterNo(), Investor_.companyRegisterNo));
            }
            if (criteria.getCompanyForeignEmployeeNo() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCompanyForeignEmployeeNo(), Investor_.companyForeignEmployeeNo));
            }
            if (criteria.getCompanyLocalEmployeeNo() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCompanyLocalEmployeeNo(), Investor_.companyLocalEmployeeNo));
            }
            if (criteria.getCompanyTradeChamberNo() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCompanyTradeChamberNo(), Investor_.companyTradeChamberNo));
            }
            if (criteria.getCompanyEstablishmentDate() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCompanyEstablishmentDate(), Investor_.companyEstablishmentDate));
            }
            if (criteria.getCompanyMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyMobileNo(), Investor_.companyMobileNo));
            }
            if (criteria.getCompanyEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyEmail(), Investor_.companyEmail));
            }
            if (criteria.getCompanyWebsite() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyWebsite(), Investor_.companyWebsite));
            }
            if (criteria.getCompanyAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyAddress(), Investor_.companyAddress));
            }
            if (criteria.getCompanyAddressCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyAddressCity(), Investor_.companyAddressCity));
            }
            if (criteria.getCompanyCapital() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCapital(), Investor_.companyCapital));
            }
            if (criteria.getCompanyCapitalText() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCapitalText(), Investor_.companyCapitalText));
            }
            if (criteria.getCompanyCapitalCurrency() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getCompanyCapitalCurrency(), Investor_.companyCapitalCurrency));
            }
            if (criteria.getRepresentativeFullName() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getRepresentativeFullName(), Investor_.representativeFullName));
            }
            if (criteria.getRepresentativeMobileNo() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getRepresentativeMobileNo(), Investor_.representativeMobileNo));
            }
            if (criteria.getRepresentativeEmail() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getRepresentativeEmail(), Investor_.representativeEmail));
            }
            if (criteria.getRepresentativeNationalNo() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getRepresentativeNationalNo(), Investor_.representativeNationalNo));
            }
            if (criteria.getRepresentativeRegisterNo() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getRepresentativeRegisterNo(), Investor_.representativeRegisterNo));
            }
            if (criteria.getRepresentativePassportNo() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getRepresentativePassportNo(), Investor_.representativePassportNo));
            }
            if (criteria.getIsRepresentativeForeign() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getIsRepresentativeForeign(), Investor_.isRepresentativeForeign));
            }
            if (criteria.getNotes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNotes(), Investor_.notes));
            }
            if (criteria.getInvestorStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getInvestorStatus(), Investor_.investorStatus));
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getUserId(), root -> root.join(Investor_.user, JoinType.LEFT).get(User_.id))
                    );
            }
            if (criteria.getCompanyCityId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCompanyCityId(),
                            root -> root.join(Investor_.companyCity, JoinType.LEFT).get(City_.id)
                        )
                    );
            }
            if (criteria.getCompanyCountryId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCompanyCountryId(),
                            root -> root.join(Investor_.companyCountry, JoinType.LEFT).get(Country_.id)
                        )
                    );
            }
            if (criteria.getCompanyAddressCountryId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCompanyAddressCountryId(),
                            root -> root.join(Investor_.companyAddressCountry, JoinType.LEFT).get(Country_.id)
                        )
                    );
            }
            if (criteria.getRepresentativeCityId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRepresentativeCityId(),
                            root -> root.join(Investor_.representativeCity, JoinType.LEFT).get(City_.id)
                        )
                    );
            }
            if (criteria.getRepresentativeCountryId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRepresentativeCountryId(),
                            root -> root.join(Investor_.representativeCountry, JoinType.LEFT).get(Country_.id)
                        )
                    );
            }
            if (criteria.getBusinessFieldId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getBusinessFieldId(),
                            root -> root.join(Investor_.businessField, JoinType.LEFT).get(BusinessField_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
