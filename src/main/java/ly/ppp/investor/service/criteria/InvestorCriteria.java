package ly.ppp.investor.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import ly.ppp.investor.domain.enumeration.CompanyPurpose;
import ly.ppp.investor.domain.enumeration.Currency;
import ly.ppp.investor.domain.enumeration.InvestorStatus;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LocalDateFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.ppp.investor.domain.Investor} entity. This class is used
 * in {@link ly.ppp.investor.web.rest.InvestorResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /investors?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InvestorCriteria implements Serializable, Criteria {

    /**
     * Class for filtering CompanyPurpose
     */
    public static class CompanyPurposeFilter extends Filter<CompanyPurpose> {

        public CompanyPurposeFilter() {}

        public CompanyPurposeFilter(CompanyPurposeFilter filter) {
            super(filter);
        }

        @Override
        public CompanyPurposeFilter copy() {
            return new CompanyPurposeFilter(this);
        }
    }

    /**
     * Class for filtering Currency
     */
    public static class CurrencyFilter extends Filter<Currency> {

        public CurrencyFilter() {}

        public CurrencyFilter(CurrencyFilter filter) {
            super(filter);
        }

        @Override
        public CurrencyFilter copy() {
            return new CurrencyFilter(this);
        }
    }

    /**
     * Class for filtering InvestorStatus
     */
    public static class InvestorStatusFilter extends Filter<InvestorStatus> {

        public InvestorStatusFilter() {}

        public InvestorStatusFilter(InvestorStatusFilter filter) {
            super(filter);
        }

        @Override
        public InvestorStatusFilter copy() {
            return new InvestorStatusFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter investorNo;

    private StringFilter companyName;

    private CompanyPurposeFilter companyPurpose;

    private StringFilter companyRegisterNo;

    private IntegerFilter companyForeignEmployeeNo;

    private IntegerFilter companyLocalEmployeeNo;

    private StringFilter companyTradeChamberNo;

    private LocalDateFilter companyEstablishmentDate;

    private StringFilter companyMobileNo;

    private StringFilter companyEmail;

    private StringFilter companyWebsite;

    private StringFilter companyAddress;

    private StringFilter companyAddressCity;

    private StringFilter companyCapital;

    private StringFilter companyCapitalText;

    private CurrencyFilter companyCapitalCurrency;

    private StringFilter representativeFullName;

    private StringFilter representativeMobileNo;

    private StringFilter representativeEmail;

    private StringFilter representativeNationalNo;

    private StringFilter representativeRegisterNo;

    private StringFilter representativePassportNo;

    private BooleanFilter isRepresentativeForeign;

    private StringFilter notes;

    private InvestorStatusFilter investorStatus;

    private LongFilter userId;

    private LongFilter companyCityId;

    private LongFilter companyCountryId;

    private LongFilter companyAddressCountryId;

    private LongFilter representativeCityId;

    private LongFilter representativeCountryId;

    private LongFilter businessFieldId;

    private Boolean distinct;

    public InvestorCriteria() {}

    public InvestorCriteria(InvestorCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.investorNo = other.investorNo == null ? null : other.investorNo.copy();
        this.companyName = other.companyName == null ? null : other.companyName.copy();
        this.companyPurpose = other.companyPurpose == null ? null : other.companyPurpose.copy();
        this.companyRegisterNo = other.companyRegisterNo == null ? null : other.companyRegisterNo.copy();
        this.companyForeignEmployeeNo = other.companyForeignEmployeeNo == null ? null : other.companyForeignEmployeeNo.copy();
        this.companyLocalEmployeeNo = other.companyLocalEmployeeNo == null ? null : other.companyLocalEmployeeNo.copy();
        this.companyTradeChamberNo = other.companyTradeChamberNo == null ? null : other.companyTradeChamberNo.copy();
        this.companyEstablishmentDate = other.companyEstablishmentDate == null ? null : other.companyEstablishmentDate.copy();
        this.companyMobileNo = other.companyMobileNo == null ? null : other.companyMobileNo.copy();
        this.companyEmail = other.companyEmail == null ? null : other.companyEmail.copy();
        this.companyWebsite = other.companyWebsite == null ? null : other.companyWebsite.copy();
        this.companyAddress = other.companyAddress == null ? null : other.companyAddress.copy();
        this.companyAddressCity = other.companyAddressCity == null ? null : other.companyAddressCity.copy();
        this.companyCapital = other.companyCapital == null ? null : other.companyCapital.copy();
        this.companyCapitalText = other.companyCapitalText == null ? null : other.companyCapitalText.copy();
        this.companyCapitalCurrency = other.companyCapitalCurrency == null ? null : other.companyCapitalCurrency.copy();
        this.representativeFullName = other.representativeFullName == null ? null : other.representativeFullName.copy();
        this.representativeMobileNo = other.representativeMobileNo == null ? null : other.representativeMobileNo.copy();
        this.representativeEmail = other.representativeEmail == null ? null : other.representativeEmail.copy();
        this.representativeNationalNo = other.representativeNationalNo == null ? null : other.representativeNationalNo.copy();
        this.representativeRegisterNo = other.representativeRegisterNo == null ? null : other.representativeRegisterNo.copy();
        this.representativePassportNo = other.representativePassportNo == null ? null : other.representativePassportNo.copy();
        this.isRepresentativeForeign = other.isRepresentativeForeign == null ? null : other.isRepresentativeForeign.copy();
        this.notes = other.notes == null ? null : other.notes.copy();
        this.investorStatus = other.investorStatus == null ? null : other.investorStatus.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.companyCityId = other.companyCityId == null ? null : other.companyCityId.copy();
        this.companyCountryId = other.companyCountryId == null ? null : other.companyCountryId.copy();
        this.companyAddressCountryId = other.companyAddressCountryId == null ? null : other.companyAddressCountryId.copy();
        this.representativeCityId = other.representativeCityId == null ? null : other.representativeCityId.copy();
        this.representativeCountryId = other.representativeCountryId == null ? null : other.representativeCountryId.copy();
        this.businessFieldId = other.businessFieldId == null ? null : other.businessFieldId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public InvestorCriteria copy() {
        return new InvestorCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getInvestorNo() {
        return investorNo;
    }

    public StringFilter investorNo() {
        if (investorNo == null) {
            investorNo = new StringFilter();
        }
        return investorNo;
    }

    public void setInvestorNo(StringFilter investorNo) {
        this.investorNo = investorNo;
    }

    public StringFilter getCompanyName() {
        return companyName;
    }

    public StringFilter companyName() {
        if (companyName == null) {
            companyName = new StringFilter();
        }
        return companyName;
    }

    public void setCompanyName(StringFilter companyName) {
        this.companyName = companyName;
    }

    public CompanyPurposeFilter getCompanyPurpose() {
        return companyPurpose;
    }

    public CompanyPurposeFilter companyPurpose() {
        if (companyPurpose == null) {
            companyPurpose = new CompanyPurposeFilter();
        }
        return companyPurpose;
    }

    public void setCompanyPurpose(CompanyPurposeFilter companyPurpose) {
        this.companyPurpose = companyPurpose;
    }

    public StringFilter getCompanyRegisterNo() {
        return companyRegisterNo;
    }

    public StringFilter companyRegisterNo() {
        if (companyRegisterNo == null) {
            companyRegisterNo = new StringFilter();
        }
        return companyRegisterNo;
    }

    public void setCompanyRegisterNo(StringFilter companyRegisterNo) {
        this.companyRegisterNo = companyRegisterNo;
    }

    public IntegerFilter getCompanyForeignEmployeeNo() {
        return companyForeignEmployeeNo;
    }

    public IntegerFilter companyForeignEmployeeNo() {
        if (companyForeignEmployeeNo == null) {
            companyForeignEmployeeNo = new IntegerFilter();
        }
        return companyForeignEmployeeNo;
    }

    public void setCompanyForeignEmployeeNo(IntegerFilter companyForeignEmployeeNo) {
        this.companyForeignEmployeeNo = companyForeignEmployeeNo;
    }

    public IntegerFilter getCompanyLocalEmployeeNo() {
        return companyLocalEmployeeNo;
    }

    public IntegerFilter companyLocalEmployeeNo() {
        if (companyLocalEmployeeNo == null) {
            companyLocalEmployeeNo = new IntegerFilter();
        }
        return companyLocalEmployeeNo;
    }

    public void setCompanyLocalEmployeeNo(IntegerFilter companyLocalEmployeeNo) {
        this.companyLocalEmployeeNo = companyLocalEmployeeNo;
    }

    public StringFilter getCompanyTradeChamberNo() {
        return companyTradeChamberNo;
    }

    public StringFilter companyTradeChamberNo() {
        if (companyTradeChamberNo == null) {
            companyTradeChamberNo = new StringFilter();
        }
        return companyTradeChamberNo;
    }

    public void setCompanyTradeChamberNo(StringFilter companyTradeChamberNo) {
        this.companyTradeChamberNo = companyTradeChamberNo;
    }

    public LocalDateFilter getCompanyEstablishmentDate() {
        return companyEstablishmentDate;
    }

    public LocalDateFilter companyEstablishmentDate() {
        if (companyEstablishmentDate == null) {
            companyEstablishmentDate = new LocalDateFilter();
        }
        return companyEstablishmentDate;
    }

    public void setCompanyEstablishmentDate(LocalDateFilter companyEstablishmentDate) {
        this.companyEstablishmentDate = companyEstablishmentDate;
    }

    public StringFilter getCompanyMobileNo() {
        return companyMobileNo;
    }

    public StringFilter companyMobileNo() {
        if (companyMobileNo == null) {
            companyMobileNo = new StringFilter();
        }
        return companyMobileNo;
    }

    public void setCompanyMobileNo(StringFilter companyMobileNo) {
        this.companyMobileNo = companyMobileNo;
    }

    public StringFilter getCompanyEmail() {
        return companyEmail;
    }

    public StringFilter companyEmail() {
        if (companyEmail == null) {
            companyEmail = new StringFilter();
        }
        return companyEmail;
    }

    public void setCompanyEmail(StringFilter companyEmail) {
        this.companyEmail = companyEmail;
    }

    public StringFilter getCompanyWebsite() {
        return companyWebsite;
    }

    public StringFilter companyWebsite() {
        if (companyWebsite == null) {
            companyWebsite = new StringFilter();
        }
        return companyWebsite;
    }

    public void setCompanyWebsite(StringFilter companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public StringFilter getCompanyAddress() {
        return companyAddress;
    }

    public StringFilter companyAddress() {
        if (companyAddress == null) {
            companyAddress = new StringFilter();
        }
        return companyAddress;
    }

    public void setCompanyAddress(StringFilter companyAddress) {
        this.companyAddress = companyAddress;
    }

    public StringFilter getCompanyAddressCity() {
        return companyAddressCity;
    }

    public StringFilter companyAddressCity() {
        if (companyAddressCity == null) {
            companyAddressCity = new StringFilter();
        }
        return companyAddressCity;
    }

    public void setCompanyAddressCity(StringFilter companyAddressCity) {
        this.companyAddressCity = companyAddressCity;
    }

    public StringFilter getCompanyCapital() {
        return companyCapital;
    }

    public StringFilter companyCapital() {
        if (companyCapital == null) {
            companyCapital = new StringFilter();
        }
        return companyCapital;
    }

    public void setCompanyCapital(StringFilter companyCapital) {
        this.companyCapital = companyCapital;
    }

    public StringFilter getCompanyCapitalText() {
        return companyCapitalText;
    }

    public StringFilter companyCapitalText() {
        if (companyCapitalText == null) {
            companyCapitalText = new StringFilter();
        }
        return companyCapitalText;
    }

    public void setCompanyCapitalText(StringFilter companyCapitalText) {
        this.companyCapitalText = companyCapitalText;
    }

    public CurrencyFilter getCompanyCapitalCurrency() {
        return companyCapitalCurrency;
    }

    public CurrencyFilter companyCapitalCurrency() {
        if (companyCapitalCurrency == null) {
            companyCapitalCurrency = new CurrencyFilter();
        }
        return companyCapitalCurrency;
    }

    public void setCompanyCapitalCurrency(CurrencyFilter companyCapitalCurrency) {
        this.companyCapitalCurrency = companyCapitalCurrency;
    }

    public StringFilter getRepresentativeFullName() {
        return representativeFullName;
    }

    public StringFilter representativeFullName() {
        if (representativeFullName == null) {
            representativeFullName = new StringFilter();
        }
        return representativeFullName;
    }

    public void setRepresentativeFullName(StringFilter representativeFullName) {
        this.representativeFullName = representativeFullName;
    }

    public StringFilter getRepresentativeMobileNo() {
        return representativeMobileNo;
    }

    public StringFilter representativeMobileNo() {
        if (representativeMobileNo == null) {
            representativeMobileNo = new StringFilter();
        }
        return representativeMobileNo;
    }

    public void setRepresentativeMobileNo(StringFilter representativeMobileNo) {
        this.representativeMobileNo = representativeMobileNo;
    }

    public StringFilter getRepresentativeEmail() {
        return representativeEmail;
    }

    public StringFilter representativeEmail() {
        if (representativeEmail == null) {
            representativeEmail = new StringFilter();
        }
        return representativeEmail;
    }

    public void setRepresentativeEmail(StringFilter representativeEmail) {
        this.representativeEmail = representativeEmail;
    }

    public StringFilter getRepresentativeNationalNo() {
        return representativeNationalNo;
    }

    public StringFilter representativeNationalNo() {
        if (representativeNationalNo == null) {
            representativeNationalNo = new StringFilter();
        }
        return representativeNationalNo;
    }

    public void setRepresentativeNationalNo(StringFilter representativeNationalNo) {
        this.representativeNationalNo = representativeNationalNo;
    }

    public StringFilter getRepresentativeRegisterNo() {
        return representativeRegisterNo;
    }

    public StringFilter representativeRegisterNo() {
        if (representativeRegisterNo == null) {
            representativeRegisterNo = new StringFilter();
        }
        return representativeRegisterNo;
    }

    public void setRepresentativeRegisterNo(StringFilter representativeRegisterNo) {
        this.representativeRegisterNo = representativeRegisterNo;
    }

    public StringFilter getRepresentativePassportNo() {
        return representativePassportNo;
    }

    public StringFilter representativePassportNo() {
        if (representativePassportNo == null) {
            representativePassportNo = new StringFilter();
        }
        return representativePassportNo;
    }

    public void setRepresentativePassportNo(StringFilter representativePassportNo) {
        this.representativePassportNo = representativePassportNo;
    }

    public BooleanFilter getIsRepresentativeForeign() {
        return isRepresentativeForeign;
    }

    public BooleanFilter isRepresentativeForeign() {
        if (isRepresentativeForeign == null) {
            isRepresentativeForeign = new BooleanFilter();
        }
        return isRepresentativeForeign;
    }

    public void setIsRepresentativeForeign(BooleanFilter isRepresentativeForeign) {
        this.isRepresentativeForeign = isRepresentativeForeign;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public StringFilter notes() {
        if (notes == null) {
            notes = new StringFilter();
        }
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public InvestorStatusFilter getInvestorStatus() {
        return investorStatus;
    }

    public InvestorStatusFilter investorStatus() {
        if (investorStatus == null) {
            investorStatus = new InvestorStatusFilter();
        }
        return investorStatus;
    }

    public void setInvestorStatus(InvestorStatusFilter investorStatus) {
        this.investorStatus = investorStatus;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public LongFilter userId() {
        if (userId == null) {
            userId = new LongFilter();
        }
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getCompanyCityId() {
        return companyCityId;
    }

    public LongFilter companyCityId() {
        if (companyCityId == null) {
            companyCityId = new LongFilter();
        }
        return companyCityId;
    }

    public void setCompanyCityId(LongFilter companyCityId) {
        this.companyCityId = companyCityId;
    }

    public LongFilter getCompanyCountryId() {
        return companyCountryId;
    }

    public LongFilter companyCountryId() {
        if (companyCountryId == null) {
            companyCountryId = new LongFilter();
        }
        return companyCountryId;
    }

    public void setCompanyCountryId(LongFilter companyCountryId) {
        this.companyCountryId = companyCountryId;
    }

    public LongFilter getCompanyAddressCountryId() {
        return companyAddressCountryId;
    }

    public LongFilter companyAddressCountryId() {
        if (companyAddressCountryId == null) {
            companyAddressCountryId = new LongFilter();
        }
        return companyAddressCountryId;
    }

    public void setCompanyAddressCountryId(LongFilter companyAddressCountryId) {
        this.companyAddressCountryId = companyAddressCountryId;
    }

    public LongFilter getRepresentativeCityId() {
        return representativeCityId;
    }

    public LongFilter representativeCityId() {
        if (representativeCityId == null) {
            representativeCityId = new LongFilter();
        }
        return representativeCityId;
    }

    public void setRepresentativeCityId(LongFilter representativeCityId) {
        this.representativeCityId = representativeCityId;
    }

    public LongFilter getRepresentativeCountryId() {
        return representativeCountryId;
    }

    public LongFilter representativeCountryId() {
        if (representativeCountryId == null) {
            representativeCountryId = new LongFilter();
        }
        return representativeCountryId;
    }

    public void setRepresentativeCountryId(LongFilter representativeCountryId) {
        this.representativeCountryId = representativeCountryId;
    }

    public LongFilter getBusinessFieldId() {
        return businessFieldId;
    }

    public LongFilter businessFieldId() {
        if (businessFieldId == null) {
            businessFieldId = new LongFilter();
        }
        return businessFieldId;
    }

    public void setBusinessFieldId(LongFilter businessFieldId) {
        this.businessFieldId = businessFieldId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InvestorCriteria that = (InvestorCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(investorNo, that.investorNo) &&
            Objects.equals(companyName, that.companyName) &&
            Objects.equals(companyPurpose, that.companyPurpose) &&
            Objects.equals(companyRegisterNo, that.companyRegisterNo) &&
            Objects.equals(companyForeignEmployeeNo, that.companyForeignEmployeeNo) &&
            Objects.equals(companyLocalEmployeeNo, that.companyLocalEmployeeNo) &&
            Objects.equals(companyTradeChamberNo, that.companyTradeChamberNo) &&
            Objects.equals(companyEstablishmentDate, that.companyEstablishmentDate) &&
            Objects.equals(companyMobileNo, that.companyMobileNo) &&
            Objects.equals(companyEmail, that.companyEmail) &&
            Objects.equals(companyWebsite, that.companyWebsite) &&
            Objects.equals(companyAddress, that.companyAddress) &&
            Objects.equals(companyAddressCity, that.companyAddressCity) &&
            Objects.equals(companyCapital, that.companyCapital) &&
            Objects.equals(companyCapitalText, that.companyCapitalText) &&
            Objects.equals(companyCapitalCurrency, that.companyCapitalCurrency) &&
            Objects.equals(representativeFullName, that.representativeFullName) &&
            Objects.equals(representativeMobileNo, that.representativeMobileNo) &&
            Objects.equals(representativeEmail, that.representativeEmail) &&
            Objects.equals(representativeNationalNo, that.representativeNationalNo) &&
            Objects.equals(representativeRegisterNo, that.representativeRegisterNo) &&
            Objects.equals(representativePassportNo, that.representativePassportNo) &&
            Objects.equals(isRepresentativeForeign, that.isRepresentativeForeign) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(investorStatus, that.investorStatus) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(companyCityId, that.companyCityId) &&
            Objects.equals(companyCountryId, that.companyCountryId) &&
            Objects.equals(companyAddressCountryId, that.companyAddressCountryId) &&
            Objects.equals(representativeCityId, that.representativeCityId) &&
            Objects.equals(representativeCountryId, that.representativeCountryId) &&
            Objects.equals(businessFieldId, that.businessFieldId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            investorNo,
            companyName,
            companyPurpose,
            companyRegisterNo,
            companyForeignEmployeeNo,
            companyLocalEmployeeNo,
            companyTradeChamberNo,
            companyEstablishmentDate,
            companyMobileNo,
            companyEmail,
            companyWebsite,
            companyAddress,
            companyAddressCity,
            companyCapital,
            companyCapitalText,
            companyCapitalCurrency,
            representativeFullName,
            representativeMobileNo,
            representativeEmail,
            representativeNationalNo,
            representativeRegisterNo,
            representativePassportNo,
            isRepresentativeForeign,
            notes,
            investorStatus,
            userId,
            companyCityId,
            companyCountryId,
            companyAddressCountryId,
            representativeCityId,
            representativeCountryId,
            businessFieldId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "InvestorCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (investorNo != null ? "investorNo=" + investorNo + ", " : "") +
            (companyName != null ? "companyName=" + companyName + ", " : "") +
            (companyPurpose != null ? "companyPurpose=" + companyPurpose + ", " : "") +
            (companyRegisterNo != null ? "companyRegisterNo=" + companyRegisterNo + ", " : "") +
            (companyForeignEmployeeNo != null ? "companyForeignEmployeeNo=" + companyForeignEmployeeNo + ", " : "") +
            (companyLocalEmployeeNo != null ? "companyLocalEmployeeNo=" + companyLocalEmployeeNo + ", " : "") +
            (companyTradeChamberNo != null ? "companyTradeChamberNo=" + companyTradeChamberNo + ", " : "") +
            (companyEstablishmentDate != null ? "companyEstablishmentDate=" + companyEstablishmentDate + ", " : "") +
            (companyMobileNo != null ? "companyMobileNo=" + companyMobileNo + ", " : "") +
            (companyEmail != null ? "companyEmail=" + companyEmail + ", " : "") +
            (companyWebsite != null ? "companyWebsite=" + companyWebsite + ", " : "") +
            (companyAddress != null ? "companyAddress=" + companyAddress + ", " : "") +
            (companyAddressCity != null ? "companyAddressCity=" + companyAddressCity + ", " : "") +
            (companyCapital != null ? "companyCapital=" + companyCapital + ", " : "") +
            (companyCapitalText != null ? "companyCapitalText=" + companyCapitalText + ", " : "") +
            (companyCapitalCurrency != null ? "companyCapitalCurrency=" + companyCapitalCurrency + ", " : "") +
            (representativeFullName != null ? "representativeFullName=" + representativeFullName + ", " : "") +
            (representativeMobileNo != null ? "representativeMobileNo=" + representativeMobileNo + ", " : "") +
            (representativeEmail != null ? "representativeEmail=" + representativeEmail + ", " : "") +
            (representativeNationalNo != null ? "representativeNationalNo=" + representativeNationalNo + ", " : "") +
            (representativeRegisterNo != null ? "representativeRegisterNo=" + representativeRegisterNo + ", " : "") +
            (representativePassportNo != null ? "representativePassportNo=" + representativePassportNo + ", " : "") +
            (isRepresentativeForeign != null ? "isRepresentativeForeign=" + isRepresentativeForeign + ", " : "") +
            (notes != null ? "notes=" + notes + ", " : "") +
            (investorStatus != null ? "investorStatus=" + investorStatus + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (companyCityId != null ? "companyCityId=" + companyCityId + ", " : "") +
            (companyCountryId != null ? "companyCountryId=" + companyCountryId + ", " : "") +
            (companyAddressCountryId != null ? "companyAddressCountryId=" + companyAddressCountryId + ", " : "") +
            (representativeCityId != null ? "representativeCityId=" + representativeCityId + ", " : "") +
            (representativeCountryId != null ? "representativeCountryId=" + representativeCountryId + ", " : "") +
            (businessFieldId != null ? "businessFieldId=" + businessFieldId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
