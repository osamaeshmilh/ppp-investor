package ly.ppp.investor.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LocalDateFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.ppp.investor.domain.PaymentReceipt} entity. This class is used
 * in {@link ly.ppp.investor.web.rest.PaymentReceiptResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /payment-receipts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PaymentReceiptCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter amount;

    private StringFilter details;

    private LocalDateFilter receiptDate;

    private LongFilter investorId;

    private Boolean distinct;

    public PaymentReceiptCriteria() {}

    public PaymentReceiptCriteria(PaymentReceiptCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.details = other.details == null ? null : other.details.copy();
        this.receiptDate = other.receiptDate == null ? null : other.receiptDate.copy();
        this.investorId = other.investorId == null ? null : other.investorId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PaymentReceiptCriteria copy() {
        return new PaymentReceiptCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getAmount() {
        return amount;
    }

    public FloatFilter amount() {
        if (amount == null) {
            amount = new FloatFilter();
        }
        return amount;
    }

    public void setAmount(FloatFilter amount) {
        this.amount = amount;
    }

    public StringFilter getDetails() {
        return details;
    }

    public StringFilter details() {
        if (details == null) {
            details = new StringFilter();
        }
        return details;
    }

    public void setDetails(StringFilter details) {
        this.details = details;
    }

    public LocalDateFilter getReceiptDate() {
        return receiptDate;
    }

    public LocalDateFilter receiptDate() {
        if (receiptDate == null) {
            receiptDate = new LocalDateFilter();
        }
        return receiptDate;
    }

    public void setReceiptDate(LocalDateFilter receiptDate) {
        this.receiptDate = receiptDate;
    }

    public LongFilter getInvestorId() {
        return investorId;
    }

    public LongFilter investorId() {
        if (investorId == null) {
            investorId = new LongFilter();
        }
        return investorId;
    }

    public void setInvestorId(LongFilter investorId) {
        this.investorId = investorId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PaymentReceiptCriteria that = (PaymentReceiptCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(details, that.details) &&
            Objects.equals(receiptDate, that.receiptDate) &&
            Objects.equals(investorId, that.investorId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amount, details, receiptDate, investorId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentReceiptCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (amount != null ? "amount=" + amount + ", " : "") +
            (details != null ? "details=" + details + ", " : "") +
            (receiptDate != null ? "receiptDate=" + receiptDate + ", " : "") +
            (investorId != null ? "investorId=" + investorId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
