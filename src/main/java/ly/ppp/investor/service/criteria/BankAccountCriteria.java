package ly.ppp.investor.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.ppp.investor.domain.BankAccount} entity. This class is used
 * in {@link ly.ppp.investor.web.rest.BankAccountResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bank-accounts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BankAccountCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter accountNo;

    private StringFilter bankBranchName;

    private StringFilter notes;

    private LongFilter bankId;

    private LongFilter investorId;

    private Boolean distinct;

    public BankAccountCriteria() {}

    public BankAccountCriteria(BankAccountCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.accountNo = other.accountNo == null ? null : other.accountNo.copy();
        this.bankBranchName = other.bankBranchName == null ? null : other.bankBranchName.copy();
        this.notes = other.notes == null ? null : other.notes.copy();
        this.bankId = other.bankId == null ? null : other.bankId.copy();
        this.investorId = other.investorId == null ? null : other.investorId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public BankAccountCriteria copy() {
        return new BankAccountCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAccountNo() {
        return accountNo;
    }

    public StringFilter accountNo() {
        if (accountNo == null) {
            accountNo = new StringFilter();
        }
        return accountNo;
    }

    public void setAccountNo(StringFilter accountNo) {
        this.accountNo = accountNo;
    }

    public StringFilter getBankBranchName() {
        return bankBranchName;
    }

    public StringFilter bankBranchName() {
        if (bankBranchName == null) {
            bankBranchName = new StringFilter();
        }
        return bankBranchName;
    }

    public void setBankBranchName(StringFilter bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public StringFilter notes() {
        if (notes == null) {
            notes = new StringFilter();
        }
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public LongFilter getBankId() {
        return bankId;
    }

    public LongFilter bankId() {
        if (bankId == null) {
            bankId = new LongFilter();
        }
        return bankId;
    }

    public void setBankId(LongFilter bankId) {
        this.bankId = bankId;
    }

    public LongFilter getInvestorId() {
        return investorId;
    }

    public LongFilter investorId() {
        if (investorId == null) {
            investorId = new LongFilter();
        }
        return investorId;
    }

    public void setInvestorId(LongFilter investorId) {
        this.investorId = investorId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BankAccountCriteria that = (BankAccountCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(accountNo, that.accountNo) &&
            Objects.equals(bankBranchName, that.bankBranchName) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(bankId, that.bankId) &&
            Objects.equals(investorId, that.investorId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNo, bankBranchName, notes, bankId, investorId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BankAccountCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (accountNo != null ? "accountNo=" + accountNo + ", " : "") +
            (bankBranchName != null ? "bankBranchName=" + bankBranchName + ", " : "") +
            (notes != null ? "notes=" + notes + ", " : "") +
            (bankId != null ? "bankId=" + bankId + ", " : "") +
            (investorId != null ? "investorId=" + investorId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
