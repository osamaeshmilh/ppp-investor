package ly.ppp.investor.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import ly.ppp.investor.domain.enumeration.Currency;
import ly.ppp.investor.domain.enumeration.ProjectStatus;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link ly.ppp.investor.domain.Project} entity. This class is used
 * in {@link ly.ppp.investor.web.rest.ProjectResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /projects?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProjectCriteria implements Serializable, Criteria {

    /**
     * Class for filtering Currency
     */
    public static class CurrencyFilter extends Filter<Currency> {

        public CurrencyFilter() {}

        public CurrencyFilter(CurrencyFilter filter) {
            super(filter);
        }

        @Override
        public CurrencyFilter copy() {
            return new CurrencyFilter(this);
        }
    }

    /**
     * Class for filtering ProjectStatus
     */
    public static class ProjectStatusFilter extends Filter<ProjectStatus> {

        public ProjectStatusFilter() {}

        public ProjectStatusFilter(ProjectStatusFilter filter) {
            super(filter);
        }

        @Override
        public ProjectStatusFilter copy() {
            return new ProjectStatusFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter projectNo;

    private StringFilter name;

    private StringFilter nameEn;

    private StringFilter details;

    private StringFilter detailsEn;

    private StringFilter address;

    private FloatFilter estimatedCost;

    private CurrencyFilter estimatedCostCurrency;

    private ProjectStatusFilter projectStatus;

    private FloatFilter lat;

    private FloatFilter lng;

    private LongFilter businessFieldId;

    private LongFilter cityId;

    private LongFilter countryId;

    private LongFilter investorId;

    private LongFilter orginizationId;

    private Boolean distinct;

    public ProjectCriteria() {}

    public ProjectCriteria(ProjectCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.projectNo = other.projectNo == null ? null : other.projectNo.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.nameEn = other.nameEn == null ? null : other.nameEn.copy();
        this.details = other.details == null ? null : other.details.copy();
        this.detailsEn = other.detailsEn == null ? null : other.detailsEn.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.estimatedCost = other.estimatedCost == null ? null : other.estimatedCost.copy();
        this.estimatedCostCurrency = other.estimatedCostCurrency == null ? null : other.estimatedCostCurrency.copy();
        this.projectStatus = other.projectStatus == null ? null : other.projectStatus.copy();
        this.lat = other.lat == null ? null : other.lat.copy();
        this.lng = other.lng == null ? null : other.lng.copy();
        this.businessFieldId = other.businessFieldId == null ? null : other.businessFieldId.copy();
        this.cityId = other.cityId == null ? null : other.cityId.copy();
        this.countryId = other.countryId == null ? null : other.countryId.copy();
        this.investorId = other.investorId == null ? null : other.investorId.copy();
        this.orginizationId = other.orginizationId == null ? null : other.orginizationId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ProjectCriteria copy() {
        return new ProjectCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getProjectNo() {
        return projectNo;
    }

    public StringFilter projectNo() {
        if (projectNo == null) {
            projectNo = new StringFilter();
        }
        return projectNo;
    }

    public void setProjectNo(StringFilter projectNo) {
        this.projectNo = projectNo;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getNameEn() {
        return nameEn;
    }

    public StringFilter nameEn() {
        if (nameEn == null) {
            nameEn = new StringFilter();
        }
        return nameEn;
    }

    public void setNameEn(StringFilter nameEn) {
        this.nameEn = nameEn;
    }

    public StringFilter getDetails() {
        return details;
    }

    public StringFilter details() {
        if (details == null) {
            details = new StringFilter();
        }
        return details;
    }

    public void setDetails(StringFilter details) {
        this.details = details;
    }

    public StringFilter getDetailsEn() {
        return detailsEn;
    }

    public StringFilter detailsEn() {
        if (detailsEn == null) {
            detailsEn = new StringFilter();
        }
        return detailsEn;
    }

    public void setDetailsEn(StringFilter detailsEn) {
        this.detailsEn = detailsEn;
    }

    public StringFilter getAddress() {
        return address;
    }

    public StringFilter address() {
        if (address == null) {
            address = new StringFilter();
        }
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public FloatFilter getEstimatedCost() {
        return estimatedCost;
    }

    public FloatFilter estimatedCost() {
        if (estimatedCost == null) {
            estimatedCost = new FloatFilter();
        }
        return estimatedCost;
    }

    public void setEstimatedCost(FloatFilter estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public CurrencyFilter getEstimatedCostCurrency() {
        return estimatedCostCurrency;
    }

    public CurrencyFilter estimatedCostCurrency() {
        if (estimatedCostCurrency == null) {
            estimatedCostCurrency = new CurrencyFilter();
        }
        return estimatedCostCurrency;
    }

    public void setEstimatedCostCurrency(CurrencyFilter estimatedCostCurrency) {
        this.estimatedCostCurrency = estimatedCostCurrency;
    }

    public ProjectStatusFilter getProjectStatus() {
        return projectStatus;
    }

    public ProjectStatusFilter projectStatus() {
        if (projectStatus == null) {
            projectStatus = new ProjectStatusFilter();
        }
        return projectStatus;
    }

    public void setProjectStatus(ProjectStatusFilter projectStatus) {
        this.projectStatus = projectStatus;
    }

    public FloatFilter getLat() {
        return lat;
    }

    public FloatFilter lat() {
        if (lat == null) {
            lat = new FloatFilter();
        }
        return lat;
    }

    public void setLat(FloatFilter lat) {
        this.lat = lat;
    }

    public FloatFilter getLng() {
        return lng;
    }

    public FloatFilter lng() {
        if (lng == null) {
            lng = new FloatFilter();
        }
        return lng;
    }

    public void setLng(FloatFilter lng) {
        this.lng = lng;
    }

    public LongFilter getBusinessFieldId() {
        return businessFieldId;
    }

    public LongFilter businessFieldId() {
        if (businessFieldId == null) {
            businessFieldId = new LongFilter();
        }
        return businessFieldId;
    }

    public void setBusinessFieldId(LongFilter businessFieldId) {
        this.businessFieldId = businessFieldId;
    }

    public LongFilter getCityId() {
        return cityId;
    }

    public LongFilter cityId() {
        if (cityId == null) {
            cityId = new LongFilter();
        }
        return cityId;
    }

    public void setCityId(LongFilter cityId) {
        this.cityId = cityId;
    }

    public LongFilter getCountryId() {
        return countryId;
    }

    public LongFilter countryId() {
        if (countryId == null) {
            countryId = new LongFilter();
        }
        return countryId;
    }

    public void setCountryId(LongFilter countryId) {
        this.countryId = countryId;
    }

    public LongFilter getInvestorId() {
        return investorId;
    }

    public LongFilter investorId() {
        if (investorId == null) {
            investorId = new LongFilter();
        }
        return investorId;
    }

    public void setInvestorId(LongFilter investorId) {
        this.investorId = investorId;
    }

    public LongFilter getOrginizationId() {
        return orginizationId;
    }

    public LongFilter orginizationId() {
        if (orginizationId == null) {
            orginizationId = new LongFilter();
        }
        return orginizationId;
    }

    public void setOrginizationId(LongFilter orginizationId) {
        this.orginizationId = orginizationId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProjectCriteria that = (ProjectCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(projectNo, that.projectNo) &&
            Objects.equals(name, that.name) &&
            Objects.equals(nameEn, that.nameEn) &&
            Objects.equals(details, that.details) &&
            Objects.equals(detailsEn, that.detailsEn) &&
            Objects.equals(address, that.address) &&
            Objects.equals(estimatedCost, that.estimatedCost) &&
            Objects.equals(estimatedCostCurrency, that.estimatedCostCurrency) &&
            Objects.equals(projectStatus, that.projectStatus) &&
            Objects.equals(lat, that.lat) &&
            Objects.equals(lng, that.lng) &&
            Objects.equals(businessFieldId, that.businessFieldId) &&
            Objects.equals(cityId, that.cityId) &&
            Objects.equals(countryId, that.countryId) &&
            Objects.equals(investorId, that.investorId) &&
            Objects.equals(orginizationId, that.orginizationId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            projectNo,
            name,
            nameEn,
            details,
            detailsEn,
            address,
            estimatedCost,
            estimatedCostCurrency,
            projectStatus,
            lat,
            lng,
            businessFieldId,
            cityId,
            countryId,
            investorId,
            orginizationId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProjectCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (projectNo != null ? "projectNo=" + projectNo + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (nameEn != null ? "nameEn=" + nameEn + ", " : "") +
            (details != null ? "details=" + details + ", " : "") +
            (detailsEn != null ? "detailsEn=" + detailsEn + ", " : "") +
            (address != null ? "address=" + address + ", " : "") +
            (estimatedCost != null ? "estimatedCost=" + estimatedCost + ", " : "") +
            (estimatedCostCurrency != null ? "estimatedCostCurrency=" + estimatedCostCurrency + ", " : "") +
            (projectStatus != null ? "projectStatus=" + projectStatus + ", " : "") +
            (lat != null ? "lat=" + lat + ", " : "") +
            (lng != null ? "lng=" + lng + ", " : "") +
            (businessFieldId != null ? "businessFieldId=" + businessFieldId + ", " : "") +
            (cityId != null ? "cityId=" + cityId + ", " : "") +
            (countryId != null ? "countryId=" + countryId + ", " : "") +
            (investorId != null ? "investorId=" + investorId + ", " : "") +
            (orginizationId != null ? "orginizationId=" + orginizationId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
