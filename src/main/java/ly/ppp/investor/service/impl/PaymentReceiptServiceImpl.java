package ly.ppp.investor.service.impl;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Optional;
import ly.ppp.investor.domain.PaymentReceipt;
import ly.ppp.investor.repository.PaymentReceiptRepository;
import ly.ppp.investor.service.PaymentReceiptService;
import ly.ppp.investor.service.dto.InvestorDTO;
import ly.ppp.investor.service.dto.PaymentReceiptDTO;
import ly.ppp.investor.service.mapper.PaymentReceiptMapper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PaymentReceipt}.
 */
@Service
@Transactional
public class PaymentReceiptServiceImpl implements PaymentReceiptService {

    private final Logger log = LoggerFactory.getLogger(PaymentReceiptServiceImpl.class);

    private final PaymentReceiptRepository paymentReceiptRepository;

    private final PaymentReceiptMapper paymentReceiptMapper;

    public PaymentReceiptServiceImpl(PaymentReceiptRepository paymentReceiptRepository, PaymentReceiptMapper paymentReceiptMapper) {
        this.paymentReceiptRepository = paymentReceiptRepository;
        this.paymentReceiptMapper = paymentReceiptMapper;
    }

    @Override
    public PaymentReceiptDTO save(PaymentReceiptDTO paymentReceiptDTO) {
        log.debug("Request to save PaymentReceipt : {}", paymentReceiptDTO);
        PaymentReceipt paymentReceipt = paymentReceiptMapper.toEntity(paymentReceiptDTO);
        paymentReceipt = paymentReceiptRepository.save(paymentReceipt);
        return paymentReceiptMapper.toDto(paymentReceipt);
    }

    @Override
    public Optional<PaymentReceiptDTO> partialUpdate(PaymentReceiptDTO paymentReceiptDTO) {
        log.debug("Request to partially update PaymentReceipt : {}", paymentReceiptDTO);

        return paymentReceiptRepository
            .findById(paymentReceiptDTO.getId())
            .map(existingPaymentReceipt -> {
                paymentReceiptMapper.partialUpdate(existingPaymentReceipt, paymentReceiptDTO);

                return existingPaymentReceipt;
            })
            .map(paymentReceiptRepository::save)
            .map(paymentReceiptMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PaymentReceiptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentReceipts");
        return paymentReceiptRepository.findAll(pageable).map(paymentReceiptMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PaymentReceiptDTO> findAll() {
        log.debug("Request to get all PaymentReceipts");
        return paymentReceiptMapper.toDto(paymentReceiptRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PaymentReceiptDTO> findOne(Long id) {
        log.debug("Request to get PaymentReceipt : {}", id);
        return paymentReceiptRepository.findById(id).map(paymentReceiptMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PaymentReceipt : {}", id);
        paymentReceiptRepository.deleteById(id);
    }

    @Override
    public byte[] getAllXslx() {
        String[] columns = { "ر.ت", "القيمة", "التفاصيل", "اسم المستثمر", "التاريخ" };
        List<PaymentReceiptDTO> paymentReceiptDTOS = findAll();

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Payments");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Other rows and cells with contacts data
        int rowNum = 1;

        for (PaymentReceiptDTO paymentReceiptDTO : paymentReceiptDTOS) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(paymentReceiptDTO.getId());
            row.createCell(1).setCellValue(paymentReceiptDTO.getAmount());
            row.createCell(2).setCellValue(paymentReceiptDTO.getDetails());
            row.createCell(3).setCellValue(paymentReceiptDTO.getInvestor() != null ? paymentReceiptDTO.getInvestor().getCompanyName() : "");
            row.createCell(4).setCellValue(paymentReceiptDTO.getReceiptDate().toString());
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        byte[] bytes = new byte[0];

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            bos.close();
            bytes = bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bytes;
    }
}
