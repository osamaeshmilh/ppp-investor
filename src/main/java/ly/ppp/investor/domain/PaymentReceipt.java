package ly.ppp.investor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A PaymentReceipt.
 */
@Entity
@Table(name = "payment_receipt")
public class PaymentReceipt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "amount")
    private Float amount;

    @Column(name = "details")
    private String details;

    @Column(name = "receipt_date")
    private LocalDate receiptDate;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "user",
            "companyCity",
            "companyCountry",
            "companyAddressCountry",
            "representativeCity",
            "representativeCountry",
            "businessField",
        },
        allowSetters = true
    )
    private Investor investor;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PaymentReceipt id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return this.amount;
    }

    public PaymentReceipt amount(Float amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return this.details;
    }

    public PaymentReceipt details(String details) {
        this.setDetails(details);
        return this;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LocalDate getReceiptDate() {
        return this.receiptDate;
    }

    public PaymentReceipt receiptDate(LocalDate receiptDate) {
        this.setReceiptDate(receiptDate);
        return this;
    }

    public void setReceiptDate(LocalDate receiptDate) {
        this.receiptDate = receiptDate;
    }

    public Investor getInvestor() {
        return this.investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public PaymentReceipt investor(Investor investor) {
        this.setInvestor(investor);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentReceipt)) {
            return false;
        }
        return id != null && id.equals(((PaymentReceipt) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentReceipt{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", details='" + getDetails() + "'" +
            ", receiptDate='" + getReceiptDate() + "'" +
            "}";
    }
}
