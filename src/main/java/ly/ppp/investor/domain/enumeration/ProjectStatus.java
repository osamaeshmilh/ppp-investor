package ly.ppp.investor.domain.enumeration;

/**
 * The ProjectStatus enumeration.
 */
public enum ProjectStatus {
    PENDING,
    APPROVED,
    ACTIVE,
    READY_NOT_ACTIVE,
    UNDER_DEVELOPMENT,
}
