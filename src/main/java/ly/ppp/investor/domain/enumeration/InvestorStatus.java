package ly.ppp.investor.domain.enumeration;

/**
 * The InvestorStatus enumeration.
 */
public enum InvestorStatus {
    PENDING,
    APPROVED,
    CANCELED,
    REJECTED,
}
