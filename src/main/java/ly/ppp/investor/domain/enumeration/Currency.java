package ly.ppp.investor.domain.enumeration;

/**
 * The Currency enumeration.
 */
public enum Currency {
    LYD,
    USD,
    EUR,
    TND,
}
