package ly.ppp.investor.domain.enumeration;

/**
 * The AttachmentType enumeration.
 */
public enum AttachmentType {
    COMMERCIAL_REGISTER,
    COMMERCIAL_LICENCE,
    ID_DOCUMENT,
    FEASIBILITY,
    STUDY,
    OTHER,
}
