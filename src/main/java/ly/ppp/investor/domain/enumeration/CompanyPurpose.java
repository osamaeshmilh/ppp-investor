package ly.ppp.investor.domain.enumeration;

/**
 * The CompanyPurpose enumeration.
 */
public enum CompanyPurpose {
    SERVICE,
    MANUFACTURE,
    IMPORT,
    EXPORT,
    PRODUCE,
    INSURANCE,
    BANKING,
    INVEST,
    OTHER,
    NONE,
}
