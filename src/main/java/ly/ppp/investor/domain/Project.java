package ly.ppp.investor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import ly.ppp.investor.domain.enumeration.Currency;
import ly.ppp.investor.domain.enumeration.ProjectStatus;
import ly.ppp.investor.service.dto.AbstractAuditingDTO;

/**
 * A Project.
 */
@Entity
@Table(name = "project")
public class Project extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "project_no")
    private String projectNo;

    @Column(name = "name")
    private String name;

    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "details")
    private String details;

    @Column(name = "details_en")
    private String detailsEn;

    @Column(name = "address")
    private String address;

    @Column(name = "estimated_cost")
    private Float estimatedCost;

    @Enumerated(EnumType.STRING)
    @Column(name = "estimated_cost_currency")
    private Currency estimatedCostCurrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "project_status")
    private ProjectStatus projectStatus;

    @Column(name = "lat")
    private Float lat;

    @Column(name = "lng")
    private Float lng;

    @ManyToOne
    private BusinessField businessField;

    @ManyToOne
    private City city;

    @ManyToOne
    private Country country;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "user",
            "companyCity",
            "companyCountry",
            "companyAddressCountry",
            "representativeCity",
            "representativeCountry",
            "businessField",
        },
        allowSetters = true
    )
    private Investor investor;

    @ManyToOne
    private Orginization orginization;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Project id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectNo() {
        return this.projectNo;
    }

    public Project projectNo(String projectNo) {
        this.setProjectNo(projectNo);
        return this;
    }

    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }

    public String getName() {
        return this.name;
    }

    public Project name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameEn() {
        return this.nameEn;
    }

    public Project nameEn(String nameEn) {
        this.setNameEn(nameEn);
        return this;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getDetails() {
        return this.details;
    }

    public Project details(String details) {
        this.setDetails(details);
        return this;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDetailsEn() {
        return this.detailsEn;
    }

    public Project detailsEn(String detailsEn) {
        this.setDetailsEn(detailsEn);
        return this;
    }

    public void setDetailsEn(String detailsEn) {
        this.detailsEn = detailsEn;
    }

    public String getAddress() {
        return this.address;
    }

    public Project address(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getEstimatedCost() {
        return this.estimatedCost;
    }

    public Project estimatedCost(Float estimatedCost) {
        this.setEstimatedCost(estimatedCost);
        return this;
    }

    public void setEstimatedCost(Float estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public Currency getEstimatedCostCurrency() {
        return this.estimatedCostCurrency;
    }

    public Project estimatedCostCurrency(Currency estimatedCostCurrency) {
        this.setEstimatedCostCurrency(estimatedCostCurrency);
        return this;
    }

    public void setEstimatedCostCurrency(Currency estimatedCostCurrency) {
        this.estimatedCostCurrency = estimatedCostCurrency;
    }

    public ProjectStatus getProjectStatus() {
        return this.projectStatus;
    }

    public Project projectStatus(ProjectStatus projectStatus) {
        this.setProjectStatus(projectStatus);
        return this;
    }

    public void setProjectStatus(ProjectStatus projectStatus) {
        this.projectStatus = projectStatus;
    }

    public Float getLat() {
        return this.lat;
    }

    public Project lat(Float lat) {
        this.setLat(lat);
        return this;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return this.lng;
    }

    public Project lng(Float lng) {
        this.setLng(lng);
        return this;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public BusinessField getBusinessField() {
        return this.businessField;
    }

    public void setBusinessField(BusinessField businessField) {
        this.businessField = businessField;
    }

    public Project businessField(BusinessField businessField) {
        this.setBusinessField(businessField);
        return this;
    }

    public City getCity() {
        return this.city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Project city(City city) {
        this.setCity(city);
        return this;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Project country(Country country) {
        this.setCountry(country);
        return this;
    }

    public Investor getInvestor() {
        return this.investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public Project investor(Investor investor) {
        this.setInvestor(investor);
        return this;
    }

    public Orginization getOrginization() {
        return this.orginization;
    }

    public void setOrginization(Orginization orginization) {
        this.orginization = orginization;
    }

    public Project orginization(Orginization orginization) {
        this.setOrginization(orginization);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Project)) {
            return false;
        }
        return id != null && id.equals(((Project) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Project{" +
            "id=" + getId() +
            ", projectNo='" + getProjectNo() + "'" +
            ", name='" + getName() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", details='" + getDetails() + "'" +
            ", detailsEn='" + getDetailsEn() + "'" +
            ", address='" + getAddress() + "'" +
            ", estimatedCost=" + getEstimatedCost() +
            ", estimatedCostCurrency='" + getEstimatedCostCurrency() + "'" +
            ", projectStatus='" + getProjectStatus() + "'" +
            ", lat=" + getLat() +
            ", lng=" + getLng() +
            "}";
    }
}
