package ly.ppp.investor.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import ly.ppp.investor.domain.enumeration.CompanyPurpose;
import ly.ppp.investor.domain.enumeration.Currency;
import ly.ppp.investor.domain.enumeration.InvestorStatus;

/**
 * A Investor.
 */
@Entity
@Table(name = "investor")
public class Investor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "investor_no")
    private String investorNo;

    @NotNull
    @Column(name = "company_name", nullable = false)
    private String companyName;

    @Enumerated(EnumType.STRING)
    @Column(name = "company_purpose")
    private CompanyPurpose companyPurpose;

    @Column(name = "company_register_no")
    private String companyRegisterNo;

    @Column(name = "company_foreign_employee_no")
    private Integer companyForeignEmployeeNo;

    @Column(name = "company_local_employee_no")
    private Integer companyLocalEmployeeNo;

    @Column(name = "company_trade_chamber_no")
    private String companyTradeChamberNo;

    @Column(name = "company_establishment_date")
    private LocalDate companyEstablishmentDate;

    @Column(name = "company_mobile_no")
    private String companyMobileNo;

    @Column(name = "company_email")
    private String companyEmail;

    @Column(name = "company_website")
    private String companyWebsite;

    @Column(name = "company_address")
    private String companyAddress;

    @Column(name = "company_address_city")
    private String companyAddressCity;

    @Column(name = "company_capital")
    private String companyCapital;

    @Column(name = "company_capital_text")
    private String companyCapitalText;

    @Enumerated(EnumType.STRING)
    @Column(name = "company_capital_currency")
    private Currency companyCapitalCurrency;

    @Column(name = "representative_full_name")
    private String representativeFullName;

    @Column(name = "representative_mobile_no")
    private String representativeMobileNo;

    @Column(name = "representative_email")
    private String representativeEmail;

    @Column(name = "representative_national_no")
    private String representativeNationalNo;

    @Column(name = "representative_register_no")
    private String representativeRegisterNo;

    @Column(name = "representative_passport_no")
    private String representativePassportNo;

    @Column(name = "is_representative_foreign")
    private Boolean isRepresentativeForeign;

    @Column(name = "notes")
    private String notes;

    @Enumerated(EnumType.STRING)
    @Column(name = "investor_status")
    private InvestorStatus investorStatus;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @ManyToOne
    private City companyCity;

    @ManyToOne
    private Country companyCountry;

    @ManyToOne
    private Country companyAddressCountry;

    @ManyToOne
    private City representativeCity;

    @ManyToOne
    private Country representativeCountry;

    @ManyToOne
    private BusinessField businessField;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Investor id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvestorNo() {
        return this.investorNo;
    }

    public Investor investorNo(String investorNo) {
        this.setInvestorNo(investorNo);
        return this;
    }

    public void setInvestorNo(String investorNo) {
        this.investorNo = investorNo;
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public Investor companyName(String companyName) {
        this.setCompanyName(companyName);
        return this;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public CompanyPurpose getCompanyPurpose() {
        return this.companyPurpose;
    }

    public Investor companyPurpose(CompanyPurpose companyPurpose) {
        this.setCompanyPurpose(companyPurpose);
        return this;
    }

    public void setCompanyPurpose(CompanyPurpose companyPurpose) {
        this.companyPurpose = companyPurpose;
    }

    public String getCompanyRegisterNo() {
        return this.companyRegisterNo;
    }

    public Investor companyRegisterNo(String companyRegisterNo) {
        this.setCompanyRegisterNo(companyRegisterNo);
        return this;
    }

    public void setCompanyRegisterNo(String companyRegisterNo) {
        this.companyRegisterNo = companyRegisterNo;
    }

    public Integer getCompanyForeignEmployeeNo() {
        return this.companyForeignEmployeeNo;
    }

    public Investor companyForeignEmployeeNo(Integer companyForeignEmployeeNo) {
        this.setCompanyForeignEmployeeNo(companyForeignEmployeeNo);
        return this;
    }

    public void setCompanyForeignEmployeeNo(Integer companyForeignEmployeeNo) {
        this.companyForeignEmployeeNo = companyForeignEmployeeNo;
    }

    public Integer getCompanyLocalEmployeeNo() {
        return this.companyLocalEmployeeNo;
    }

    public Investor companyLocalEmployeeNo(Integer companyLocalEmployeeNo) {
        this.setCompanyLocalEmployeeNo(companyLocalEmployeeNo);
        return this;
    }

    public void setCompanyLocalEmployeeNo(Integer companyLocalEmployeeNo) {
        this.companyLocalEmployeeNo = companyLocalEmployeeNo;
    }

    public String getCompanyTradeChamberNo() {
        return this.companyTradeChamberNo;
    }

    public Investor companyTradeChamberNo(String companyTradeChamberNo) {
        this.setCompanyTradeChamberNo(companyTradeChamberNo);
        return this;
    }

    public void setCompanyTradeChamberNo(String companyTradeChamberNo) {
        this.companyTradeChamberNo = companyTradeChamberNo;
    }

    public LocalDate getCompanyEstablishmentDate() {
        return this.companyEstablishmentDate;
    }

    public Investor companyEstablishmentDate(LocalDate companyEstablishmentDate) {
        this.setCompanyEstablishmentDate(companyEstablishmentDate);
        return this;
    }

    public void setCompanyEstablishmentDate(LocalDate companyEstablishmentDate) {
        this.companyEstablishmentDate = companyEstablishmentDate;
    }

    public String getCompanyMobileNo() {
        return this.companyMobileNo;
    }

    public Investor companyMobileNo(String companyMobileNo) {
        this.setCompanyMobileNo(companyMobileNo);
        return this;
    }

    public void setCompanyMobileNo(String companyMobileNo) {
        this.companyMobileNo = companyMobileNo;
    }

    public String getCompanyEmail() {
        return this.companyEmail;
    }

    public Investor companyEmail(String companyEmail) {
        this.setCompanyEmail(companyEmail);
        return this;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyWebsite() {
        return this.companyWebsite;
    }

    public Investor companyWebsite(String companyWebsite) {
        this.setCompanyWebsite(companyWebsite);
        return this;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getCompanyAddress() {
        return this.companyAddress;
    }

    public Investor companyAddress(String companyAddress) {
        this.setCompanyAddress(companyAddress);
        return this;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyAddressCity() {
        return this.companyAddressCity;
    }

    public Investor companyAddressCity(String companyAddressCity) {
        this.setCompanyAddressCity(companyAddressCity);
        return this;
    }

    public void setCompanyAddressCity(String companyAddressCity) {
        this.companyAddressCity = companyAddressCity;
    }

    public String getCompanyCapital() {
        return this.companyCapital;
    }

    public Investor companyCapital(String companyCapital) {
        this.setCompanyCapital(companyCapital);
        return this;
    }

    public void setCompanyCapital(String companyCapital) {
        this.companyCapital = companyCapital;
    }

    public String getCompanyCapitalText() {
        return this.companyCapitalText;
    }

    public Investor companyCapitalText(String companyCapitalText) {
        this.setCompanyCapitalText(companyCapitalText);
        return this;
    }

    public void setCompanyCapitalText(String companyCapitalText) {
        this.companyCapitalText = companyCapitalText;
    }

    public Currency getCompanyCapitalCurrency() {
        return this.companyCapitalCurrency;
    }

    public Investor companyCapitalCurrency(Currency companyCapitalCurrency) {
        this.setCompanyCapitalCurrency(companyCapitalCurrency);
        return this;
    }

    public void setCompanyCapitalCurrency(Currency companyCapitalCurrency) {
        this.companyCapitalCurrency = companyCapitalCurrency;
    }

    public String getRepresentativeFullName() {
        return this.representativeFullName;
    }

    public Investor representativeFullName(String representativeFullName) {
        this.setRepresentativeFullName(representativeFullName);
        return this;
    }

    public void setRepresentativeFullName(String representativeFullName) {
        this.representativeFullName = representativeFullName;
    }

    public String getRepresentativeMobileNo() {
        return this.representativeMobileNo;
    }

    public Investor representativeMobileNo(String representativeMobileNo) {
        this.setRepresentativeMobileNo(representativeMobileNo);
        return this;
    }

    public void setRepresentativeMobileNo(String representativeMobileNo) {
        this.representativeMobileNo = representativeMobileNo;
    }

    public String getRepresentativeEmail() {
        return this.representativeEmail;
    }

    public Investor representativeEmail(String representativeEmail) {
        this.setRepresentativeEmail(representativeEmail);
        return this;
    }

    public void setRepresentativeEmail(String representativeEmail) {
        this.representativeEmail = representativeEmail;
    }

    public String getRepresentativeNationalNo() {
        return this.representativeNationalNo;
    }

    public Investor representativeNationalNo(String representativeNationalNo) {
        this.setRepresentativeNationalNo(representativeNationalNo);
        return this;
    }

    public void setRepresentativeNationalNo(String representativeNationalNo) {
        this.representativeNationalNo = representativeNationalNo;
    }

    public String getRepresentativeRegisterNo() {
        return this.representativeRegisterNo;
    }

    public Investor representativeRegisterNo(String representativeRegisterNo) {
        this.setRepresentativeRegisterNo(representativeRegisterNo);
        return this;
    }

    public void setRepresentativeRegisterNo(String representativeRegisterNo) {
        this.representativeRegisterNo = representativeRegisterNo;
    }

    public String getRepresentativePassportNo() {
        return this.representativePassportNo;
    }

    public Investor representativePassportNo(String representativePassportNo) {
        this.setRepresentativePassportNo(representativePassportNo);
        return this;
    }

    public void setRepresentativePassportNo(String representativePassportNo) {
        this.representativePassportNo = representativePassportNo;
    }

    public Boolean getIsRepresentativeForeign() {
        return this.isRepresentativeForeign;
    }

    public Investor isRepresentativeForeign(Boolean isRepresentativeForeign) {
        this.setIsRepresentativeForeign(isRepresentativeForeign);
        return this;
    }

    public void setIsRepresentativeForeign(Boolean isRepresentativeForeign) {
        this.isRepresentativeForeign = isRepresentativeForeign;
    }

    public String getNotes() {
        return this.notes;
    }

    public Investor notes(String notes) {
        this.setNotes(notes);
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public InvestorStatus getInvestorStatus() {
        return this.investorStatus;
    }

    public Investor investorStatus(InvestorStatus investorStatus) {
        this.setInvestorStatus(investorStatus);
        return this;
    }

    public void setInvestorStatus(InvestorStatus investorStatus) {
        this.investorStatus = investorStatus;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Investor user(User user) {
        this.setUser(user);
        return this;
    }

    public City getCompanyCity() {
        return this.companyCity;
    }

    public void setCompanyCity(City city) {
        this.companyCity = city;
    }

    public Investor companyCity(City city) {
        this.setCompanyCity(city);
        return this;
    }

    public Country getCompanyCountry() {
        return this.companyCountry;
    }

    public void setCompanyCountry(Country country) {
        this.companyCountry = country;
    }

    public Investor companyCountry(Country country) {
        this.setCompanyCountry(country);
        return this;
    }

    public Country getCompanyAddressCountry() {
        return this.companyAddressCountry;
    }

    public void setCompanyAddressCountry(Country country) {
        this.companyAddressCountry = country;
    }

    public Investor companyAddressCountry(Country country) {
        this.setCompanyAddressCountry(country);
        return this;
    }

    public City getRepresentativeCity() {
        return this.representativeCity;
    }

    public void setRepresentativeCity(City city) {
        this.representativeCity = city;
    }

    public Investor representativeCity(City city) {
        this.setRepresentativeCity(city);
        return this;
    }

    public Country getRepresentativeCountry() {
        return this.representativeCountry;
    }

    public void setRepresentativeCountry(Country country) {
        this.representativeCountry = country;
    }

    public Investor representativeCountry(Country country) {
        this.setRepresentativeCountry(country);
        return this;
    }

    public BusinessField getBusinessField() {
        return this.businessField;
    }

    public void setBusinessField(BusinessField businessField) {
        this.businessField = businessField;
    }

    public Investor businessField(BusinessField businessField) {
        this.setBusinessField(businessField);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Investor)) {
            return false;
        }
        return id != null && id.equals(((Investor) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Investor{" +
            "id=" + getId() +
            ", investorNo='" + getInvestorNo() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            ", companyPurpose='" + getCompanyPurpose() + "'" +
            ", companyRegisterNo='" + getCompanyRegisterNo() + "'" +
            ", companyForeignEmployeeNo=" + getCompanyForeignEmployeeNo() +
            ", companyLocalEmployeeNo=" + getCompanyLocalEmployeeNo() +
            ", companyTradeChamberNo='" + getCompanyTradeChamberNo() + "'" +
            ", companyEstablishmentDate='" + getCompanyEstablishmentDate() + "'" +
            ", companyMobileNo='" + getCompanyMobileNo() + "'" +
            ", companyEmail='" + getCompanyEmail() + "'" +
            ", companyWebsite='" + getCompanyWebsite() + "'" +
            ", companyAddress='" + getCompanyAddress() + "'" +
            ", companyAddressCity='" + getCompanyAddressCity() + "'" +
            ", companyCapital='" + getCompanyCapital() + "'" +
            ", companyCapitalText='" + getCompanyCapitalText() + "'" +
            ", companyCapitalCurrency='" + getCompanyCapitalCurrency() + "'" +
            ", representativeFullName='" + getRepresentativeFullName() + "'" +
            ", representativeMobileNo='" + getRepresentativeMobileNo() + "'" +
            ", representativeEmail='" + getRepresentativeEmail() + "'" +
            ", representativeNationalNo='" + getRepresentativeNationalNo() + "'" +
            ", representativeRegisterNo='" + getRepresentativeRegisterNo() + "'" +
            ", representativePassportNo='" + getRepresentativePassportNo() + "'" +
            ", isRepresentativeForeign='" + getIsRepresentativeForeign() + "'" +
            ", notes='" + getNotes() + "'" +
            ", investorStatus='" + getInvestorStatus() + "'" +
            "}";
    }
}
