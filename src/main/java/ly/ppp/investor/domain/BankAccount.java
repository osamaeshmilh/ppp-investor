package ly.ppp.investor.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A BankAccount.
 */
@Entity
@Table(name = "bank_account")
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account_no")
    private String accountNo;

    @Column(name = "bank_branch_name")
    private String bankBranchName;

    @Column(name = "notes")
    private String notes;

    @ManyToOne
    @JsonIgnoreProperties(value = { "user" }, allowSetters = true)
    private Bank bank;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "user", "companyCity", "companyCountry", "representativeCity", "representativeCountry", "businessField" },
        allowSetters = true
    )
    private Investor investor;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public BankAccount id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNo() {
        return this.accountNo;
    }

    public BankAccount accountNo(String accountNo) {
        this.setAccountNo(accountNo);
        return this;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBankBranchName() {
        return this.bankBranchName;
    }

    public BankAccount bankBranchName(String bankBranchName) {
        this.setBankBranchName(bankBranchName);
        return this;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getNotes() {
        return this.notes;
    }

    public BankAccount notes(String notes) {
        this.setNotes(notes);
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Bank getBank() {
        return this.bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public BankAccount bank(Bank bank) {
        this.setBank(bank);
        return this;
    }

    public Investor getInvestor() {
        return this.investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public BankAccount investor(Investor investor) {
        this.setInvestor(investor);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankAccount)) {
            return false;
        }
        return id != null && id.equals(((BankAccount) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BankAccount{" +
            "id=" + getId() +
            ", accountNo='" + getAccountNo() + "'" +
            ", bankBranchName='" + getBankBranchName() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }
}
