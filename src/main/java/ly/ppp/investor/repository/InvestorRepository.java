package ly.ppp.investor.repository;

import ly.ppp.investor.domain.Investor;
import ly.ppp.investor.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Investor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvestorRepository extends JpaRepository<Investor, Long>, JpaSpecificationExecutor<Investor> {
    Long countByInvestorNoStartingWith(String s);

    Investor findByUser(User user);
}
