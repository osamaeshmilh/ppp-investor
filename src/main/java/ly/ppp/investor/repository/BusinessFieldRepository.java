package ly.ppp.investor.repository;

import ly.ppp.investor.domain.BusinessField;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the BusinessField entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusinessFieldRepository extends JpaRepository<BusinessField, Long>, JpaSpecificationExecutor<BusinessField> {}
