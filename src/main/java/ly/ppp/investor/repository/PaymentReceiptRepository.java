package ly.ppp.investor.repository;

import ly.ppp.investor.domain.PaymentReceipt;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PaymentReceipt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentReceiptRepository extends JpaRepository<PaymentReceipt, Long>, JpaSpecificationExecutor<PaymentReceipt> {}
