package ly.ppp.investor.repository;

import ly.ppp.investor.domain.Orginization;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Orginization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrginizationRepository extends JpaRepository<Orginization, Long>, JpaSpecificationExecutor<Orginization> {}
