package ly.ppp.investor.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String INVESTOR = "ROLE_INVESTOR";

    public static final String TREASURY = "ROLE_TREASURY";

    public static final String BANK = "ROLE_BANK";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {}
}
